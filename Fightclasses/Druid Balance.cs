#if VISUAL_STUDIO
using robotManager.Helpful;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using wManager.Wow.Bot.Tasks;
using wManager.Wow.Class;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager.Wow.Enums;

// testing testing
public class FightclassDruid
{
#endif
	#region COMMON
	public static bool IsCastingAndInterruptible { get { return ObjectManager.Target.IsValid && ObjectManager.Target.IsCast &&; ObjectManager.Target.CanInterruptCasting; } }
	public static bool IsCastingAndInterruptibleButTooLate { get { return IsCastingAndInterruptible && ObjectManager.Target.CastingTimeLeft < 1000; } }
	public static bool WantShapeshift {
		get {
			if (Conditions.ForceIgnoreIsAttacked)
				return false;

			if (wManager.Wow.Bot.Tasks.FishingTask.IsLaunched)
				return false;

			if (ObjectManager.Me.IsCast)
				return false;

			if (ObjectManager.Pet.UnitFlags.HasFlag(UnitFlags.Possessed))
				return false;

			return true;
		}
	}

	/* Shapeshift Form IDs */
	public static uint CatFormID = 768;
	public static uint BearFormID = 5487;
	public static uint MoonkinFormID = 197625;
	public static uint TravelFormID = 783;
	public static uint ProwlID = 22842;

	/* Buff IDS */
	public static uint GalacticGuardianBuffID = 213708;
	public static uint IronfurBuffID = 192081;
	public static uint FrenziedRegenerationID = 22842;

	/* Debuff IDs*/
	public static uint ThrashDebuffID = 192090;
	public static uint MoonfireDebuffID = 164812;

	/*Spell IDs*/

	#endregion COMMON

	#region GUARDIAN
	public static bool NeedBearForm {
		get {
			return Enabled && !ObjectManager.Me.HaveBuff(BearFormID);
		}
	}

	public static bool TargetNeedsMoreThrash {
		get {
		return Enabled && wManager﻿.Wow.ObjectManager.ObjectManager﻿.Target.BuffStack(ThrashDebuffID) < 3;
		}
	}

	#endregion GUARDIAN
}
/*
	#region FERAL
	public static bool NeedCatForm
	{
		get
		{
			return Enabled && !ObjectManager.Me.HaveBuff(768);
		}
	}


	static uint PredatorySwiftnessID = 69369;
	static string PredatorySwiftness = string.Empty;
	public static bool IsRegrowthInstant
	{
		get
		{
			if (string.IsNullOrEmpty(PredatorySwiftness))
				PredatorySwiftness = SpellListManager.SpellNameInGameById(PredatorySwiftnessID);

			if (SpellManager.HaveBuffLua(PredatorySwiftness))
				return true;

			return ObjectManager.Me.HaveBuff(PredatorySwiftnessID);
		}
	}
	#endregion FERAL

#if VISUAL_STUDIO
}
*/
