
local InviteList={}
local PlayersInvited={}

MAX_HEIGHT = math.min(PlayerFrame:GetBottom()-QuickJoinToastButton:GetTop()-40,500)
WIDTH = PlayerFrame:GetRight()-PlayerPortrait:GetLeft()
--[[
I hear BACKDROP is "basically obsolute"...but I haven't spent the time
or figured out how to implement textures well yet, so...guidance appreciated
]]
-- Place the frame 20 px below PlayerFrame
count = 0

local function CheckSameRealm(unitID)
   local name, realm = UnitName(unitID)
   local isSameRealm = false -- defaulting to False
   if realm == nil then isSameRealm = true
   end
   return isSameRealm
end

local function CheckHasGuild(unitID)
   local guildName, guildRankName, guildRankIndex = GetGuildInfo(unitID)
   local hasGuild = true
   if guildName == nil then hasGuild = false end
   return hasGuild
end

local function CheckAlreadyInvited(name) return not not PlayersInvited[name] end --[[HUGE THANKS TO: Meorawr, Icesythe7, and Vlad]]

local function CheckAlreadyOnList(name) return not not InviteList[name] end

local function CheckIsPlayer(unitID) return not not UnitIsPlayer(unitID) end

local function CheckElligibility(unitID)
   local name, realm = UnitName(unitID)
   local isPlayer = CheckIsPlayer(unitID)
   local isSameRealm = CheckSameRealm(unitID)
   local hasGuild = CheckHasGuild(unitID)
   local alreadyInvited = CheckAlreadyInvited(name)
   local alreadyOnList = CheckAlreadyOnList(name)
   local isElligible = false
   if isSameRealm and isPlayer and not hasGuild and not alreadyInvited and not alreadyOnList then isElligible = true end
   return isElligible
end

function CreateTargetPlayerFrame(UnitID)
   local f = CreateFrame("Button","ProspectFrame"..count,UIParent,"SecureUnitButtonTemplate")
   f.unit = unitID
   local name = UnitName(f.unit)
   print(name)

   --f:RegisterForClicks("AnyUp")
   f:SetAttribute("type","macro")
   f:SetAttribute("macrotext", "/tar "..name)
   f:SetSize(120,32)
   if count == 0 then
      local y_offset = PlayerFrame:GetBottom() -20
      local x_offset = PlayerPortrait:GetLeft()
      f:SetPoint("BOTTOMLEFT",x_offset,y_offset)
   elseif count > 0 then
      print("Positioning frame relative to ProspectFrame"..(count - 1))
      f:SetPoint("TOPLEFT", "ProspectFrame"..(count - 1),"BOTTOMLEFT",0,-4) -- 4px spacing
   end
   count = count + 1
   -- background texture
   f.back = f:CreateTexture(nil,"BACKGROUND")
   f.back:SetAllPoints(true)
   f.back:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Background")

   -- health statusbar
   f.hp = CreateFrame("StatusBar",nil,f,"TextStatusBar")
   f.hp:SetHeight(16)
   f.hp:SetPoint("BOTTOMLEFT")
   f.hp:SetPoint("BOTTOMRIGHT")
   f.hp:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
   f.hp:SetStatusBarColor(0,1,0)
   f.hp:SetMinMaxValues(0,1)

   -- name fontstring
   f.name = f:CreateFontString(nil,"ARTWORK","GameFontHighlight")
   f.name:SetPoint("TOP",0,-2)
   f.name:SetText((UnitName(f.unit)))
end

local function GetInitialProspects()
   for i = 1,40 do
      local unitID = "nameplate"..i
      local name, realm = UnitName(unitID)
      local isElligible = CheckElligibility(unitID)
      if isElligible then
         print(name, unitID)
         CreateTargetPlayerFrame(unitID)
      end
   end
end

GetInitialProspects()
