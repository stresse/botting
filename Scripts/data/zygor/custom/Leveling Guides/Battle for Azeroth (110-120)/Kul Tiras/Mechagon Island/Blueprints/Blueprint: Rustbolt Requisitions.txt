 return level == 120 and not completedq(55074) end,
condition_end=function() return completedq(55074) end,
},[[
step
use the Irradiated Box of Assorted Parts##168395
|tip These are received after completing the hardmode of the Reclamaition Rig event.
|tip To activate the hardmode, you need the Supercollider weapon awarded from completing the "Toys for Destruction" daily quest four times.
|tip Use the special action button on-screen on every Irradiated Elemental for the entire event.
|tip If done correctly, 3 large red Unstable Irradiated Golems spawn at the end of the event.
collect 1 Blueprint: Rustbolt Requisitions##168495 |goto Mechagon Island/0 69.85,61.49  |q 55074 |future
|tip This blueprint is common, but not a guaranteed drop.
step
use the Blueprint: Rustbolt Requisitions##168495
accept Blueprint: Rustbolt Requisitions##55074
step
talk Pascal-K1N6##150359
turnin Blueprint: Rustbolt Requisitions##55074 |goto 71.35,32.28
step
