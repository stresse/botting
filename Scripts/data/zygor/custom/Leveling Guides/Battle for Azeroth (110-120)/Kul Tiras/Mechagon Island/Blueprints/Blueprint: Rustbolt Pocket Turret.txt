 return level == 120 and not completedq(55082) end,
condition_end=function() return completedq(55082) end,
},[[
step
kill Enforcer KX-T57##154153
|tip This is a rare spawn and will not always be available.
collect 1 Blueprint: Rustbolt Pocket Turret##169174 |goto Mechagon Island/0 52.49,62.92 |q 55082 |future
|tip This blueprint is common, but not a guaranteed drop.
step
accept Blueprint: Rustbolt Pocket Turret##55082
|tip You will accept this quest automatically.
step
talk Pascal-K1N6##150359
turnin Blueprint: Rustbolt Pocket Turret##55082 |goto 71.35,32.28
step
