 return level == 120 and not completedq(56087) end,
condition_end=function() return completedq(56087) end,
},[[
step
Kill Rare enemies on Mechagon Island
|tip Use the POI system to find and kill them.
|tip This blueprint has a small chance to drop from any rare enemy on Mechagon Island.
collect Blueprint: Experimental Adventurer Augment##168908 |goto Mechagon Island/0 71.77,35.33 |q 56087 |future
step
use the Blueprint: Experimental Adventurer Augment##168908
accept Blueprint: Experimental Adventurer Augment##56087
step
talk Pascal-K1N6##150359
turnin Blueprint: Experimental Adventurer Augment##56087 |goto 71.35,32.28
step
