#!/usr/bin/env python3
from lxml import etree,html,objectify
import requests, string, urllib
import json, re, ast, pathlib
from bs4 import BeautifulSoup

wowhead_url = "http://www.wowhead.com/"

ACTION_REGEX_D = {
	"pickup" : re.compile(r"(?ims)^talk (?P<npc_name>.*?)##(?P<npc_id>\d*?)(?:\D|$).*?^accept (?P<quest_name>.*?)##(?P<quest_id>\d*?)(?:\D|$)"),
	"turnin" : re.compile(r"(?ims)^talk (?P<npc_name>.*?)##(?P<npc_id>\d*?)(?:\D|$).*?^turnin (?P<quest_name>.*?)##(?P<quest_id>\d*?)(?:\D|$)"),
	"flightpath" : re.compile(r"(?ims)^talk (?P<npc_name>.*?)##(?P<npc_id>\d*?)(?:\D|$).*?^fpath (?P<fpath>.*?) \|goto"),
	"set_home" : re.compile(r"(?ims)^talk (?P<npc_name>.*?)##(?P<npc_id>\d*?)(?:\D|$).*?^home (?P<home_name>.*?) \|goto"),
	"gossip" : re.compile(r"(?ims)^talk (?P<npc_name>.*?)##(?P<npc_id>\d*?)(?:\D|$).*?(?:Choose|Tell him|Tell her|Ask him|Ask her) _(?P<gossip_option>.*?)_.*? \|q (?P<quest_id>\d*)(?:/)?(?P<objective_number>\d)?(?: |$)"),
	#"(?P<quest_id>\d*)(?:/)?(?P<objective_number>\d)?(?: |$)"
	"click_object" : re.compile(r"(?ims)^(?P<action_type>click) (?P<target>.*?)(?:##)?(?P<target_id>[0-9+]*?)$.*?\|q (?P<quest_id>\d*?)(?:/)?(?P<objective_number>\d)(?: |$)"),
	"interact_npcs" : re.compile(r"(?ims)^clicknpc (?P<npc_name>.*?)##(?P<npc_id>[0-9+]*?)(?: |$).*?#(?P<objective_count>\d*?)#\|q (?P<quest_id>\d*?)(?:/)?(?P<objective_number>\d)(?: |$)"),
	"follow_path" : re.compile(r"(?ims)^(?:Track |Follow )(?P<destination_name>.*?) \|q (?P<quest_id>\d*?)(?:/)?(?P<objective_number>\d)(?: |$)"),
	"kill_and_loot" : re.compile(r"(?ims)^Kill .*?^collect (?P<objective_count>\d*?) (?P<loot_name>.*?)##(?P<loot_id>\d*?) \|q (?P<quest_id>\d*?)(?:/)?(?P<objective_number>\d)(?: |$)"),
	"kill_regex_1" : re.compile(r"(?ims)^Kill .*?^Slay #(?P<objective_count>\d*?)# (?P<enemy_name>.*?) \|q (?P<quest_id>\d*?)(?:/)?(?P<objective_number>\d)(?: |$)"),
	"kill_regex_2" : re.compile(r"(?ims)^kill (?P<enemy_name>.*?)##(?P<npc_id>\d*?)(\D|$).*?\|q (?P<quest_id>\d*?)(?:/)?(?P<objective_number>\d)(?: |$)(.*? \|count (?P<count_number>\d)(\D|$))?"),
	"custom_regex" : re.compile(r"(?ims)^(?P<custom_name>Guide Loh) .*?\|q (?P<quest_id>\d*?)(?:/)?(?P<objective_number>\d)(?: |$)")
}

bfa_zygor_file = "/Applications/World of Warcraft/_retail_/Interface/AddOns/ZygorGuidesViewer/Guides/Leveling/ZygorLevelingHordeBFA.lua"
#legion_zygor_file = "/Users/jesse/Documents/programming/stresse/zygor/HordeLegion.txt"
zygor_npcdb_file = "/Users/jesse/Documents/programming/stresse/zygor/NPCdb.txt"
guide_output_directory = "/Users/jesse/Programming/botting/Scripts/data/zygor/custom"
zuldazar_file = "/Users/jesse/Programming/botting/Scripts/data/zygor/custom/Leveling Guides/Battle for Azeroth (110-120)/Zandalar/Zuldazar.txt"


def wowhead_lookup_npc(npc):
	quests_started = []
	quests_ended = []
	npc_id = npc
	url = f'{wowhead_url!s}npc={npc_id!s}'
	page = requests.get(url)
	tree = lxml.html.fromstring(page.text)
	full_url = tree.xpath('//meta[@property="og:url"]/@content')[0]
	print(full_url)
	scripts = tree.xpath('//*[@id="main-contents"]/script/text()')
	correct_script = list(filter(lambda s: re.search(r"name_enus",s),scripts))
	# WORKING npc_info_regex = re.compile(r"\$.extend\(g_npcs\[\d*?\], \{\"classification\":(?P<classification>\d*?),\"id\":(?P<npc_id>\d*?),\"location\":\[(?P<locations_string>.*?)\],\"maxlevel\":(?P<max_level>\d*?),\"minlevel\":(?P<min_level>\d*?),\"name\":\"(?P<name>.*?)\",\"react\":\[(?P<react_string>.*?)\],\"type\":(?P<type>\d*?)\}\);")
	npc_info_regex = re.compile(r"\$.extend\(g_npcs\[\d*?\], \{\"classification\":(?P<classification>\d*?),(\"hasQuests\":(?P<has_quests>\d*?),)?\"id\":(?P<npc_id>\d*?),\"location\":\[(?P<locations_string>.*?)\],\"maxlevel\":(?P<max_level>\d*?),\"minlevel\":(?P<min_level>\d*?),\"name\":\"(?P<name>.*?)\",\"react\":\[(?P<react_string>.*?)\],\"type\":(?P<type>\d*?)\}\);")
	quests_started_regex = re.compile(r"new Listview\(\{template: \'quest\', id: \'starts\',.*?data: (\[.*?\])\}\);")
	quests_ended_regex = re.compile(r"new Listview\(\{template: \'quest\', id: \'ends\',.*?data: (\[.*?\])\}\);")
	authenticity_check_regex = re.compile(r"new Listview\(\{template: 'comment', id: 'comments', name: LANG.tab_comments, tabs: tabsRelated,")
	info = (sorted(filter(lambda s: npc_info_regex.search(s) and authenticity_check_regex.search(s),scripts)) or [""])[-1]
	npc_info = npc_info_regex.search(info).groupdict() if info else {}
	quests_started_search = quests_started_regex.search(info)
	quests_started_info = ast.literal_eval(quests_started_search.group(1)) if quests_started_search else []
	quests_ended_search = quests_ended_regex.search(info)
	quests_ended_info = ast.literal_eval(quests_ended_search.group(1)) if quests_ended_search else []
	for q in quests_started_info:
		questid = q.get('id')
		quests_started.append(questid)
	for q in quests_ended_info:
		questid = q.get('id')
		quests_ended.append(questid)
	npc_info.update(quests_started=quests_started,quests_ended=quests_ended,locations=npc_info["locations_string"].split(","),reactions=npc_info["react_string"].split(","),wowhead_url=full_url) if info else ""
	return npc_info

def read_zygor_file(f):
	content = open(f,"rt")
	text = content.read()
	content.close()
	return text

def split_into_guides(text):
	""" This function will take a full Zygor file and split it into individual guides
	TO DO: Add ability to process entire file or text """
	congrats_regex = re.compile(r"(?ims)RegisterGuide\(\"(?P<guide_name>[^\"]*?)\"\,[^\)]*?\)(?P<text>.*?)_Congratulations!_\n(?P<message>[^^]*?)$")
	guides = congrats_regex.findall(text)
	for g in guides:
		guide_name, text, messages = g
		split_guide_string = g[0].split("\\\\")
		guide_name = split_guide_string[-1]
		guide_directory_name = "/".join(split_guide_string[:-1])
		guide_directory = f"{guide_output_directory}/{guide_directory_name}"
		guide_file_name = f"{guide_name}.txt"
		guide_file_path = f"{guide_directory}/{guide_file_name}"
		pathlib.Path(guide_directory).mkdir(parents=True, exist_ok=True)
		print(guide_name)
		print(guide_directory)
		print(guide_file_path)
		print(g[1])
		f = open(guide_file_path, "w+")
		f.write(text)
		f.close()
#	for guide in guides:
#		if len(guide) == 1:
#			#guide_title_regex = re.compile(r"(?ims)RegisterGuide\(\"([^\"]*?)\"\,")
#			guide_title_string = re.findall(r"(?ims)",zone[0])[0]
#			print(guide_title_string)
#			good_guides =
#			split_string = guide_title_string.split("\\\\")
#			print(split_string)



def split_into_sections(text):
	sections = re.findall(r"(?ims)^label \".*?(?=^label|_Congratulations!_)",text)
	return sections

def split_into_steps(section):
	steps = re.split(r"(?ims)^step$",section)
	print(f"There are {len(steps)} steps in this sections.")
	return steps

def get_step_actions(step):
	actions = []
	for k in ACTION_REGEX_D.keys():
		search = ACTION_REGEX_D[k].finditer(step)
		for match in search: # check to see if the text matches any our our quest step regexs
			action = k
			actions.append(dict(action=k,**match.groupdict()))
	return actions




#for s in sections[:1]:
#	steps = split_into_steps(s)
#	for s in steps:
#		actions = get_step_actions(s)
#		for a in actions:
#			print(a)

#for s in sections:
#	print(s)

def parse_zygor_file(f):
	guide_content = read_zygor_file(f)

	guide_content = []
	all_quests = {}
	quests_info = {}
	all_actions = []
	all_quest_ids = []
	for s in sections:
		section_label = re.findall(r"(?ims)^label \"(.*?)\"", s)[0]
		section_content = '\n'.join(s.splitlines()[1:]) #get rid of the label line
		section_number = sections.index(s)
		section_steps = list(filter(lambda s: s != '',re.split(r"(?ims)\nstep$",section_content)))
		guide_content.append(dict(label=section_label,section_number=section_number,steps=section_steps,steps_quantity=len(section_steps)))
	total_steps = sum([s["steps_quantity"] for s in guide_content])
	print(f"There are {len(sections)} sections in this guide.")
	print(f"There are {total_steps} total steps in this guide.")
	print("Starting steps")
	for section in guide_content:
		section_steps = section['steps']
		for step in section_steps:
			step_actions = []
			zygor_text = step
			step_number = section_steps.index(step)
			zygor_tip = (re.findall(r"(?ims)\n\|tip (.*?)\n", zygor_text) or [''])[0]
			npc_info_regex = re.compile(r"(?ims)^talk (?P<npc_name>.*?)##(?P<npc_id>\d*?)$")
			npc_info = ([npc.groupdict() for npc in npc_info_regex.finditer(zygor_text)] or [''])[0] #limits to one npc; may need to adjust down the line if zygor guide has multiple npcs in one step



			pickup_turnins_actions = [pt.groupdict() for pt in pickup_turnin_regex.finditer(zygor_text)]
			step_actions.extend(pickup_turnins_actions)


			all_quests.update([(pt.get('quest_id'),dict(action="PickUp" if pt.get('action_type')=="accept" else "TurnIn" if pt.get('action_type')=="turnin" else "ERROR",name=pt.get('quest_name'),quest_id=pt.get('quest_id'),nameclass=pt.get('quest_name').replace(' ',''))) for pt in pickup_turnins_actions])

			set_home_actions = [dict(g.groupdict(),**dict(action_type='sethome')) for g in set_home_search.finditer(zygor_text)]
			step_actions.extend(set_home_actions)
			fpath_actions = [dict(g.groupdict(),**dict(action_type='fpath')) for g in fpath_regex.finditer(zygor_text)]
			step_actions.extend(fpath_actions)

			gossip_npc_actions = [g.groupdict() for g in gossip_npc_regex.finditer(zygor_text)]
			step_actions.extend(gossip_npc_actions)
			click_object_actions = [dict(o.groupdict(),**dict(quest_type='InteractWithNpc')) for o in click_object_regex.finditer(zygor_text)]
			step_actions.extend(click_object_actions)
			interact_npcs_actions = [dict(o.groupdict(),**dict(action_type='interactnpc',quest_type='RunCode')) for o in interact_npcs_regex.finditer(zygor_text)]
			step_actions.extend(interact_npcs_actions)
			follow_path_actions = [dict(o.groupdict(),**dict(action_type='followpath',quest_type='FollowPath')) for o in follow_path_regex.finditer(zygor_text)]
			step_actions.extend(follow_path_actions)
			kill_and_loot_actions = [dict(o.groupdict(),**dict(action_type='killforloot',quest_type='KillAndLoot')) for o in kill_and_loot_regex.finditer(zygor_text)]
			step_actions.extend(kill_and_loot_actions)
			kill_actions_1 = [dict(o.groupdict(),**dict(action_type='kill',quest_type='KillAndLoot')) for o in kill_regex_1.finditer(zygor_text)]
			step_actions.extend(kill_actions_1)
			kill_actions_2 = [dict(o.groupdict(),**dict(action_type='kill',quest_type='KillAndLoot')) for o in kill_regex_2.finditer(zygor_text)]
			step_actions.extend(kill_actions_2)
			custom_actions = [dict(o.groupdict(),**dict(action_type='custom',quest_type='None')) for o in custom_regex.finditer(zygor_text)]
			step_actions.extend(custom_actions)
			#quest_placeholder_quest_id_regex = re.compile(r"(?ims)^(?P<custom_name>Guide Loh)")
			#placeholder_step_actions = [dict(action_type='UNKNOWN',quest_type='UNKNOWN')] if len(step_actions) == 0 else []
			#step_actions.extend(placeholder_step_actions)
			[sa.update(comment=zygor_text) for sa in step_actions]
			print(step_actions)
			all_actions.extend(step_actions)
		section.update(actions=all_actions)
	for a in all_actions:
		quest_id = int(a.get("quest_id")) if a.get("quest_id") else None
		quests_info.update({quest_id: {}})
		objective_number = int(a.get("objective_number")) if a.get("objective_number") else 1
		quests_info[quest_id].update({objective_number:{}})
		objective_count = int(a.get("objective_count")) if a.get("objective_count") else 1
		quests_info[quest_id][objective_number].update(dict(objective_count=objective_count))
		#quest_info_dict = {quest_id:{objective_number:dict(objective_count=objective_count)}}
		#quests_info.update(quest_info_dict)
		#print(quest_info_dict)
		#quest_info.update()
	print(quests_info)
	print(quests_info[53736])
	return guide_content

#def quest_lookup(id)


def parse_for_quest_givers(file):
	quest_givers = []
	all_quests = []
	guide_text = open(file, "rt")
	guide_content = guide_text.read()
	#	In case I want to split the guide into steps:
	#	section_steps = list(filter(lambda s: s != '',re.split(r"(?ims)\nstep\n|\nstep$",section_content)))
	#	Works for grabbing npc names and ids (use this if attempts to add coordinates fail)
	npc_info_regex = re.compile(r"(?ims)^talk (?P<npc_name>[^\n]*?)##(?P<npc_id>\d*?)$")
	all_npcs = npc_info_regex.findall(guide_content)
	unique_npc_ids = set(all_npcs)
	print(f"There are {len(unique_npc_ids)} npc")
	for npc_name,npc_id in unique_npc_ids:
		print(npc_id)
		quest_givers.extend(wowhead_lookup_npc(npc_id))
	return quest_givers

def zygor_npcdb_to_class(file=zygor_npcdb_file):
	guide_text = open(file, "rt")
	guide_content = guide_text.read()
	zygor_npcdb_regex = re.compile(r"(?ims)^\[(\d*?)\]=\"(.*?)\|\"")
	npc_pairs = zygor_npcdb_regex.findall(guide_content)
	for i,n in npc_pairs:
		npc_id = int(i)
		npc_name = n


def parse_for_quests(file):
	quest_givers = []
	all_quests = {}
	guide_text = open(file, "rt")
	guide_content = guide_text.read()
	#Good, but no objective count
	#quests_in_file_regex = re.compile(r"(?ims)\|q (?P<quest_id>\d*?)(/(?P<objective_number>\d*?))?(?: |$)")
	quests_in_file_regex = re.compile(r"(?ims)(#(?P<objective_count>\d+?)#[^\n]*?)?\|q (?P<quest_id>\d*?)(/(?P<objective_number>\d*?))?(?: |$)")
	raw_quests_in_file = [q.groupdict() for q in quests_in_file_regex.finditer(guide_content)]
	quest_ids = list(set([q["quest_id"] for q in raw_quests_in_file]))
	print(f"There are {len(raw_quests_in_file)} raw quests in this file.")
	print(f"There are {len(quest_ids)} unique quests in this file.")
	quests_dic = {}
	for q in quest_ids:
		raw_quest_matches = [rq for rq in raw_quests_in_file if rq['quest_id'] == q]
		objectives = sorted(set([int(rq['objective_number']) for rq in raw_quest_matches if rq['objective_number']]))
		#q_dic = {q:{"objectives":objectives}}
		#print(q_dic)
		#quests_dic.update((q,dict(objectives=objectives)))
		for o in objectives:
			objective_matches = [m for m in raw_quest_matches if m["objective_number"] and int(m["objective_number"])==o]
			#print(type(o))
			print(f"Objective: {o!s}")
			objective_count = (sorted(int(m["objective_count"]) for m in objective_matches if m["objective_count"]) or [1])[0]
			#matching_objective_results = ([m["objective_count"] for m in raw_quest_matches if m["objective_number"] and int(m["objective_number"])==o] or [1])[0]
			print(f"Objective count: {objective_count!s}")
			q_dic = {q:{o:{objective_count}}}
			quests_dic.update(q_dic)
	print(quests_dic)



def write_quest_profile(t):
	guide_info = parse_zygor_file(t)
	#print(guide_info)
	print(len(guide_info))
	#output_filename = "/Users/jesse/Documents/programming/profile creator/Zuldaar 1.xml"
	#easy_quest_profile = etree.Element('EasyQuestProfile',"xmlns:xsd"="http://www.w3.org/2001/XMLSchema","xmlns:xsi"="http://www.w3.org/2001/XMLSchema-instance")
	#xsd = "http://www.w3.org/2001/XMLSchema"
	#xsi = "http://www.w3.org/2001/XMLSchema-instance"
	#{f"xmlns:xsd":"http://www.w3.org/2001/XMLSchema","xsi":"http://www.w3.org/2001/XMLSchema-instance"}
	#rootname = etree.QName(NS_MAP,"stylesheet")
	easy_quest_profile = etree.Element('EasyQuestProfile',nsmap=dict(xsd="http://www.w3.org/2001/XMLSchema",xsi="http://www.w3.org/2001/XMLSchema-instance"))
	sorted_quest_section = etree.SubElement(easy_quest_profile,'QuestsSorted')

	for quest_line in guide_info:
		quest_line_actions = quest_line['actions']
		all_actions.extend(quest_line_actions)
	for a in quest_line_actions:
		action_keys = a.keys()
		all_keys.extend(action_keys)
	#	action_quest =


	for a in test_actions:
		quest = a.get("quest_name")
		print(test_actions.index(a))
		print(a)
		if not quest:
			actions_no_quest.extend(a)
	print(f"There are {len(actions_no_quest)} actions without quests.")
	for a in actions_no_quest:
		print(a)
		#print(q["quest_type"])
		#quest_child = etree.SubElement(sorted_quest_section,'QuestsSorted',Action=f"{}",NameClass='')
	#print(etree.tostring(easy_quest_profile,encoding="unicode",pretty_print=True))

def wowhead_quest_lookup(id):
	wowhead_quest_url = f"http://www.wowhead.com/quest={id!s}/"
	wowhead_page = requests.get(wowhead_quest_url, allow_redirects=True)
	page_text = wowhead_page.text
	tree = html.fromstring(wowhead_page.content)
	quest_title = tree.xpath('//h1[@class="heading-size-1"]')[0]
	quest_description = tree.xpath('//meta[@name="description"]/@content')
	npc_starts_regex = re.compile(r'WH.markup.printHtml\([^^]*?\[icon name=quest_start\]Start: \[url=\\/npc=(?P<npc_id>\d*)\](?P<npc_name>[^^]*?)\[[^^]*?$')
	npc_starts = '{npc_name} ({npc_id})'.format(**npc_starts_regex.search(page_text).groupdict())
	npc_ends_regex = re.compile(r'WH.markup.printHtml\([^^]*?\[icon name=quest_end\]End: \[url=\\/npc=(?P<npc_id>\d*)\](?P<npc_name>[^^]*?)\[[^^]*?$')
	npc_ends = '{npc_name} ({npc_id})'.format(**npc_ends_regex.search(page_text).groupdict())
	return {"quest_title": quest_title, "npc_starts": npc_starts, "npc_ends": npc_ends}
	#print(npc_start)
	#print(f"There are {len(quest_objectives)} objectives.")
	#print(quest_objectives.text)



#http://www.wowhead.com/npc=131258

wowhead_quest_lookup(47437)
#print(wowhead_lookup_npc(122583))
#print(wowhead_lookup_npc(131258))
#zygor_npcdb_to_class()
#parse_for_quests(zuldazar_file)
#write_quest_profile(bfa_zygor_file)
#print(parse_zygor_file(bfa_zygor_file))
#parse_zygor_file(bfa_zygor_file)
