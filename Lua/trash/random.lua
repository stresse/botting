Annette McTague

-- Good Texture Around A String



CreateFittedTextFrame("Does this work?")

-- Constants
local BACKDROP =
    {
      bgFile = "Interface/Tooltips/UI-Tooltip-Background",
      edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
      tile = true, tileSize = 16, edgeSize = 16,
      insets = { left = 4, right = 4, top = 4, bottom = 4 }
    }

-- Frame Functions

local function AddChatMessage(message)
  DEFAULT_CHAT_FRAME:AddMessage("|cffC41F3B"..message)
end

--local function ScaleFrameToText(frame)


function AddTitle(frame, title)
  title_frame = CreateFrame("Frame", "$parent_Title_Frame", frame)
  title_string = title_frame:CreateFontString("$parent_Title_String", "OVERLAY", "GameFontNormalLarge")
  title_string:SetText(title)
  title_string:SetJustifyH("CENTER")
	title_string:SetJustifyV("CENTER")
  s_height = title_string:GetStringHeight() -- These values do end up returning the correct values (or near enough); also same as GetHeight()
  s_width = title_string:GetStringWidth()   -- Ditto
  AddChatMessage("Height: "..s_height)
  AddChatMessage("Width: "..s_width)
  title_frame:SetSize(s_width,s_height)
  title_frame:SetBackdrop(BACKDROP)
  title_frame:SetBackdropColor(0, 0, 0)
  title_frame:SetPoint("CENTER", frame, "TOP", 0, 0)
end

function AddCloseButton(frame)
  closeButton = CreateFrame("BUTTON", nil, frame, "UIPanelCloseButton")
  closeButton:SetSize(16, 16)
  closeButton:SetPoint("TOPRIGHT", frame, 3, 3)
  closeButton:SetScript("OnClick", function()
        frame:Hide()
        DEFAULT_CHAT_FRAME:AddMessage(frame:GetName().."|cffC41F3Bhidden.")
  end)
end

function MakeResizable(frame)
  local resizeButton = CreateFrame("BUTTON", nil, frame)
  resizeButton:SetSize(16, 16)
  resizeButton:SetPoint("BOTTOMRIGHT")
  resizeButton:SetNormalTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Up")
  resizeButton:SetHighlightTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Highlight")
  resizeButton:SetPushedTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Down")
  resizeButton:SetScript("OnMouseDown", function() frame:StartSizing("BOTTOMRIGHT") frame:SetUserPlaced(true) end)
  resizeButton:SetScript("OnMouseUp",function() frame:StopMovingOrSizing() end)
end

function MakeMovable(frame)
  frame:SetMovable(true)
  frame:EnableMouse(true)
  frame:SetUserPlaced(true)
  frame:SetScript("OnMouseDown", function(self, button)
    if button == "LeftButton" and not self.isMoving then
      self:StartMoving();
      self.isMoving = true;
    end
  end)
  frame:SetScript("OnMouseUp", function(self, button)
    if button == "LeftButton" and self.isMoving then
      self:StopMovingOrSizing();
      self.isMoving = false;
    end
  end)
  frame:SetScript("OnHide", function(self)
    if ( self.isMoving ) then
      self:StopMovingOrSizing();
      self.isMoving = false;
    end
  end)
end

local function CreateFittedTextFrame(text) -- THANK YOU MooreaTv
  local f = CreateFrame("Frame")
  f:SetPoint("CENTER")
  local t = f:CreateFontString()
  t:SetFontObject(GameFontHighlightSmall)
  t:SetText(text)
  t:SetPoint("CENTER")
  f:SetSize(t:GetStringWidth(),t:GetHeight())
  local bg = f:CreateTexture()
  bg:SetColorTexture(0.317, 0.317, 0.345, .5) -- Greyish color; half opaque
  bg:SetAllPoints()
  f:Show()
end

-- Main Frame
local Stress=CreateFrame("Frame","Stressed Frame", UIParent)
Stress:SetSize(300,500)  --width, height
Stress:SetBackdrop(BACKDROP) --  Backdrops are "basically deprecated"; should re-write using Textures
Stress:SetBackdropColor(0, 0, 0)
Stress:SetPoint("LEFT", 4, 150)

--  Target Player Frame
local function CreateTargetPlayerFrame()
  btn = CreateFrame("Button", "myButton", UIParent, "SecureActionButtonTemplate");
  btn:SetAttribute("type", "action");
  btn:SetAttribute("action", 1);
end


-- Apply Function To Frame
MakeResizable(Stress)
MakeMovable(Stress)
AddCloseButton(Stress)
AddTitle(Stress, "Stress Panel")
