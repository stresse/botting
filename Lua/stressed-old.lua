local myframe = CreateFrame("Frame", "Stressed Frame", UIParent)
myframe:ClearAllPoints()
myframe:SetPoint("CENTER", UIParent, "CENTER", 0, 0)
myframe:SetWidth(300)
myframe:SetHeight(300)
local mytexture = myframe:CreateTexture("MyTexture", "ARTWORK")
mytexture:SetWidth(300)
mytexture:SetHeight(300)
mytexture:ClearAllPoints()
mytexture:SetTexture(0, 0, 1)
mytexture:SetAllPoints(myframe)

--      START

BACKDROP = {bgFile = "Interface/Tooltips/UI-Tooltip-Background",
    edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
    tile = true, tileSize = 16, edgeSize = 16,
    insets = { left = 4, right = 4, top = 4, bottom = 4 }}

local Stress=CreateFrame("Frame","Stressed Frame", UIParent)
Stress:SetFrameStrata("BACKGROUND")
Stress:SetHeight(200)
Stress:SetWidth(300)
Stress:SetBackdrop(BACKDROP)
Stress:SetBackdropColor(0, 0, 0)
Stress:SetPoint("CENTER")
Stress:SetMovable(true)
Stress:EnableMouse(true)
Stress:SetUserPlaced(true)
Stress:SetScript("OnMouseDown",function() Stress:StartMoving() end)
Stress:SetScript("OnMouseUp",function() Stress:StopMovingOrSizing() end)

Stress.Close = CreateFrame("BUTTON", nil, Stress, "UIPanelCloseButton")
Stress.Close:SetSize(16, 16)
Stress.Close:SetPoint("TOPRIGHT", Stress, 3, 3)
Stress.Close:SetScript("OnClick", function()
    Stress:Hide()
    DEFAULT_CHAT_FRAME:AddMessage("Stress Info |cffC41F3Bhidden|r |cffFFFFFFUse /stress to reenable.")
    end)

Stress.Resize = CreateFrame("Button", nil, Stress)
Stress.Resize:SetSize(16, 16)
Stress.Resize:SetPoint("BOTTOMRIGHT")
Stress.Resize:SetNormalTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Up")
Stress.Resize:SetHighlightTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Highlight")
Stress.Resize:SetPushedTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Down")
Stress.Resize:SetScript("OnMouseDown", function(self, button)
    Stress:StartSizing("BOTTOMRIGHT")
    Stress:SetUserPlaced(true)
	end)

-- END


-- Project B START



local buttonHeight = 24
local numButtons = 12

-- create the base frame
local frame = CreateFrame("Frame","ExpandExample",UIParent,"BasicFrameTemplate")
frame:SetSize(300,buttonHeight*numButtons+36)
frame:SetPoint("CENTER")

function GetAvailablePlayers(self)
	local


function ExpandExample_ListButtonOnClick(self)
  local index = self:GetID()
  local skillName, skillType, _, isExpanded = GetTradeSkillInfo(index)
  if skillType=="header" or skillType=="subheader" then
    -- only headers can be expanded or collapsed
    if isExpanded then
      CollapseTradeSkillSubClass(index)
    else
      ExpandTradeSkillSubClass(index)
    end
    ExpandExample_UpdateList()
  end
end

function ExpandExample_UpdateList()
  for i=1,numButtons do
    local button = frame.buttons[i]
    if i<=GetNumTradeSkills() then
      local skillName, skillType, _, isExpanded = GetTradeSkillInfo(i)
      if skillType=="header" or skillType=="subheader" then
        -- this is a header, we repurpose the button to show a header
        button.headerText:SetText(skillName)
        button.headerText:Show()
        button.contentText:Hide()
        button:SetNormalTexture("Interface\\Common\\Dark-GoldFrame-Button")




        local Debug = function  (str, ...)
            if ... then str = str:format(...) end
            DEFAULT_CHAT_FRAME:AddMessage(("Stress Alert!: %s"):format(str));
        end






-- Project A END

    OnShow = function()
      botEditBox = this:GetName().."EditBox"
      SS_Bot:RegisterEvent("PLAYER_TARGET_CHANGED", function()
        if UnitIsPlayer("target") then
          getglobal(botEditBox):SetText(UnitName("target") or "")
        end
      end)
      if UnitIsPlayer("target") then
        getglobal(botEditBox):SetText(UnitName("target") or "")
      end
    end,




--      START Working Copy Using Local

local f=CreateFrame("Frame","Stressed Frame", UIParent)

f:SetFrameStrata("BACKGROUND")
f:SetHeight(200)
f:SetWidth(300)

f:SetBackdrop({bgFile = "Interface/Tooltips/UI-Tooltip-Background",
    edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
    tile = true, tileSize = 16, edgeSize = 16,
    insets = { left = 4, right = 4, top = 4, bottom = 4 }})

f:SetBackdropColor(0, 0, 0)
f:SetPoint("LEFT")

f:SetMovable(true)
f:EnableMouse(true)
f:SetScript("OnMouseDown",function() f:StartMoving() end)
f:SetScript("OnMouseUp",function() f:StopMovingOrSizing() end)
f.Close = CreateFrame("BUTTON", nil, f, "UIPanelCloseButton")
f.Close:SetWidth(20)
f.Close:SetHeight(20)
f.Close:SetPoint("TOPRIGHT", f, 3, 3)
f.Close:SetScript("OnClick", function()
    f:Hide()
    DEFAULT_CHAT_FRAME:AddMessage("Stress Info |cffC41F3Bhidden|r |cffFFFFFFUse /stress to reenable.")
    end)


-- Add target (id, name) to list


AddUnit = function()

StaticPopupDialogs["ADD_UNIT_QUESTION"] = {
	text = "Would you like to add " + ,
	button1 = ALL_SETTINGS,
	button3 = CURRENT_SETTINGS,
	button2 = CANCEL,
	OnShow = function (self, data)
    self.editBox:SetText("")
		end,
	OnCancel = function() end,
	hasEditBox = true,
	showAlert = 1,
	timeout = 0,
	exclusive = 1,
	hideOnEscape = 1,
	whileDead = 1,
}
-- START
Stress = {}
function Stress.AddMob()
local unitName = UnitName("target")
local npcId = tonumber((UnitGUID("target")):sub(-12, -9), 16)
  StaticPopupDialogs["STRESS_ADDMOB"] = {
    text = "Would you like to add " + unitName + " (" + npcId + ") to this quest profile?",
    button1 = YES,
    button2 = NO,
    OnAccept = function()
      Stress:Add()
    end,
    OnShow = function()
      botEditBox = this:GetName().."EditBox"
      SS_Bot:RegisterEvent("PLAYER_TARGET_CHANGED", function()
        if not UnitIsPlayer("target") then
          getglobal(botEditBox):SetText(UnitName("target") or "")
        end
      end)
      if not UnitIsPlayer("target") then
        getglobal(botEditBox):SetText(UnitName("target") or "")
      end
    end,
    OnHide = function()
      SS_Bot:UnregisterEvent("PLAYER_TARGET_CHANGED")
    end,
    hasEditBox = 1,
    timeout = 0,
    whileDead = 1,
    hideOnEscape = 1,
  }
  StaticPopup_Show ("SPAMSENTRY_MARKBOT")
end
-- WORKING
--local resizeButton = CreateFrame("Button", nil, f)
--resizeButton:SetSize(16, 16)
--resizeButton:SetPoint("BOTTOMRIGHT")
--resizeButton:SetNormalTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Up")
--resizeButton:SetHighlightTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Highlight")
--resizeButton:SetPushedTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Down")
--
--resizeButton:SetScript("OnMouseDown", function(self, button)
--    frame:StartSizing("BOTTOMRIGHT")
--    frame:SetUserPlaced(true)
--end)
--
--resizeButton:SetScript("OnMouseUp", function(self, button)
--    frame:StopMovingOrSizing()
--end)


local resizeButton = CreateFrame("Button", nil, f)
resizeButton:SetSize(16, 16)
resizeButton:SetPoint("BOTTOMRIGHT")
resizeButton:SetNormalTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Up")
resizeButton:SetHighlightTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Highlight")
resizeButton:SetPushedTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Down")

resizeButton:SetScript("OnMouseDown", function(self, button)
    frame:StartSizing("BOTTOMRIGHT")
    frame:SetUserPlaced(true)
end)

-- END







local function CreateMainPanel()
  if not StressPanel:
    CreateFrame("Frame","StressPanel", UIParent)
    :SetFrameStrata("BACKGROUND")
    :SetHeight(200)
    :SetWidth(300)

f:SetBackdrop({bgFile = "Interface/Tooltips/UI-Tooltip-Background",
    edgeFile = "Interface/Tooltips/UI-Tooltip-Border",
    tile = true, tileSize = 16, edgeSize = 16,
    insets = { left = 4, right = 4, top = 4, bottom = 4 }})

f:SetBackdropColor(0, 0, 0)
f:SetPoint("CENTER")

f:SetMovable(true)
f:EnableMouse(true)
f:SetScript("OnMouseDown",function() f:StartMoving() end)
f:SetScript("OnMouseUp",function() f:StopMovingOrSizing() end)
f.Close = CreateFrame("BUTTON", nil, f, "UIPanelCloseButton")
f.Close:SetWidth(20)
f.Close:SetHeight(20)
f.Close:SetPoint("TOPRIGHT", f, 3, 3)
f.Close:SetScript("OnClick", function()
    f:Hide()
    DEFAULT_CHAT_FRAME:AddMessage("Stress Info |cffC41F3Bhidden|r |cffFFFFFFUse /stress to reenable.")
    end)

-- START
-- WORKING
--local resizeButton = CreateFrame("Button", nil, f)
--resizeButton:SetSize(16, 16)
--resizeButton:SetPoint("BOTTOMRIGHT")
--resizeButton:SetNormalTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Up")
--resizeButton:SetHighlightTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Highlight")
--resizeButton:SetPushedTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Down")
--
--resizeButton:SetScript("OnMouseDown", function(self, button)
--    frame:StartSizing("BOTTOMRIGHT")
--    frame:SetUserPlaced(true)
--end)
--
--resizeButton:SetScript("OnMouseUp", function(self, button)
--    frame:StopMovingOrSizing()
--end)


local resizeButton = CreateFrame("Button", nil, f)
resizeButton:SetSize(16, 16)
resizeButton:SetPoint("BOTTOMRIGHT")
resizeButton:SetNormalTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Up")
resizeButton:SetHighlightTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Highlight")
resizeButton:SetPushedTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Down")

resizeButton:SetScript("OnMouseDown", function(self, button)
    frame:StartSizing("BOTTOMRIGHT")
    frame:SetUserPlaced(true)
end)















local cb=CreateFrame("CheckButton","myFirstCheckButton",f,"UICheckButtonTemplate")
 cb:ClearAllPoints()
 cb:SetPoint("TOPLEFT",30,-70)
 cb.text:SetText("Automatically save all NPC info")
 cb.text:SetFontObject("GameFontNormal")
 cb:SetScript("OnClick", function()
    print("WORKING")
    end)

SLASH_STRESSED1 = "/stress"
SLASH_STRESSED2 = "/addontest1"
SlashCmdList["STRESSED"] = function(msg)
   print("Hello World!")
end



--- New Addon

Stress = {};
Stress.fully_loaded = false;
Stress.default_options = {

	-- main frame position
	frameRef = "CENTER",
	frameX = 0,
	frameY = 0,
	hide = false,

	-- sizing
	frameW = 200,
	frameH = 200,
};


function Stress.OnReady()

	-- set up default options
	_G.StressPrefs = _G.StressPrefs or {};

	for k,v in pairs(Stress.default_options) do
		if (not _G.StressPrefs[k]) then
			_G.StressPrefs[k] = v;
		end
	end

	Stress.CreateUIFrame();
end

function Stress.OnSaving()

	if (Stress.UIFrame) then
		local point, relativeTo, relativePoint, xOfs, yOfs = Stress.UIFrame:GetPoint()
		_G.StressPrefs.frameRef = relativePoint;
		_G.StressPrefs.frameX = xOfs;
		_G.StressPrefs.frameY = yOfs;
	end
end

function Stress.OnUpdate()
	if (not Stress.fully_loaded) then
		return;
	end

	if (StressPrefs.hide) then
		return;
	end

	Stress.UpdateFrame();
end

function Stress.CheckTarget()


function Stress.OnEvent(frame, event, ...)

	if (event == 'ADDON_LOADED') then
		local name = ...;
		if name == 'Stress' then
			Stress.OnReady();
		end
		return;
	end

  if (event == 'PLAYER_TARGET_CHANGED') then
		local name = ...;
		if name == 'Stress' then
			Stress.CheckUnit();
		end
		return;
	end

	if (event == 'PLAYER_LOGIN') then

		Stress.fully_loaded = true;
		return;
	end

	if (event == 'PLAYER_LOGOUT') then
		Stress.OnSaving();
		return;
	end
end

function Stress.CreateUIFrame()

	-- create the UI frame
	Stress.UIFrame = CreateFrame("Frame",nil,UIParent);
	Stress.UIFrame:SetFrameStrata("BACKGROUND")
	Stress.UIFrame:SetWidth(_G.StressPrefs.frameW);
	Stress.UIFrame:SetHeight(_G.StressPrefs.frameH);

	-- make it black
	Stress.UIFrame.texture = Stress.UIFrame:CreateTexture();
	Stress.UIFrame.texture:SetAllPoints(Stress.UIFrame);
	Stress.UIFrame.texture:SetTexture(0, 0, 0);

	-- position it
	Stress.UIFrame:SetPoint(_G.StressPrefs.frameRef, _G.StressPrefs.frameX, _G.StressPrefs.frameY);

	-- make it draggable
	Stress.UIFrame:SetMovable(true);
	Stress.UIFrame:EnableMouse(true);

	-- create a button that covers the entire addon
	Stress.Cover = CreateFrame("Button", nil, Stress.UIFrame);
	Stress.Cover:SetFrameLevel(128);
	Stress.Cover:SetPoint("TOPLEFT", 0, 0);
	Stress.Cover:SetWidth(_G.StressPrefs.frameW);
	Stress.Cover:SetHeight(_G.StressPrefs.frameH);
	Stress.Cover:EnableMouse(true);
	Stress.Cover:RegisterForClicks("AnyUp");
	Stress.Cover:RegisterForDrag("LeftButton");
	Stress.Cover:SetScript("OnDragStart", Stress.OnDragStart);
	Stress.Cover:SetScript("OnDragStop", Stress.OnDragStop);
	Stress.Cover:SetScript("OnClick", Stress.OnClick);

	-- add a main label - just so we can show something
	Stress.Label = Stress.Cover:CreateFontString(nil, "OVERLAY");
	Stress.Label:SetPoint("CENTER", Stress.UIFrame, "CENTER", 2, 0);
	Stress.Label:SetJustifyH("LEFT");
	Stress.Label:SetFont([[Fonts\FRIZQT__.TTF]], 12, "OUTLINE");
	Stress.Label:SetText(" ");
	Stress.Label:SetTextColor(1,1,1,1);
	Stress.SetFontSize(Stress.Label, 20);
end

function Stress.SetFontSize(string, size)

	local Font, Height, Flags = string:GetFont()
	if (not (Height == size)) then
		string:SetFont(Font, size, Flags)
	end
end

function Stress.OnDragStart(frame)
	Stress.UIFrame:StartMoving();
	Stress.UIFrame.isMoving = true;
	GameTooltip:Hide()
end

function Stress.OnDragStop(frame)
	Stress.UIFrame:StopMovingOrSizing();
	Stress.UIFrame.isMoving = false;
end

function Stress.OnClick(self, aButton)
	if (aButton == "RightButton") then
		print("show menu here!");
	end
end

function Stress.UpdateFrame()

	-- update the main frame state here
	Stress.Label:SetText(string.format("%d", GetTime()));
end


Stress.EventFrame = CreateFrame("Frame");
Stress.EventFrame:Show();
Stress.EventFrame:SetScript("OnEvent", Stress.OnEvent);
Stress.EventFrame:SetScript("OnUpdate", Stress.OnUpdate);
Stress.EventFrame:RegisterEvent("ADDON_LOADED");
Stress.EventFrame:RegisterEvent("PLAYER_LOGIN");
Stress.EventFrame:RegisterEvent("PLAYER_LOGOUT");
