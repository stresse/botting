﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using robotManager.FiniteStateMachine;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Plugin;
using wManager.Wow.Bot.States;
using wManager.Wow.Bot.Tasks;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System.Linq;


// Base CustomProfile Class:
public class CustomProfile : Custom_Profile.ICustomProfile
{
    public void Pulse()
    {
        try
        {
            Treasures.Pulse();
            PluginsManager.LoadAllPlugins();
        }
        catch (Exception e)
        {
            Logging.WriteError("CustomProfile > Pulse(): " + e);
        }
    }

    public void Dispose()
    {
        try
        {
            Treasures.Dispose();
        }
        catch (Exception e)
        {
            Logging.WriteError("CustomProfile > Dispose(): " + e);
        }
    }
}

// Treasures Launch Class:
internal static class Treasures
{
    private static readonly Engine Fsm = new Engine();

    public static bool Pulse()
    {
        try
        {
            var forContinent = new List<int>
            {
                (int) ContinentId.Draenor,
                (int) ContinentId.TanaanJungle,
                (int) ContinentId.SMVAllianceGarrisonLevel1,
                (int) ContinentId.SMVAllianceGarrisonLevel2new,
                (int) ContinentId.SMVAllianceGarrisonLevel2,
                (int) ContinentId.FWHordeGarrisonLevel1,
                (int) ContinentId.FWHordeGarrisonLeve2new,
                (int) ContinentId.FWHordeGarrisonLevel2,
            };
            if (!forContinent.Contains(Usefuls.ContinentId))
            {
                Logging.Write("[Treasures] Please go to Draenor.");
                MessageBox.Show("Please go to Draenor");
                Products.ProductStop();
                return false;
            }

            // Update spell list
            SpellManager.UpdateSpellBook();

            // Load FC:
            wManager.Wow.Helpers.CustomClass.LoadCustomClass();

            // FSM
            Fsm.States.Clear();

            Fsm.AddState(new Pause { Priority = 16 });
            Fsm.AddState(new BattlegrounderCombination { Priority = 15 });

            Fsm.AddState(new Resurrect { Priority = 13 });
            Fsm.AddState(new MyMacro { Priority = 12 });
            Fsm.AddState(new IsAttacked { Priority = 11 });
            Fsm.AddState(new Regeneration { Priority = 10 });
            Fsm.AddState(new Looting { Priority = 9 });
            Fsm.AddState(new Farming { Priority = 8 });
            Fsm.AddState(new MillingState { Priority = 7 });
            Fsm.AddState(new ProspectingState { Priority = 6 });
            Fsm.AddState(new FlightMasterTakeTaxiState { Priority = 5 });
            Fsm.AddState(new FlightMasterDiscoverState { Priority = 5 });

            Fsm.AddState(new ToTown { Priority = 4 });
            Fsm.AddState(new Talents { Priority = 3 });
            Fsm.AddState(new Trainers { Priority = 3 });
            Fsm.AddState(new TreasuresState { Priority = 1 });
            Fsm.AddState(new Idle { Priority = 0 });

            Fsm.States.Sort();
            Fsm.StartEngine(15, "_Treasures");

            StopBotIf.LaunchNewThread();

            // NpcScan:
            NPCScanState.LaunchNewThread();

            Logging.Write("[Treasures] Started.");

            return true;
        }
        catch (Exception e)
        {
            try
            {
                Dispose();
            }
            catch
            {
            }
            Logging.WriteError("Treasures > Pulse(): " + e);
            Products.ProductStop();
            return false;
        }
    }

    public static void Dispose()
    {
        Fsm.StopEngine();
        wManager.Wow.Helpers.CustomClass.DisposeCustomClass();
        Fight.StopFight();
        MovementManager.StopMove();
        FishingTask.StopLoopFish();
        Logging.Write("[Treasures] Stoped");
    }

    public static List<Tuple<int, int, string, int, Vector3, int>> ExtractListFromWowHead()
    {
        var WowheadLocationToMapId = new Dictionary<int, int>
        {
                {6720, 941}, // Frostfire Ridge
                {6721, 949}, // Gorgrond
                {6755, 950}, // Nagrand
                {6719, 947}, // Shadowmoon Valley
                {6722, 948}, // Spires of Arak
                {6662, 946}, // Talador
                {6723, 945}, // Tanaan Jungle
            };
        var dataString = "readonly List<Tuple<string, int, int, Vector3>> _data = new List<Tuple<string, int, int, Vector3>> {" + Environment.NewLine;

        // "id","location","name","type","position","questId"
        var treasures = new List<Tuple<int, int, string, int, Vector3, int>>();

        var tresorsCode = Others.StringBetween(Others.GetRequest("http://www.wowhead.com/objects=-8", ""), "data: [", "}]");
        var listTresors = tresorsCode.Split(new[] { "},{" }, StringSplitOptions.RemoveEmptyEntries);
        foreach (var t in listTresors)
        {
            //Sample: {"id":233641,"location":[6755],"name":"Watertight Bag","type":-8}
            var id = Others.ParseInt(Others.StringBetween(t, "\"id\":", ","));
            var location = Others.ParseInt(Others.StringBetween(t, "\"location\":[", "]"));
            var name = Others.StringBetween(t, "\"name\":\"", "\"");
            var type = Others.ParseInt(Others.StringBetween(t, "\"type\":", ""));

            if (id <= 0)
                continue;

            if (location <= 0 || !WowheadLocationToMapId.ContainsKey(location))
                continue;

            var objectPage = Others.GetRequest("http://www.wowhead.com/object=" + id, "");
            var questId = Others.ParseInt(Others.StringBetween(objectPage, "IsQuestFlaggedCompleted(", ")"));

            if (questId <= 0)
                continue;

            var objectPosString = Others.StringBetween(objectPage, "coords: [[", "]")
                                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (objectPosString.Length != 2)
                continue;

            var posLua = Lua.LuaDoString<string>(string.Format(@" 
                local ret = '';
                SetMapToCurrentZone(); 
                continent = GetCurrentMapContinent(); 
                SetMapZoom(continent); 
                SetMapByID({0});
                SetMapByID({0});
			    local _, worldX, worldY = GetWorldLocFromMapPos({1}/100, {2}/100);
                ret = worldX .. '#' ..  worldY;
                return ret;", WowheadLocationToMapId[location], objectPosString[0], objectPosString[1]))
                             .Split(new[] { '#' }, StringSplitOptions.RemoveEmptyEntries);

            if (posLua.Length != 2)
                continue;

            var position = new Vector3(Others.ParseDouble(posLua[0]), Others.ParseDouble(posLua[1]), 0);
            position.Z = PathFinder.GetZPosition(new Vector3(position));

            if (!string.IsNullOrEmpty(name) && type == -8 && position.DistanceTo2D(Vector3.Zero) > 0 && position.DistanceZ(Vector3.Zero) > 0)
            {
                treasures.Add(new Tuple<int, int, string, int, Vector3, int>(id, location, name, type, position, questId));
                dataString += string.Format("new Tuple<string, int, int, Vector3>(\"{0}\", {1} ,{2} , new Vector3({3}, {4}, {5})),{6}", name.Replace("\"", "\\\""), id, questId, position.X.ToString(CultureInfo.InvariantCulture), position.Y.ToString(CultureInfo.InvariantCulture), position.Z.ToString(CultureInfo.InvariantCulture), Environment.NewLine);
            }
        }

        dataString += " };";

        Logging.WriteDebug(dataString);

        return treasures;
    }
}

internal class TreasuresState : State
{
    readonly List<int> _blackListObjectId = new List<int>
    {
        224750, // Hanging Satchel
        233642, // Abu'Gar's Favorite Lure
        236141, // Discarded Pack
        228020, // Relic of Telmor 
        234618, // Gift of Anzu
        235313, // Abandoned Mining Pick
		233126, // Shadowmoon Treasure
    };
    
    // Name, Object id, Quest Id, Position
    readonly List<Tuple<string, int, int, Vector3>> _data = new List<Tuple<string, int, int, Vector3>> {
        new Tuple<string, int, int, Vector3>("Demonic Cache", 224785, 33575, new Vector3(1293.619, 908.65, 120.3094)),
        new Tuple<string, int, int, Vector3>("Rotting Basket", 224781, 33572, new Vector3(1146.219, 741.15, 97.80453)), 
        new Tuple<string, int, int, Vector3>("Sunken Fishing boat", 233101, 35677, new Vector3(1494.619, -216.95, 27.77732)), 
        new Tuple<string, int, int, Vector3>("A Pile of Dirt" ,233452 ,35951 , new Vector3(2178.02, -2622.25, -658.1528)),
        new Tuple<string, int, int, Vector3>("Aarko's Family Treasure" ,227793 ,34182 , new Vector3(609.1912, 3657.019, 7.876956)),
        new Tuple<string, int, int, Vector3>("Abandoned Cargo" ,233206 ,35759 , new Vector3(2660.483, 5472.03, 82.95664)),
        new Tuple<string, int, int, Vector3>("Abandoned Mining Pick" ,235313 ,36458 , new Vector3(-838.4391, 2220.719, 52.33915)),
        new Tuple<string, int, int, Vector3>("Abu'Gar's Favorite Lure" ,233642 ,36072 , new Vector3(449.4182, -297.3499, 21.91867)),
        new Tuple<string, int, int, Vector3>("Abu'gar's Vitality" ,233157 ,35711 , new Vector3(2611.299, 5574.18, 70.17872)),
        new Tuple<string, int, int, Vector3>("Adventurer's Mace" ,233650 ,36077 , new Vector3(-108.9155, -2809.85, -40.82064)),
        new Tuple<string, int, int, Vector3>("Adventurer's Pack" ,232406 ,35597 , new Vector3(2940.45, 5341.505, 56.73552)),
        new Tuple<string, int, int, Vector3>("Adventurer's Pack" ,233218 ,35765 , new Vector3(2781.55, 4637.805, 181.2675)),
        new Tuple<string, int, int, Vector3>("Adventurer's Pack" ,233511 ,35969 , new Vector3(2955.583, 6720.53, 26.02572)),
        new Tuple<string, int, int, Vector3>("Adventurer's Pouch" ,233623 ,36050 , new Vector3(2164.865, 6096.28, 94.65625)),
        new Tuple<string, int, int, Vector3>("Adventurer's Pouch" ,233658 ,36088 , new Vector3(2494.016, 6277.88, -48.99597)),
        new Tuple<string, int, int, Vector3>("Adventurer's Staff" ,233457 ,35953 , new Vector3(4427.302, 4688.88, 140.1536)),
        new Tuple<string, int, int, Vector3>("Alchemist's Satchel" ,224756 ,35581 , new Vector3(650.4185, -1416.25, -24.13658)),
        new Tuple<string, int, int, Vector3>("Amethyl Crystal" ,227955 ,34236 , new Vector3(3141.269, 2130.62, 98.52029)),
        new Tuple<string, int, int, Vector3>("Appropriated Warsong Supplies" ,233079 ,35673 , new Vector3(2070.282, 5165.58, 186.7395)),
        new Tuple<string, int, int, Vector3>("Aruuna Mining Cart" ,228024 ,34260 , new Vector3(3037.918, 956.0083, 106.0648)),
        new Tuple<string, int, int, Vector3>("Assassin's Spear" ,235143 ,36445 , new Vector3(-123.4333, 1699.523, 15.37603)),
        new Tuple<string, int, int, Vector3>("Bag of Herbs" ,233773 ,36116 , new Vector3(-332.249, -1918.75, 103.9178)),
        new Tuple<string, int, int, Vector3>("Barrel of Fish" ,228016 ,34252 , new Vector3(2521.168, 2112.732, 98.81682)),
        new Tuple<string, int, int, Vector3>("Bone-Carved Dagger" ,233532 ,35986 , new Vector3(3856.018, 4921.555, 97.06129)),
        new Tuple<string, int, int, Vector3>("Bonechewer Remnants" ,228023 ,34259 , new Vector3(1380.342, 3847.819, 178.3899)),
        new Tuple<string, int, int, Vector3>("Bright Coin" ,229354 ,34471 , new Vector3(2386.018, 1456.858, 104.7569)),
        new Tuple<string, int, int, Vector3>("Brilliant Dreampetal" ,233048 ,35661 , new Vector3(3519.301, 4705.905, 144.8199)),
        new Tuple<string, int, int, Vector3>("Brokor's Sack" ,235859 ,36506 , new Vector3(297.5515, -525.1499, -13.42004)),
        new Tuple<string, int, int, Vector3>("Bubbling Cauldron" ,224228 ,33613 , new Vector3(1628.619, -223.6501, 1.825593)),
        new Tuple<string, int, int, Vector3>("Burning Blade Cache" ,233137 ,35696 , new Vector3(2902.617, 4461.88, 182.0675)),
        new Tuple<string, int, int, Vector3>("Burning Pearl" ,230252 ,34642 , new Vector3(7484.046, 4925.897, 130.5755)),
        new Tuple<string, int, int, Vector3>("Campaign Contributions" ,234473 ,36366 , new Vector3(-2284.609, 1317.717, 22.20821)),
        new Tuple<string, int, int, Vector3>("Charred Sword" ,228012 ,34248 , new Vector3(427.085, -2890.25, 115.8916)),
        new Tuple<string, int, int, Vector3>("Coinbender's Payment" ,235299 ,36453 , new Vector3(-2211.896, 535.9229, -118.2362)),
        new Tuple<string, int, int, Vector3>("Crag-Leaper's Cache" ,226983 ,34642 , new Vector3(6982.565, 4895.324, 164.8398)),
        new Tuple<string, int, int, Vector3>("Curious Deathweb Egg" ,227996 ,34239 , new Vector3(970.9165, 1874.233, -29.39136)),
        new Tuple<string, int, int, Vector3>("Discarded Pack" ,236141 ,36625 , new Vector3(-1064.783, -572.05, 4.384167)),
        new Tuple<string, int, int, Vector3>("Draenei Weapons" ,228017 ,34253 , new Vector3(1773.867, 2542.032, 133.8695)),
        new Tuple<string, int, int, Vector3>("Elemental Offering" ,233492 ,35954 , new Vector3(1793.886, -2213.55, -43.28041)),
        new Tuple<string, int, int, Vector3>("Elixir of Shadow Sight" ,234703 ,36381 , new Vector3(777.3929, 2020.726, 170.6086)),
        new Tuple<string, int, int, Vector3>("Elixir of Shadow Sight" ,234704 ,36381 , new Vector3(385.5537, 2026.786, 4.653019)),
        new Tuple<string, int, int, Vector3>("Elixir of Shadow Sight" ,234705 ,36381 , new Vector3(-365.8082, 487.4395, 73.59894)),
        new Tuple<string, int, int, Vector3>("Elixir of Shadow Sight" ,234734 ,36381 , new Vector3(-1141.408, 1717.705, 48.11575)),
        new Tuple<string, int, int, Vector3>("Elixir of Shadow Sight" ,234735 ,36381 , new Vector3(490.5828, 1311.656, 82.36712)),
        new Tuple<string, int, int, Vector3>("Elixir of Shadow Sight" ,234736 ,36381 , new Vector3(-2026.076, 1463.167, -7.762097)),
        new Tuple<string, int, int, Vector3>("Ephial's Dark Grimoire" ,235097 ,36418 , new Vector3(-951.5475, 2475.257, 30.33915)),
        new Tuple<string, int, int, Vector3>("Explorer Canister" ,236139 ,36621 , new Vector3(5414.88, 1763.125, 85.80937)),
        new Tuple<string, int, int, Vector3>("False-Bottomed Jar" ,224783 ,33037 , new Vector3(1079.219, -1201.85, -40.13309)),
        new Tuple<string, int, int, Vector3>("Fantastic Fish" ,227743 ,34174 , new Vector3(2405.82, 493.2501, 0.7308061)),
        new Tuple<string, int, int, Vector3>("Farmer's Bounty" ,228013 ,34249 , new Vector3(597.2661, 3722.606, 1.708618)),
        new Tuple<string, int, int, Vector3>("Foreman's Lunchbox" ,227956 ,34238 , new Vector3(3288.344, 2410.857, 114.2657)),
        new Tuple<string, int, int, Vector3>("Forgotten Supplies" ,230909 ,34642 , new Vector3(6012.221, 4834.179, 139.3876)),
        new Tuple<string, int, int, Vector3>("Fractured Sunstone" ,234157 ,36401 , new Vector3(490.5828, 1626.798, 70.7604)),
        new Tuple<string, int, int, Vector3>("Fragment of Oshu'gun" ,233560 ,36020 , new Vector3(2414.566, 6709.18, 9.159419)),
        new Tuple<string, int, int, Vector3>("Freshwater Clam" ,233132 ,35692 , new Vector3(4105.719, 5159.905, 38.66129)),
        new Tuple<string, int, int, Vector3>("Frozen Frostwolf Axe" ,229640 ,34642 , new Vector3(6293.539, 6026.521, 89.556)),
        new Tuple<string, int, int, Vector3>("Frozen Orc Skeleton" ,229367 ,34642 , new Vector3(6150.841, 4008.71, 109.6234)),
        new Tuple<string, int, int, Vector3>("Fungus-Covered Chest" ,233044 ,35660 , new Vector3(4234.352, 4263.255, 0.411098)),
        new Tuple<string, int, int, Vector3>("Gambler's Purse" ,233649 ,36074 , new Vector3(3140.967, 5029.38, 86.63517)),
        new Tuple<string, int, int, Vector3>("Garrison Supplies" ,235103 ,36420 , new Vector3(-531.431, 2426.774, 17.9408)),
        new Tuple<string, int, int, Vector3>("Garrison Workman's Hammer" ,235289 ,36451 , new Vector3(-652.6184, 2147.994, 7.148508)),
        new Tuple<string, int, int, Vector3>("Genedar Debris" ,233539 ,35987 , new Vector3(2751.283, 6851.055, 17.81081)),
        new Tuple<string, int, int, Vector3>("Genedar Debris" ,233549 ,35999 , new Vector3(2649.133, 6578.655, 14.43544)),
        new Tuple<string, int, int, Vector3>("Genedar Debris" ,233551 ,36002 , new Vector3(2372.949, 6777.28, 20.83545)),
        new Tuple<string, int, int, Vector3>("Genedar Debris" ,233555 ,36008 , new Vector3(2172.432, 6550.28, 36.03545)),
        new Tuple<string, int, int, Vector3>("Genedar Debris" ,233557 ,36011 , new Vector3(2342.682, 6170.055, 65.05625)),
        new Tuple<string, int, int, Vector3>("Gift of Anzu" ,234618 ,36381 , new Vector3(-410.2437, 1735.886, 46.11129)),
        new Tuple<string, int, int, Vector3>("Glowing Cave Mushroom" ,233241 ,35798 , new Vector3(543.2183, -994.1499, -8.315096)),
        new Tuple<string, int, int, Vector3>("Glowing Obsidian Shard" ,230253 ,34642 , new Vector3(7345.426, 4387.814, 149.2646)),
        new Tuple<string, int, int, Vector3>("Gnawed Bone" ,230425 ,34642 , new Vector3(7443.276, 5953.146, 125.6833)),
        new Tuple<string, int, int, Vector3>("Goblin Pack" ,232571 ,35576 , new Vector3(2111.898, 6629.73, 31.1622)),
        new Tuple<string, int, int, Vector3>("Golden Kaliri Egg" ,233134 ,35694 , new Vector3(2932.883, 6005.48, 95.80192)),
        new Tuple<string, int, int, Vector3>("Goren Leftovers" ,226996 ,34642 , new Vector3(7202.728, 3427.825, 260.8)),
        new Tuple<string, int, int, Vector3>("Grimfrost Treasure" ,226994 ,34642 , new Vector3(6407.697, 3336.106, 158.3229)),
        new Tuple<string, int, int, Vector3>("Grizzlemaw's Bonepile" ,233626 ,36051 , new Vector3(2164.865, 4365.405, 216.1585)),
        new Tuple<string, int, int, Vector3>("Hanging Satchel" ,224750 ,33564 , new Vector3(601.2852, -886.95, -1.048987)),
        new Tuple<string, int, int, Vector3>("Hidden Stash" ,232986 ,35622 , new Vector3(3224.2, 4337.03, 145.0351)),
        new Tuple<string, int, int, Vector3>("Highmaul Sledge" ,233611 ,36039 , new Vector3(3065.3, 5483.38, 14.59871)),
        new Tuple<string, int, int, Vector3>("Horned Skull" ,231644 ,35056 , new Vector3(7038.98, 1583.23, 3.085174)),
        new Tuple<string, int, int, Vector3>("Important Exploration Supplies" ,233696 ,36099 , new Vector3(2437.266, 5040.73, 216.2057)),
        new Tuple<string, int, int, Vector3>("Iron Box" ,228015 ,34251 , new Vector3(1280.967, 1981.557, 335.9706)),
        new Tuple<string, int, int, Vector3>("Iron Horde Cargo Shipment" ,227134 ,33041 , new Vector3(-77.64868, -551.95, 23.57691)),
        new Tuple<string, int, int, Vector3>("Iron Horde Explosives" ,235141 ,36444 , new Vector3(341.1184, 1626.798, 21.38294)),
        new Tuple<string, int, int, Vector3>("Iron Horde Munitions" ,236693 ,34642 , new Vector3(5347.656, 4039.283, 132.6101)),
        new Tuple<string, int, int, Vector3>("Iron Horde Tribute" ,224755 ,33567 , new Vector3(11.68457, -237.05, 90.52822)),
        new Tuple<string, int, int, Vector3>("Jug of Aged Ironwine" ,227953 ,34233 , new Vector3(3979.994, 1933.858, -1.20886)),
        new Tuple<string, int, int, Vector3>("Kaliri Egg" ,232579 ,33568 , new Vector3(637.0184, -1610.55, -2.44525)),
        new Tuple<string, int, int, Vector3>("Keluu's Belongings" ,228025 ,34261 , new Vector3(2783.518, 1319.72, 168.3582)),
        new Tuple<string, int, int, Vector3>("Ketya's Stash" ,228570 ,34290 , new Vector3(3332.069, 2613.582, 3.46572)),
        new Tuple<string, int, int, Vector3>("Light of the Sea" ,228022 ,34258 , new Vector3(3936.27, 3561.619, 2.018263)),
        new Tuple<string, int, int, Vector3>("Lightbearer" ,227527 ,34101 , new Vector3(2199.193, 1731.133, 119.0244)),
        new Tuple<string, int, int, Vector3>("Lost Herb Satchel" ,234159 ,36247 , new Vector3(223.9705, 1608.617, 6.58294)),
        new Tuple<string, int, int, Vector3>("Lost Pendant" ,233651 ,36082 , new Vector3(2751.283, 5801.18, 88.52789)),
        new Tuple<string, int, int, Vector3>("Lost Ring" ,235091 ,36411 , new Vector3(-74.9585, 1784.369, 20.011)),
        new Tuple<string, int, int, Vector3>("Lucky Coin" ,230402 ,34642 , new Vector3(7785.75, 6332.25, 11.11796)),
        new Tuple<string, int, int, Vector3>("Luminous Shell" ,227954 ,34235 , new Vector3(3260.519, 2703.02, 0.4648669)),
        new Tuple<string, int, int, Vector3>("Mysterious Mushrooms" ,235300 ,36454 , new Vector3(-1339.347, 826.823, 44.7061)),
        new Tuple<string, int, int, Vector3>("Norana's Cache" ,239194 ,34116 , new Vector3(1579.092, 2196.207, 139.0108)),
        new Tuple<string, int, int, Vector3>("Ockbar's Pack" ,227998 ,34241 , new Vector3(4526.53, 1542.344, 145.0038)),
        new Tuple<string, int, int, Vector3>("Odd Skull" ,236715 ,36509 , new Vector3(5938.08, 781.875, 44.82548)),
        new Tuple<string, int, int, Vector3>("Offering to the Raven Mother" ,234744 ,36403 , new Vector3(-862.6766, 1451.046, 76.58493)),
        new Tuple<string, int, int, Vector3>("Offering to the Raven Mother" ,234748 ,36406 , new Vector3(-826.3202, 1717.705, 72.54191)),
        new Tuple<string, int, int, Vector3>("Offering to the Raven Mother" ,235073 ,36407 , new Vector3(-1226.239, 1535.892, 107.67)),
        new Tuple<string, int, int, Vector3>("Offering to the Raven Mother" ,235090 ,36410 , new Vector3(-1193.922, 984.3938, 22.2645)),
        new Tuple<string, int, int, Vector3>("Ogre Beads" ,233618 ,36049 , new Vector3(1903.815, 4717.255, 336.6294)),
        new Tuple<string, int, int, Vector3>("Ogron Plunder" ,234432 ,36340 , new Vector3(-1052.537, 1123.783, 58.74848)),
        new Tuple<string, int, int, Vector3>("Orc Skeleton" ,235860 ,36507 , new Vector3(1168.552, -2220.25, 1.423193)),
        new Tuple<string, int, int, Vector3>("Orcish Signaling Horn" ,234740 ,36402 , new Vector3(-208.2645, 2481.318, 32.7408)),
        new Tuple<string, int, int, Vector3>("Outcast's Belongings" ,234147 ,36243 , new Vector3(688.5222, 2451.015, 236.6827)),
        new Tuple<string, int, int, Vector3>("Outcast's Belongings" ,235172 ,36447 , new Vector3(506.7411, 2129.813, 35.81601)),
        new Tuple<string, int, int, Vector3>("Pale Elixir" ,233768 ,36115 , new Vector3(2569.683, 6028.18, 35.05625)),
        new Tuple<string, int, int, Vector3>("Pale Loot Sack" ,230611 ,34642 , new Vector3(6207.92, 6185.5, 98.2651)),
        new Tuple<string, int, int, Vector3>("Peaceful Offering" ,225501 ,33610 , new Vector3(-37.44849, -759.6499, 23.19507)),
        new Tuple<string, int, int, Vector3>("Peaceful Offering" ,225502 ,33611 , new Vector3(-46.38208, -665.8499, 17.61443)),
        new Tuple<string, int, int, Vector3>("Peaceful Offering" ,225503 ,33612 , new Vector3(16.15137, -706.05, 19.5153)),
        new Tuple<string, int, int, Vector3>("Petrified Rylak Egg" ,235881 ,36521 , new Vector3(7251.53, 1288.854, -17.8069)),
        new Tuple<string, int, int, Vector3>("Pokkar's Thirteenth Axe" ,233561 ,36021 , new Vector3(2675.616, 5999.805, 43.2399)),
        new Tuple<string, int, int, Vector3>("Polished Saberon Skull" ,233593 ,36035 , new Vector3(2615.083, 5182.605, 108.6057)),
        new Tuple<string, int, int, Vector3>("Pure Crystal Dust" ,228026 ,34263 , new Vector3(3840.869, 1164.696, -63.23313)),
        new Tuple<string, int, int, Vector3>("Raided Loot" ,231103 ,34642 , new Vector3(5861.369, 5231.626, 93.99796)),
        new Tuple<string, int, int, Vector3>("Relic of Aruuna" ,228014 ,34250 , new Vector3(2648.368, 1313.758, 189.7405)),
        new Tuple<string, int, int, Vector3>("Relic of Telmor" ,228020 ,34256 , new Vector3(784.0913, 3030.957, 141.8862)),
        new Tuple<string, int, int, Vector3>("Remains of Balik Orecrusher" ,236170 ,36654 , new Vector3(5534.78, 724.6353, 78.82859)),
        new Tuple<string, int, int, Vector3>("Remains of Balldir Deeprock" ,236096 ,36605 , new Vector3(6537.58, 340.312, 63.11237)),
        new Tuple<string, int, int, Vector3>("Rooby's Roo" ,233975 ,36798 , new Vector3(-664.7372, 2420.713, 21.53915)),
        new Tuple<string, int, int, Vector3>("Rook's Tacklebox" ,227951 ,34232 , new Vector3(3896.52, 1963.67, 151.9207)),
        new Tuple<string, int, int, Vector3>("Rusted Lockbox" ,228483 ,34276 , new Vector3(1042.467, 1904.045, -2.591359)),
        new Tuple<string, int, int, Vector3>("Saberon Stash" ,233697 ,36102 , new Vector3(2463.749, 5040.73, 216.1101)),
        new Tuple<string, int, int, Vector3>("Sasha's Secret Stash" ,236149 ,36631 , new Vector3(5878.13, 1877.605, 81.28403)),
        new Tuple<string, int, int, Vector3>("Scaly Rylak Egg" ,224753 ,33565 , new Vector3(-1104.983, -2226.95, 21.39442)),
        new Tuple<string, int, int, Vector3>("Sealed Jug" ,230401 ,34642 , new Vector3(6424.006, 6907.021, 2.356809)),
        new Tuple<string, int, int, Vector3>("Sethekk Ritual Brew" ,235282 ,36450 , new Vector3(-575.8665, 341.9893, 27.829)),
        new Tuple<string, int, int, Vector3>("Shadowmoon Exile Treasure" ,224770 ,33570 , new Vector3(1561.619, -799.8501, -24.13376)),
        new Tuple<string, int, int, Vector3>("Shadowmoon Treasure" ,233126 ,33571 , new Vector3(905.0186, 372.65, 57.05927)),
        new Tuple<string, int, int, Vector3>("Shattered Hand Cache" ,234458 ,36362 , new Vector3(219.9309, 1275.294, 1.567121)),
        new Tuple<string, int, int, Vector3>("Shattered Hand Lockbox" ,234456 ,36361 , new Vector3(143.1788, 1778.309, 24.58294)),
        new Tuple<string, int, int, Vector3>("Shredder Parts" ,235310 ,36456 , new Vector3(-2034.155, 990.4541, 1.193109)),
        new Tuple<string, int, int, Vector3>("Slave's Stash" ,224392 ,34642 , new Vector3(6530.01, 5818.626, 142.5661)),
        new Tuple<string, int, int, Vector3>("Smuggler's Cache" ,236633 ,36857 , new Vector3(3670.634, 4251.905, 179.2099)),
        new Tuple<string, int, int, Vector3>("Sniper's Crossbow" ,236158 ,36634 , new Vector3(7267.88, 1386.979, 84.79122)),
        new Tuple<string, int, int, Vector3>("Snow-Covered Strongbox" ,230424 ,34642 , new Vector3(7744.979, 6038.751, 33.10094)),
        new Tuple<string, int, int, Vector3>("Soulbinder's Reliquary" ,228018 ,34254 , new Vector3(2238.943, 3484.107, 64.30066)),
        new Tuple<string, int, int, Vector3>("Spray-O-Matic 5000 XT" ,234471 ,36365 , new Vector3(-1900.849, 1063.179, 19.99311)),
        new Tuple<string, int, int, Vector3>("Stashed Emergency Rucksack" ,236092 ,36604 , new Vector3(4499.28, 1133.49, 163.4038)),
        new Tuple<string, int, int, Vector3>("Steamwheedle Supplies" ,232584 ,35577 , new Vector3(1813.014, 6465.155, -18.8378)),
        new Tuple<string, int, int, Vector3>("Steamwheedle Supplies" ,232595 ,35583 , new Vector3(1892.465, 6317.605, -5.047197)),
        new Tuple<string, int, int, Vector3>("Steamwheedle Supplies" ,232598 ,35591 , new Vector3(2959.367, 4893.18, 129.25)),
        new Tuple<string, int, int, Vector3>("Steamwheedle Supplies" ,232985 ,35616 , new Vector3(3307.434, 4297.305, 144.8199)),
        new Tuple<string, int, int, Vector3>("Steamwheedle Supplies" ,233033 ,35646 , new Vector3(4219.219, 5301.78, 63.86129)),
        new Tuple<string, int, int, Vector3>("Steamwheedle Supplies" ,233034 ,35648 , new Vector3(4260.835, 5642.28, 113.7238)),
        new Tuple<string, int, int, Vector3>("Steamwheedle Supplies" ,233052 ,35662 , new Vector3(4154.902, 4337.03, 0.2516416)),
        new Tuple<string, int, int, Vector3>("Stolen Treasure" ,232067 ,35280 , new Vector3(2553.22, 453.05, 4.39792)),
        new Tuple<string, int, int, Vector3>("Strange Looking Dagger" ,231069 ,34940 , new Vector3(5229.58, 732.8125, 98.08203)),
        new Tuple<string, int, int, Vector3>("Sun-Touched Cache" ,235104 ,36421 , new Vector3(276.4851, 2614.647, 38.2518)),
        new Tuple<string, int, int, Vector3>("Sunken Treasure" ,232066 ,35279 , new Vector3(2343.286, 339.15, 13.23989)),
        new Tuple<string, int, int, Vector3>("Supply Dump" ,226990 ,34642 , new Vector3(6244.614, 6521.803, 126.8781)),
        new Tuple<string, int, int, Vector3>("Survivalist's Cache" ,226993 ,34642 , new Vector3(7227.19, 3550.117, 169.8853)),
        new Tuple<string, int, int, Vector3>("Telaar Defender Shield" ,233613 ,36046 , new Vector3(2433.482, 5636.605, 151.7787)),
        new Tuple<string, int, int, Vector3>("Teroclaw Nest" ,230643 ,35162 , new Vector3(1340.592, 3490.069, 135.9191)),
        new Tuple<string, int, int, Vector3>("Thunderlord Cache" ,220641 ,34642 , new Vector3(7320.963, 5415.063, 63.80998)),
        new Tuple<string, int, int, Vector3>("Toxicfang Venom" ,234461 ,36364 , new Vector3(70.46643, 1390.442, 7.587349)),
        new Tuple<string, int, int, Vector3>("Treasure of Ango'rosh" ,228021 ,34257 , new Vector3(1074.267, 3549.694, 135.4001)),
        new Tuple<string, int, int, Vector3>("Treasure of Kull'krosh" ,230725 ,34760 , new Vector3(2251.882, 7168.855, 0.2748018)),
        new Tuple<string, int, int, Vector3>("Vindicator's Cache" ,224784 ,33574 , new Vector3(-872.7161, -1154.95, 69.9517)),
        new Tuple<string, int, int, Vector3>("Vindicator's Hammer" ,236147 ,36628 , new Vector3(6117.93, 209.4785, 1.00299)),
        new Tuple<string, int, int, Vector3>("Void-Infused Crystal" ,232590 ,35579 , new Vector3(2410.782, 6470.83, 37.23544)),
        new Tuple<string, int, int, Vector3>("Warm Goren Egg" ,234054 ,36203 , new Vector3(7011.73, 1068.073, 28.20876)),
        new Tuple<string, int, int, Vector3>("Warsong Cache" ,233135 ,35695 , new Vector3(2637.783, 6374.355, -46.14375)),
        new Tuple<string, int, int, Vector3>("Warsong Helm" ,233645 ,36073 , new Vector3(3243.117, 6334.63, 3.330285)),
        new Tuple<string, int, int, Vector3>("Warsong Lockbox" ,233103 ,35678 , new Vector3(2259.449, 5159.905, 231.4057)),
        new Tuple<string, int, int, Vector3>("Warsong Spoils" ,232599 ,35593 , new Vector3(2630.216, 4734.28, 184.1782)),
        new Tuple<string, int, int, Vector3>("Warsong Supplies" ,233521 ,35976 , new Vector3(2433.482, 4234.88, 241.3012)),
        new Tuple<string, int, int, Vector3>("Waterlogged Chest" ,224754 ,33566 , new Vector3(-1087.116, -357.6499, 2.364629)),
        new Tuple<string, int, int, Vector3>("Waterlogged Satchel" ,235307 ,36071 , new Vector3(-894.9933, 657.1313, -19.44117)),
        new Tuple<string, int, int, Vector3>("Weapons Cache" ,235869 ,36596 , new Vector3(7213.38, 1035.365, 38.95135)),
        new Tuple<string, int, int, Vector3>("Webbed Sac" ,228019 ,34255 , new Vector3(907.3167, 1933.858, -29.39136)),
 };

    public override string DisplayName
    {
        get { return "Treasures"; }
    }

    public override int Priority
    {
        get { return _priority; }
        set { _priority = value; }
    }
    private int _priority;

    public override bool NeedToRun
    {
        get
        {
            if (!Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause ||
                Conditions.IsAttackedAndCannotIgnore)
                return false;

            return true;
        }
    }

    public override List<State> NextStates
    {
        get { return new List<State>(); }
    }

    public override List<State> BeforeStates
    {
        get { return new List<State>(); }
    }

    private int _lastContinent;
    public override void Run()
    {
        foreach (var treasure in _data.OrderBy(t => t.Item4.DistanceTo(ObjectManager.Me.Position)))
        {
            if (!Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause ||
                Conditions.IsAttackedAndCannotIgnore)
                return;

            if (_blackListObjectId.Contains(treasure.Item2))
                continue;

            if (Lua.LuaDoString<bool>(string.Format("return IsQuestFlaggedCompleted({0});", treasure.Item3)))
                continue;

            Logging.Write(string.Format("[Treasures] Go farm {0} (distance: {1})", treasure.Item1, treasure.Item4.DistanceTo(ObjectManager.Me.Position)));
            
            _lastContinent = Usefuls.ContinentId;
            if (!GoToTask.ToPositionAndIntecractWithGameObject(treasure.Item4, treasure.Item2, -1,
                    wManager.wManagerSetting.CurrentSetting.BlackListIfNotCompletePath))
            {
                if (ObjectManager.Me.InCombatFlagOnly && ObjectManager.Me.IsMounted && treasure.Item4.DistanceTo2D(ObjectManager.Me.Position) < 20)
                    MountTask.DismountMount();

                if (_lastContinent == Usefuls.ContinentId &&
                    Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause &&
                    !Conditions.IsAttackedAndCannotIgnore)
                {
                    Logging.Write(string.Format("[Treasures] Blacklist {0} (id: {1}), cannot reach it.", treasure.Item1, treasure.Item2));
                    _blackListObjectId.Add(treasure.Item2);
                }
                return;
            }
            Usefuls.WaitIsCastingAndLooting();
        }
    }
}