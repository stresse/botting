﻿using System;
using System.Collections.Generic;
using System.Globalization;
using robotManager.FiniteStateMachine;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Plugin;
using wManager.Wow.Bot.States;
using wManager.Wow.Bot.Tasks;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System.Linq;
using System.Diagnostics;

#if VISUAL_STUDIO
namespace TanaanHellbaneDaily
{
#endif
	public class CustomProfile : Custom_Profile.ICustomProfile
	{
		public void Pulse()
		{
			try
			{
				TanaanHellbaneDaily.Pulse();
				PluginsManager.LoadAllPlugins();
			}
			catch (Exception e)
			{
				Logging.WriteError("CustomProfile > Pulse(): " + e);
			}
		}

		public void Dispose()
		{
			try
			{
				//Treasures.Dispose();
			}
			catch (Exception e)
			{
				Logging.WriteError("CustomProfile > Dispose(): " + e);
			}
		}
	}

	internal static class TanaanHellbaneDaily
	{
		// SETTINGS
		private static readonly string _profileName = "[Tanaan Hellbane Daily]";
		private static readonly string _engineName = "_TanaanHellbaneDaily";


		private static readonly Engine Fsm = new Engine();
		public static bool Pulse()
		{
			try
			{
				var forContinent = new List<int>
			{
				(int) ContinentId.Draenor,
				(int) ContinentId.TanaanJungle,
				(int) ContinentId.SMVAllianceGarrisonLevel1,
				(int) ContinentId.SMVAllianceGarrisonLevel2new,
				(int) ContinentId.SMVAllianceGarrisonLevel2,
				(int) ContinentId.FWHordeGarrisonLevel1,
				(int) ContinentId.FWHordeGarrisonLeve2new,
				(int) ContinentId.FWHordeGarrisonLevel2,
			};
				if (!forContinent.Contains(Usefuls.ContinentId))
				{
					Logging.Write(_profileName + " Please go to Draenor.");
					Products.ProductStop();
					return false;
				}

				var state = new TanaanHellbaneDailyState { Priority = 1 };
				CustomProfileLibrary.PulseDefault(Fsm, state, _engineName);
				Logging.Write(_profileName + " Started.");

				return true;
			}
			catch (Exception e)
			{
				try
				{
					Dispose();
				}
				catch
				{
				}
				Logging.WriteError(_profileName + " > Pulse(): " + e);
				Products.ProductStop();
				return false;
			}
		}

		public static void Dispose()
		{
			CustomProfileLibrary.DisposeDefault(Fsm);
			Logging.Write(_profileName + " Stoped");
		}



	}

	internal class TanaanHellbaneDailyState : State
	{
		//custom settings
		private bool _needToRun = true;
		private float _range = 5f;

		public override string DisplayName { get { return "TanaanHellbaneDaily"; } }
		public override int Priority { get; set; }

		public override bool NeedToRun
		{
			get
			{
				if (!Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause || Conditions.IsAttackedAndCannotIgnore)
					return false;

				return _needToRun;
			}
		}

		public override List<State> NextStates
		{
			get { return new List<State>(); }
		}

		public override List<State> BeforeStates
		{
			get { return new List<State>(); }
		}

		public override void Run()
		{
			if (!Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause || Conditions.IsAttackedAndCannotIgnore)
				return;

			//CustomProfileLibrary.TryLootAround();

			foreach (var mobInfo in KillList.Mobs)
			{
				var questId = mobInfo.Key;
				var mob = mobInfo.Value;
				//already killed today, try to next
				if (Quest.GetQuestCompleted(questId))
				{
					//Logging.Write("Already killed: " + mob.name);
					continue;
				}
				if (CustomProfileLibrary.TryKill(mob))
					return;

				CustomProfileLibrary.TryAntiAfk();
				return;
			}
			// seems like all mobs killd. stop
			Logging.Write("Profile complete");
			_needToRun = false;
		}
	}

	internal class KillList
	{
		/// <summary>
		/// mobs list: questId, mob
		/// </summary>
		/// <typeparam name="questId"></typeparam>
		/// <typeparam name="mob"></typeparam>
		/// <returns></returns>
		public static Dictionary<int, MobToKill> Mobs = new Dictionary<int, MobToKill>()
	{
		/*
		{
			-39287, new MobToKill("TestMOB", 93003, new Vector3(3770.632, 230.7031, 68.66753, "Flying"))
		},
		//*/
		{
			39287, new MobToKill("Deathtalon", 95053, new Vector3(4534.897, 470.0904, 255.3821, "Flying"))
		},
		{
			39288, new MobToKill("Terrorfist", 95044, new Vector3(3728.102, 941.2968, 293.5783, "Flying"))
		},
		{
			39290, new MobToKill("Vengeance", 95054, new Vector3(3381.605, 30.24771, 217.7738, "Flying"))
		},
		{
			39289, new MobToKill("Doomroller", 95053, new Vector3(4097.221, -760.3251, 95.26067, "Flying"))
		}
	};

	}


	/// <summary>
	/// mob info
	/// </summary>
	internal class MobToKill
	{
		public string name;
		public int id;
		public Vector3 position;

		public MobToKill()
		{
		}

		public MobToKill(string mobName, int mobId, Vector3 mobPosition)
		{
			name = mobName;
			id = mobId;
			position = mobPosition;
		}

	}

	/// <summary>
	/// Library for Custom profiles. Regular routines and functions. Can be used in any other profiles
	/// </summary>
	internal static class CustomProfileLibrary
	{
		static Random MyRandom = new Random();
		static Stopwatch StopWatch = Stopwatch.StartNew();
		static double NextAntiAfkTime = 0;
		static double NextSearchAndKill = 0;

		/// <summary>
		/// move, search, attack every 0.5-1sec
		/// </summary>
		/// <param name="mob"></param>
		public static bool TryKill(MobToKill mob)
		{
			if (StopWatch.Elapsed.TotalMilliseconds < NextSearchAndKill)
				return true;

			NextSearchAndKill = StopWatch.Elapsed.TotalMilliseconds + 500 + (MyRandom.NextDouble() * 500);
			var mobEntry = ObjectManager.GetWoWUnitByEntry(mob.id).FirstOrDefault();
			//mob found. move and attack
			if (mobEntry != null && mobEntry.IsAlive && mobEntry.IsAttackable && mobEntry.IsValid)
			{
				//mob around, but too far, move close
				if (mobEntry.GetDistance > 5)
				{
					if (ObjectManager.Me.SpeedMoving <= 0)
					{
						//Logging.Write("Mob found: " + mob.name + ". But not close enought. Moving closer");
						var wp = PathFinder.FindPath(mobEntry.Position);
						MovementManager.Go(wp);
					}
				}
				//move near, attack
				else
				{
					if (ObjectManager.Me.IsMounted)
					{
						Logging.Write("Mob near: " + mob.name + ". Dismount");
						MountTask.DismountMount();
					}
					else
					{
						Logging.Write("Mob near: " + mob.name + ". Attacking");
						Fight.StartFight(mobEntry.Guid);
					}
				}
				return true;
			}

			//mob not found. move to hover position
			var range = 5f;
			if (mob.position.DistanceTo(ObjectManager.Me.Position) > range)
			{
				//random position inside range
				var randNearPosition = mob.position;
				randNearPosition.X += (float)MyRandom.NextDouble() * (range + range) - range;
				randNearPosition.Y += (float)MyRandom.NextDouble() * (range + range) - range;
				randNearPosition.Z += (float)MyRandom.NextDouble() * (range + range) - range;
				Logging.Write("Too far from camp point: " + mob.name + ". Moving to point: " + randNearPosition.X + "," + randNearPosition.Y + "," + randNearPosition.Z + "");
				GoToTask.ToPosition(randNearPosition);
				return true;
			}
			return false;
		}


		/// <summary>
		/// AntiAfk.Pulse every 3-5min
		/// </summary>
		public static void TryAntiAfk()
		{
			//		if (StopWatch == null || !StopWatch.IsRunning)
			//			StopWatch = Stopwatch.StartNew();

			if (StopWatch.Elapsed.TotalSeconds < NextAntiAfkTime)
				return;

			//current elapsed + 3min + 0-2min;
			NextAntiAfkTime = StopWatch.Elapsed.TotalSeconds + (3 * 60) + (MyRandom.NextDouble() * 120);
			Logging.Write("Anti AFK next: " + NextAntiAfkTime + "");
			AntiAfk.Pulse();
		}


		/// <summary>
		/// Try to loot everything around
		/// </summary>
		public static void TryLootAround()
		{
			//try loot first
			var lootable = ObjectManager.GetWoWUnitLootable();
			if (lootable.Count > 0)
				LootingTask.Pulse(lootable);
		}

		/// <summary>
		/// Prepare and launch regular custom profile pulse
		/// </summary>
		/// <param name="Fsm"></param>
		/// <param name="customState"></param>
		/// <param name="engineName"></param>
		public static void PulseDefault(Engine Fsm, State customState, string engineName)
		{
			// Update spell list
			SpellManager.UpdateSpellBook();

			// Load FC:
			wManager.Wow.Helpers.CustomClass.LoadCustomClass();

			// FSM
			Fsm.States.Clear();

			Fsm.AddState(new wManager.Wow.Bot.States.Relogger() { Priority = 200 });

			Fsm.AddState(new Pause { Priority = 16 });
			Fsm.AddState(new BattlegrounderCombination { Priority = 15 });

			Fsm.AddState(new Resurrect { Priority = 13 });
			Fsm.AddState(new MyMacro { Priority = 12 });
			Fsm.AddState(new IsAttacked { Priority = 11 });
			Fsm.AddState(new Regeneration { Priority = 10 });
			Fsm.AddState(new Looting { Priority = 9 });
			Fsm.AddState(new Farming { Priority = 8 });
			Fsm.AddState(new MillingState { Priority = 7 });
			Fsm.AddState(new ProspectingState { Priority = 6 });
			Fsm.AddState(new FlightMasterTakeTaxiState { Priority = 5 });
			Fsm.AddState(new FlightMasterDiscoverState { Priority = 5 });

			Fsm.AddState(new ToTown { Priority = 4 });
			Fsm.AddState(new Talents { Priority = 3 });
			Fsm.AddState(new Trainers { Priority = 3 });
			Fsm.AddState(customState);
			Fsm.AddState(new Idle { Priority = 0 });

			Fsm.States.Sort();
			Fsm.StartEngine(15, engineName);

			StopBotIf.LaunchNewThread();

			// NpcScan:
			NPCScanState.LaunchNewThread();
		}

		/// <summary>
		/// Regular dispose
		/// </summary>
		/// <param name="Fsm"></param>
		public static void DisposeDefault(Engine Fsm)
		{
			Fsm.StopEngine();
			CustomClass.DisposeCustomClass();
			Fight.StopFight();
			MovementManager.StopMove();
			Logging.Write("[Custom Profile] Stoped");
		}
	}
#if VISUAL_STUDIO
}
#endif
