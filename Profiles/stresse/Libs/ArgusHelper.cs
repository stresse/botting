﻿#if VISUAL_STUDIO
using robotManager.Helpful;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using wManager.Wow.Bot.Tasks;
using wManager.Wow.Class;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager.Wow.Enums;
#endif

public class ArgusHelper
{
	//map id: macaree=1170, antoranwastes=1171, krokuun=1184
	//map id vendicar: krokuun 1135, antoran wastes 1171,
	//area id. krokuun 8574, antoranwastes 8899, 
	public static int AREA_KROKUUN = 8574;
	public static int AREA_ANTORANWASTES = 8899;
	public static int AREA_MACAREE = 8701;
	public static int MAP_KROKUUN = 1184;
	public static int MAP_ANTORAN_WASTES = 1171;
	public static int MAP_MACAREE = 1170;
	//
	static int DalaranPortalID = 273784; //273784 // 273784
										 //
	static int AntoranWastesVeiledDen = 48202; // http://www.wowhead.com/quest=48202/reinforce-the-veiled-den or http://www.wowhead.com/quest=48929/sizing-up-the-opposition
	static int AntoranWastesLightsPurchase = 48201; // http://www.wowhead.com/quest=48201/reinforce-lights-purchase or http://www.wowhead.com/quest=47473/sizing-up-the-opposition

	static ArgusHelper()
	{
		ResetSettings();
	}
	public static bool InArgus { get { return Usefuls.ContinentId == (int)ContinentId.Argus_1; } }
	public static bool ToArgus()
	{
		if (InArgus)
		{
			Log("Bot is in Argus.");
			return true;
		}
		if (LegionQuests.InDalaran)
		{
			Log("Bot is in Dalaran. Going to use teleporter to get to Argus.");
			Teleporter.DalaranToArgus.Use();
			return false;
		}
		Log("Bot isn't in Argus, it isn't even in Dalaran, so using Dalaran Hearthstone...");
		LegionQuests.UseDalaranHeathstone();
		return false;
	}
	public static bool FromArgus()
	{
		if (!InArgus)
		{
			Log("Bot is not in Argus.");
			return true;
		}
		if (InVendicarAntoranWastes)
		{
			Log("Bot is in the Vendicar (Anrotan Wastes). Will use portal to get to Dalaran.");
			GoToTask.ToPositionAndIntecractWithGameObject(Positions.DalaranPortalAntoranWastes, DalaranPortalID);
			return false;
		}
		if (InVendicarKrokuun)
		{
			Log("Bot is in the Vendicar (Krokuun). Will use portal to get to Dalaran.");
			GoToTask.ToPositionAndIntecractWithGameObject(Positions.DalaranPortalKrokuun, DalaranPortalID);
			return false;
		}
		if (InVendicarMacAree)
		{
			Log("Bot is in Vendicar (Mac'Aree). Will use portal to get to Dalaran.");
			GoToTask.ToPositionAndIntecractWithGameObject(Positions.DalaranPortalMacAree, DalaranPortalID);
			return false;
		}
		Log("Bot is in Argus but isn't in Vendicar. Will attempt to return to Vendicar to move there.");
		ToVendicar();
		return false;
	}
	public static bool InVendicarKrokuun
	{
		get
		{
			return InKrokuun && ObjectManager.Me.Position.DistanceTo(Positions.KrokuunVendicar) < 100;
		}
	}
	public static bool ToVendicarKrokuun()
	{
		if (InVendicarKrokuun)
		{
			Log("Bot is in Vendicar (Krokuun).");
			return true;
		}
		if (InKrokuunOpen)
		{
			Log("Bot is in Krokuun open world. Teleporting to Vendicar (Krokuun).");
			Teleporter.VendicarKrokuun.To();
			return false;
		}
		Log("Bot is not in Krokuun. Traveling to Vendicar (Krokuun).");
		ToKrokuun();
		return false;
	}
	public static bool InVendicarAntoranWastes
	{
		get
		{
			return InAntoranWastes && ObjectManager.Me.Position.DistanceTo(Positions.AntoranWastesVendicar) < 100;
		}
	}
	public static bool ToVendicarAntoranWastes()
	{
		if (InVendicarAntoranWastes)
		{
			Log("Bot is in Vendicar (Antoran Wastes).");
			return true;
		}
		if (InAntoranWastesOpen)
		{
			Log("Bot is in Antoran Wastes open world. Teleporting to Vendicar (Vendicar Antoran Wastes).");
			Teleporter.VendicarAntoranWastes.To();
			return false;
		}
		Log("Bot is not in Antoran Wastes. Traveling to Vendicar (Antoran Wastes).");
		ToAntoranWastes();
		return false;
	}
	public static bool InVendicarMacAree
	{
		get
		{
			return InMacAree && ObjectManager.Me.Position.DistanceTo(Positions.MacAreeVendicar) < 100;
		}
	}
	public static bool ToVendicarMacAree()
	{
		if (InVendicarMacAree)
		{
			Log("Bot is in Vendicar (Mac'Aree).");
			return true;
		}
		if (InMacAreeOpen)
		{
			Log("Bot is in Mac'Aree open world. Teleporting to Vendicar (Mac'Aree).");
			Teleporter.VendicarMacAree.To();
			return false;
		}
		Log("Bot is not in Mac'Aree. Traveling to Vendicar (Mac'Aree).");
		ToMacAree();
		return false;
	}
	public static bool InVendicar
	{
		get
		{
			return InVendicarKrokuun || InVendicarAntoranWastes || InVendicarMacAree;
		}
	}
	public static bool ToVendicar()
	{
		if (InVendicar)
		{
			Log("Bot is in Vendicar.");
			return true;
		}
		if (InKrokuunOpen)
		{
			Log("Bot is in Krokuun open world. Teleporting to Vendicar (Krokuun).");
			Teleporter.VendicarKrokuun.To();
			return false;
		}
		if (InAntoranWastesOpen)
		{
			Log("Bot is in Antoran Wastes open world. Teleporting to Vendicar (Vendicar Antoran Wastes).");
			Teleporter.VendicarAntoranWastes.To();
			return false;
		}
		if (InMacAreeOpen)
		{
			Log("Bot is in Mac'Aree open world. Teleporting to Vendicar (Mac'Aree).");
			Teleporter.VendicarMacAree.To();
			return false;
		}
		Log("Bot isn't in Argus. Traveling there.");
		ToArgus();
		return false;
	}
	public static bool InKrokuun
	{
		get
		{
			return InArgus && Usefuls.AreaId == AREA_KROKUUN;
		}
	}
	public static bool ToKrokuun()
	{
		if (InKrokuun)
		{
			Log("Bot is in Krokuun.");
			return true;
		}
		if (InVendicar)
		{
			Log("Bot is in the Vendicar. Teleporting to Krokuun.");
			Teleporter.VendicarKrokuun.To();
			return false;
		}
		Log("Bot isn't in Vendicar. Navigating there.");
		ToVendicar();
		return false;
	}
	public static bool InKrokuunOpen
	{
		get
		{
			return InKrokuun && !InVendicarKrokuun;
		}
	}
	public static bool ToKrokuunOpen()
	{
		if (InKrokuunOpen)
		{
			Log("Bot is in Krokuun open world.");
			return true;
		}
		if (InVendicar)
		{
			Log("Bot is in Vendicar. Going to use teleporter to get to Vindicar (Krokul Hovel).");
			Teleporter.KrokulHovel.To();
			return false;
		}
		Log("Bot isn't in Vendicar. Navigating there.");
		ToVendicar();
		return false;
	}
	public static bool InAntoranWastes
	{
		get
		{
			return InArgus && Usefuls.AreaId == AREA_ANTORANWASTES;
		}
	}
	public static bool ToAntoranWastes()
	{
		if (InAntoranWastes)
		{
			Log("Bot is in Antoran Wastes.");
			return true;
		}
		if (InVendicar)
		{
			Log("Bot is in Vendicar. Going to use teleporter to get to Vindicar (Antoran Wastes).");
			Teleporter.VendicarAntoranWastes.To();
			return false;
		}
		Log("Bot isn't in Vendicar. Navigating there.");
		ToVendicar();
		return false;
	}
	public static bool InAntoranWastesOpen
	{
		get
		{
			return InAntoranWastes && !InVendicarAntoranWastes;
		}
	}
	public static bool ToAntoranWastesOpen()
	{
		if (InAntoranWastesOpen)
		{
			Log("Bot is in Antoran Wastes.");
			return true;
		}
		if (InVendicar)
		{
			Log("Bot is in Vendicar, teleporting to Hope's Landing (Antoran Wastes Open)");
			Teleporter.HopesLanding.To();
			return false;
		}
		Log("Bot isn't in Vendicar. Navigating there.");
		ToVendicar();
		return false;
	}
	public static bool OpenedVeiledDen
	{
		get
		{
			return Quest.GetQuestCompleted(AntoranWastesVeiledDen);
		}
	}
	public static bool OpenedLightsPurchase
	{
		get
		{
			return Quest.GetQuestCompleted(AntoranWastesLightsPurchase);
		}
	}
	public static bool InMacAree
	{
		get
		{
			return InArgus && Usefuls.AreaId == AREA_MACAREE;
		}
	}
	public static bool ToMacAree()
	{
		if (InMacAree)
		{
			Log("Bot is in Mac'Aree.");
			return true;
		}
		if (InVendicar)
		{
			Log("Bot is in Vendicar. Going to use teleporter to get to Vindicar (Mac'Aree).");
			Teleporter.VendicarMacAree.To();
			return false;
		}
		Log("Bot isn't in Vendicar. Navigating there.");
		ToVendicar();
		return false;
	}
	public static bool InMacAreeOpen
	{
		get
		{
			return InMacAree && !InVendicarMacAree;
		}
	}
	public static bool ToMacAreeOpen()
	{
		if (InMacAreeOpen)
		{
			Log("Bot is in Mac'Aree open world.");
			return true;
		}
		if (InVendicar)
		{
			Log("Bot is on the Vendicar. Teleporting to Triumvirate's End (Mac'Aree Open World).");
			Teleporter.TriumviratesEnd.To();
			return false;
		}
		Log("Bot isn't in Vendicar. Navigating there.");
		ToVendicar();
		return false;
	}
	//S from Krokul Hovel
	public static bool InKrokuunDarkfallRidge
	{
		get
		{
			var myPos = ObjectManager.Me.Position;
			return InKrokuunOpen && myPos.DistanceTo2D(Positions.DarkfallRidgeCenter) < 220 && myPos.DistanceZ(Positions.DarkfallRidgeCenter) < 50;
		}
	}
	public static bool InKrokuunNathraxasHold
	{
		get
		{
			var myPos = ObjectManager.Me.Position;
			return InKrokuunOpen && myPos.X > Positions.NathraxasHoldInside.X;
		}
	}
	public static bool InKrokuunShatteredFieldsCliff
	{
		get
		{
			var myPos = ObjectManager.Me.Position;
			return InKrokuunOpen && myPos.DistanceTo2D(Positions.ShatteredFieldsCliffInside) < 70;
		}
	}

	#region POSITIONS
	public static class Positions
	{
		public static Vector3 KrokuunVendicar = new Vector3(458.5647, 1449.929, 757.573, "None");
		public static Vector3 AntoranWastesVendicar = new Vector3(-2623.374, 8653.992, -79.04244, "None");
		public static Vector3 MacAreeVendicar = new Vector3(4682.881, 9851.88, 56.06652, "None");
		public static Vector3 DalaranPortalKrokuun = new Vector3(498.9296, 1467.982, 742.4532, "None");
		public static Vector3 DalaranPortalAntoranWastes = new Vector3(-2634.677, 8697.06, -94.16268, "None");
		public static Vector3 DalaranPortalMacAree = new Vector3(4725.081, 9861.455, 40.94718, "None");
		//krokuun
		public static Vector3 DarkfallRidgeCenter = new Vector3(734.3407, 1630.395, 588.4875, "None");
		public static Vector3 DarkfallRidgeOutSide = new Vector3(970.46, 1623.67, 533.5179, "None");
		public static Vector3 NathraxasHoldInside = new Vector3(1535.4, 1491.447, 474.5933, "None");
		public static Vector3 NathraxasHoldOutside = new Vector3(1511.262, 1484.037, 483.1274, "None");
		public static Vector3 ShatteredFieldsCliffInside = new Vector3(770.8271, 2533.532, 369.8428, "None");
		public static Vector3 ShatteredFieldsCliffOutside = new Vector3(850.5693, 2518.001, 366.3449, "None");
		//antoran wastes
		public static Vector3 HopesLandingCrossroad = new Vector3(-2858.894, 8886.03, -209.7137, "None");
		public static bool InHopesLandingCrossroad(Vector3 p)
		{
			return p.DistanceTo(HopesLandingCrossroad) < 55f;
		}
	}
	#endregion

	#region TELEPORTERS
	public class Teleporter
	{
		public Vector3 position;
		public int entry;
		public ContinentId continent;
		public int taxiNode = -1;
		public string x;
		public string y;
		public int questID = -1;
		static string luaFindNode = @"
local needX = {0}
local needY = {1}
local needT = 'REACHABLE'
for i = 1,NumTaxiNodes() do
	local n = TaxiNodeName(i)
	local x, y = TaxiNodePosition(i)
	local t = TaxiNodeGetType(i)
	if (x and y and needX and needY and t) then
		if (abs(x-needX) < 0.001 and abs(y-needY) and t == needT) then
			return i;
		end
	end
end
return -1;
";

		public static Teleporter Vendicar
		{
			get
			{
				if (InVendicarKrokuun)
					return VendicarKrokuun;

				if (InVendicarAntoranWastes)
					return VendicarAntoranWastes;

				if (InVendicarMacAree)
					return VendicarMacAree;

				return VendicarKrokuun;
			}
		}
		public static Teleporter DalaranToArgus = new Teleporter()
		{
			entry = 121014,
			position = new Vector3(-850.5267, 4265.826, 746.2749, "None"),
			continent = ContinentId.Troll_Raid,
		};
		public static Teleporter VendicarKrokuun = new Teleporter()
		{
			entry = 123139,
			position = new Vector3(505.1614, 1471.857, 765.7972, "None"),
			continent = ContinentId.Argus_1,
			x = "0.44006",
			y = "0.52043",
		};
		public static Teleporter VendicarAntoranWastes = new Teleporter()
		{
			entry = 125514,
			position = new Vector3(-2636.905, 8703.858, -70.82, "None"),
			continent = ContinentId.Argus_1,
			x = "0.14538",
			y = "0.39255",
			questID = 48559, //http://www.wowhead.com/quest=48199/the-burning-heart
		};
		public static Teleporter VendicarMacAree = new Teleporter()
		{
			entry = 125461,
			position = new Vector3(4732.196, 9863.496, 64.28996, "None"),
			continent = ContinentId.Argus_1,
			x = "0.09799",
			y = "0.69274",
			questID = 47994, //http://www.wowhead.com/quest=47994/forming-a-bond
		};
		//krokuun
		public static Teleporter KrokulHovel = new Teleporter()
		{
			entry = 118830,
			position = new Vector3(986.6412, 1709.364, 516.9945, "None"),
			continent = ContinentId.Argus_1,
			x = "0.43022",
			y = "0.54018",
			questID = 46816, //http://www.wowhead.com/quest=46816/rendezvous
		};
		public static Teleporter ShatteredFields = new Teleporter()
		{
			entry = 123260,
			position = new Vector3(1082.926, 2270.06, 408.4375, "None"),
			continent = ContinentId.Argus_1,
			x = "0.40723",
			y = "0.54406",
		};
		public static Teleporter DestinyPoint = new Teleporter()
		{
			entry = 124569,
			position = new Vector3(1437.48, 1444.27, 491.3067, "None"),
			continent = ContinentId.Argus_1,
			x = "0.44113",
			y = "0.55859",
		};
		//antoran wastes
		public static Teleporter HopesLanding = new Teleporter()
		{
			entry = 125407,
			position = new Vector3(-2931.165, 8798.77, -231.9501, "None"),
			continent = ContinentId.Argus_1,
			x = "0.14136",
			y = "0.38037",
		};
		public static Teleporter TheVeiledDen = new Teleporter()
		{
			entry = 125409,
			position = new Vector3(-2365.713, 8878.157, -140.2207, "None"),
			continent = ContinentId.Argus_1,
			x = "0.13808",
			y = "0.40351",
		};
		//macaree
		public static Teleporter ShadowguardIncursion = new Teleporter()
		{
			entry = 123258,
			position = new Vector3(5546.06, 10563, 7.527513, "None"),
			continent = ContinentId.Argus_1,
			x = "0.06944",
			y = "0.72606",
		};
		public static Teleporter CityCenter = new Teleporter()
		{
			entry = 126951,
			position = new Vector3(5417.01, 10013.3, -81.4, "None"),
			continent = ContinentId.Argus_1,
			x = "0.09185",
			y = "0.72080",
		};
		public static Teleporter ProphetsReflection = new Teleporter()
		{
			entry = 125350,
			position = new Vector3(6307.33, 10116.5, -16.06369, "None"),
			continent = ContinentId.Argus_1,
			x = "0.08764",
			y = "0.75709",
			questID = 47856, // http://www.wowhead.com/quest=47856/across-the-universe
		};
		public static Teleporter ConservatoryOfTheArcane = new Teleporter()
		{
			entry = 124313,
			position = new Vector3(5764.89, 9493.78, -66.74951, "None"),
			continent = ContinentId.Argus_1,
			x = "0.11302",
			y = "0.73498",
			questID = 47690, //http://www.wowhead.com/quest=47690/the-defilers-legacy
		};
		public static Teleporter TriumviratesEnd = new Teleporter()
		{
			entry = 122509,
			position = new Vector3(4983.3, 9823.47, -78.77355, "None"),
			continent = ContinentId.Argus_1,
			x = "0.09958",
			y = "0.70312",
			questID = 46941, //http://www.wowhead.com/quest=46941/the-path-forward
		};
		static List<Teleporter> Krokuun = new List<Teleporter>()
		{
			KrokulHovel,
			/*
			ShatteredFields,
			DestinyPoint,
			//*/
		};

		static List<Teleporter> AntoranWastes = new List<Teleporter>()
		{
			HopesLanding,
			/*
			TheVeiledDen,
			//*/
		};

		static List<Teleporter> MacAree = new List<Teleporter>()
		{
			TriumviratesEnd,
			ConservatoryOfTheArcane,
			ProphetsReflection,
			/*
			ShadowguardIncursion,
			CityCenter,
			//*/
		};
		public static Teleporter GetNear()
		{
			if (InVendicar)
				return Vendicar;

			if (InKrokuun)
				return SearchNear(Krokuun);// Krokuun.OrderBy(t => ObjectManager.Me.Position.DistanceTo(t.position)).FirstOrDefault();

			if (InAntoranWastes)
				return SearchNear(AntoranWastes);// return AntoranWastes.OrderBy(t => ObjectManager.Me.Position.DistanceTo(t.position)).FirstOrDefault();

			if (InMacAree)
				return SearchNear(MacAree);// return MacAree.OrderBy(t => ObjectManager.Me.Position.DistanceTo(t.position)).FirstOrDefault();

			return null;
		}
		static Teleporter SearchNear(List<Teleporter> teleporters)
		{
			return teleporters.Where(t => t.questID < 0 || Quest.GetQuestCompleted(t.questID)).OrderBy(t => ObjectManager.Me.Position.DistanceTo(t.position)).FirstOrDefault();
		}
		public bool To()
		{
			if (!InArgus)
			{
				ToArgus();
				return false;
			}
			if (Usefuls.ContinentId != (int)continent)
				return false;

			var near = GetNear();
			if (near != null)
			{
				Log("found near node=" + near);
				near.Use();
				Take();
				return true;
			}
			Log("cannot find near nodes to reach node=" + this);
			return false;
		}
		public bool Take()
		{
			Thread.Sleep(Usefuls.Latency * 2);
			if (Questing.IsVisible("FlightMapFrame"))
			{
				if (taxiNode < 1 || true) //always detect nodes, coz changed after every quest
				{
					var runCode = string.Format(luaFindNode, x, y);
					taxiNode = Lua.LuaDoString<int>(runCode);
					Thread.Sleep(Usefuls.Latency * 2);
					Log("calculated taxi node=" + this);
					if (taxiNode < 1)
						return false;
				}
				Log("take taxi node=" + this);
				Lua.LuaDoString("TakeTaxiNode(" + taxiNode + ")");
				//Thread.Sleep(Usefuls.Latency * 2);
				Questing.WaitCurrentAreaIDChange();
				return true;
			}
			return false;
		}
		public bool Use()
		{
			if (Usefuls.ContinentId != (int)continent)
				return false;

			GoToTask.ToPositionAndIntecractWithNpc(position, entry);
			return true;
		}
		public override string ToString()
		{
			return "[Argus Teleporter id=" + entry + " node=" + taxiNode + " xy=" + x + "," + y + " quest=" + questID + /* " pos=" + position.ToStringNewVector() + */"]";
		}
	}
	#endregion TELEPORTERS

	#region SUBZONE
	public static void StartMoveFix()
	{
		robotManager.Events.ProductEvents.OnProductStopping += OnProductStop;
		wManager.Events.MovementEvents.OnMovementPulse += Subzone.Vendicar.OnMovementPulse;
		//krokuun
		wManager.Events.MovementEvents.OnMovementPulse += Subzone.KrokulHovel.OnMovementPulse;
		wManager.Events.MovementEvents.OnMovementPulse += Subzone.NathraxasHold.OnMovementPulse;
		wManager.Events.MovementEvents.OnMovementPulse += Subzone.DarkfallRidge.OnMovementPulse;
		wManager.Events.MovementEvents.OnMovementPulse += Subzone.ShatteredFieldsCliff.OnMovementPulse;
		//antoran wastes
		//mac'aree
		wManager.Events.MovementEvents.OnMovementPulse += Subzone.TriumviratesEnd.OnMovementPulse;

		Log("Argus move fix started.");
	}
	public static void StopMoveFix()
	{
		robotManager.Events.ProductEvents.OnProductStopping -= OnProductStop;
		wManager.Events.MovementEvents.OnMovementPulse -= Subzone.Vendicar.OnMovementPulse;
		//krokuun
		wManager.Events.MovementEvents.OnMovementPulse -= Subzone.KrokulHovel.OnMovementPulse;
		wManager.Events.MovementEvents.OnMovementPulse -= Subzone.NathraxasHold.OnMovementPulse;
		wManager.Events.MovementEvents.OnMovementPulse -= Subzone.DarkfallRidge.OnMovementPulse;
		wManager.Events.MovementEvents.OnMovementPulse -= Subzone.ShatteredFieldsCliff.OnMovementPulse;
		//antoran wastes
		//mac'aree
		wManager.Events.MovementEvents.OnMovementPulse -= Subzone.TriumviratesEnd.OnMovementPulse;

		Log("Argus move fix ended.");
	}
	static void OnProductStop(string productName)
	{
		StopMoveFix();
	}
	public class Subzone
	{
		//all vendicars
		internal class Vendicar
		{
			const int TOTAL_TRIES = 3;
			static int currentTries = TOTAL_TRIES;
			static Vector3 lastTryPosition = Vector3.Zero;
			internal static void OnMovementPulse(List<Vector3> points, System.ComponentModel.CancelEventArgs cancelable)
			{
				if (!InVendicar)
					return;

				if (points == null)
					return;
				var myPos = ObjectManager.Me.Position;
				var pointsCount = points.Count;
				if (pointsCount < 1)
					return;

				var end = points[points.Count - 1];
				//if (!TraceLine.TraceLineGo(end)) return;
				if (lastTryPosition.DistanceTo(end) > 3.5f)
				{
					currentTries = TOTAL_TRIES;
					lastTryPosition = end;
				}
				else if (currentTries-- < 1)
				{
					return;
				}

				if (InVendicarKrokuun)
				{
					if (!TraceLine.TraceLineGo(end))
					{
						Log("Moving directly from Vendicar (Krokuun) to " + end + ".");
						points.Clear();
						points.Add(end);
						return;
					}
					var prev = points.Count;
					var path = Questing.PathClampDirected(VendicarKrokuun, myPos, end);
					points.Clear();
					points.AddRange(path);
					Log("Vendicar (Krokuun) path fixed from " + prev + " to " + points.Count + ".");
					return;
				}
				if (InVendicarAntoranWastes)
				{
					if (!TraceLine.TraceLineGo(end))
					{
						Log("Moving directly from Vendicar (Antoran Wastes) to " + end + ".");
						points.Clear();
						points.Add(end);
						return;
					}
					var prev = points.Count;
					var path = Questing.PathClampDirected(VendicarAntoranWastes, myPos, end);
					points.Clear();
					points.AddRange(path);
					Log("Vendicar (Antoran Wastes) path fixed from " + prev + " to " + points.Count + ".");
					return;
				}
				if (InVendicarMacAree)
				{
					if (!TraceLine.TraceLineGo(end))
					{
						Log("Moving directly from Vendicar (Mac'Aree) to " + end + ".");
						points.Clear();
						points.Add(end);
						return;
					}
					var prev = points.Count;
					var path = Questing.PathClampDirected(VendicarMacAree, myPos, end);
					//Log(">>>> LAST PATH " + path[path.Count-1].ToStringNewVector() );
					points.Clear();
					points.AddRange(path);
					Log("Vendicar (Mac'Aree) path fixed from " + prev + " to " + points.Count + ".");
					return;
				}
			}
			public static List<Vector3> VendicarAntoranWastes = new List<Vector3>()
			{
				new Vector3(-2601.875f, 8570.061f, -67.06408f, "None"),
				new Vector3(-2603.131f, 8574.359f, -67.04562f, "None"),
				new Vector3(-2603.994f, 8577.781f, -67.02934f, "None"),
				new Vector3(-2604.839f, 8581.17f, -66.78188f, "None"),
				new Vector3(-2605.67f, 8584.533f, -65.77453f, "None"),
				new Vector3(-2606.558f, 8587.94f, -64.52246f, "None"),
				new Vector3(-2607.037f, 8591.463f, -63.58455f, "None"),
				new Vector3(-2607.826f, 8598.284f, -63.72207f, "None"),
				new Vector3(-2608.234f, 8601.81f, -63.72207f, "None"),
				new Vector3(-2608.092f, 8608.701f, -63.72207f, "None"),
				new Vector3(-2606.757f, 8612.055f, -63.94997f, "None"),
				new Vector3(-2602.873f, 8617.838f, -63.9659f, "None"),
				new Vector3(-2598f, 8622.747f, -63.9447f, "None"),
				new Vector3(-2595.5f, 8625.344f, -64.32557f, "None"),
				new Vector3(-2593.148f, 8627.898f, -64.99951f, "None"),
				new Vector3(-2590.914f, 8630.601f, -65.74329f, "None"),
				new Vector3(-2588.74f, 8633.291f, -66.5158f, "None"),
				new Vector3(-2586.732f, 8636.128f, -67.26829f, "None"),
				new Vector3(-2585.234f, 8639.42f, -68.0367f, "None"),
				new Vector3(-2583.494f, 8646.024f, -69.51395f, "None"),
				new Vector3(-2582.883f, 8649.685f, -70.32391f, "None"),
				new Vector3(-2582.577f, 8653.186f, -71.07793f, "None"),
				new Vector3(-2582.647f, 8656.644f, -71.83707f, "None"),
				new Vector3(-2582.776f, 8660.134f, -72.47202f, "None"),
				new Vector3(-2583.153f, 8663.589f, -73.25223f, "None"),
				new Vector3(-2584.094f, 8667.004f, -73.92773f, "None"),
				new Vector3(-2585.226f, 8670.333f, -74.63814f, "None"),
				new Vector3(-2586.746f, 8673.465f, -75.40399f, "None"),
				new Vector3(-2590.664f, 8679.183f, -76.93823f, "None"),
				new Vector3(-2593.229f, 8681.562f, -77.818f, "None"),
				new Vector3(-2596.104f, 8683.558f, -78.57767f, "None"),
				new Vector3(-2599.438f, 8684.661f, -79.09508f, "None"),
				new Vector3(-2606.127f, 8683.644f, -79.45836f, "None"),
				new Vector3(-2609.441f, 8682.374f, -79.18909f, "None"),
				new Vector3(-2612.778f, 8681.185f, -79.25461f, "None"),
				new Vector3(-2616.138f, 8680.133f, -79.19041f, "None"),
				new Vector3(-2622.598f, 8681.001f, -78.95972f, "None"),
				new Vector3(-2627.118f, 8686.219f, -76.93365f, "None"),
				new Vector3(-2629.015f, 8689.358f, -74.85479f, "None"),
				new Vector3(-2630.497f, 8692.545f, -72.36125f, "None"),
				new Vector3(-2631.751f, 8695.675f, -70.53207f, "None"),
				new Vector3(-2633.044f, 8699.011f, -70.82211f, "None"),
				new Vector3(-2637.755f, 8703.205f, -70.81985f, "None"),
				new Vector3(-2639.195f, 8697.208f, -70.82909f, "None"),
				new Vector3(-2638.203f, 8693.811f, -70.54588f, "None"),
				new Vector3(-2637.196f, 8690.55f, -72.50269f, "None"),
				new Vector3(-2635.931f, 8687.1f, -75.39765f, "None"),
				new Vector3(-2634.652f, 8683.932f, -77.54271f, "None"),
				new Vector3(-2633.113f, 8680.726f, -78.95428f, "None"),
				new Vector3(-2628.022f, 8676.417f, -78.95428f, "None"),
				new Vector3(-2624.423f, 8676.015f, -78.95428f, "None"),
				new Vector3(-2617.502f, 8676.029f, -79.33512f, "None"),
				new Vector3(-2610.896f, 8675.639f, -79.33226f, "None"),
				new Vector3(-2607.363f, 8674.322f, -79.07319f, "None"),
				new Vector3(-2604.603f, 8672.146f, -80.55556f, "None"),
				new Vector3(-2602.013f, 8669.809f, -81.75923f, "None"),
				new Vector3(-2599.756f, 8667.102f, -82.96237f, "None"),
				new Vector3(-2598.037f, 8664.069f, -84.15471f, "None"),
				new Vector3(-2596.651f, 8660.827f, -85.34707f, "None"),
				new Vector3(-2595.941f, 8657.475f, -86.48708f, "None"),
				new Vector3(-2596.084f, 8653.976f, -87.65275f, "None"),
				new Vector3(-2596.718f, 8650.477f, -88.8518f, "None"),
				new Vector3(-2597.733f, 8647.163f, -90.02518f, "None"),
				new Vector3(-2599.005f, 8643.909f, -91.22013f, "None"),
				new Vector3(-2600.634f, 8640.844f, -92.4043f, "None"),
				new Vector3(-2602.383f, 8637.74f, -93.5546f, "None"),
				new Vector3(-2604.055f, 8634.729f, -94.55651f, "None"),
				new Vector3(-2605.287f, 8628.144f, -94.64252f, "None"),
				new Vector3(-2599.717f, 8625.025f, -94.64252f, "None"),
				new Vector3(-2593.246f, 8627.21f, -94.64252f, "None"),
				new Vector3(-2588.24f, 8632.234f, -94.64252f, "None"),
				new Vector3(-2585.12f, 8638.335f, -94.64252f, "None"),
				new Vector3(-2583.727f, 8641.552f, -94.64252f, "None"),
				new Vector3(-2581.647f, 8648.288f, -94.64252f, "None"),
				new Vector3(-2581.328f, 8655.325f, -94.64252f, "None"),
				new Vector3(-2581.969f, 8662.118f, -94.64252f, "None"),
				new Vector3(-2583.345f, 8665.367f, -94.64252f, "None"),
				new Vector3(-2584.725f, 8668.614f, -94.64252f, "None"),
				new Vector3(-2587.849f, 8674.77f, -94.64252f, "None"),
				new Vector3(-2592.669f, 8679.77f, -94.64252f, "None"),
				new Vector3(-2595.567f, 8681.745f, -94.64252f, "None"),
				new Vector3(-2598.767f, 8683.533f, -94.64252f, "None"),
				new Vector3(-2605.216f, 8685.858f, -94.64252f, "None"),
				new Vector3(-2611.923f, 8688.024f, -94.60181f, "None"),
				new Vector3(-2618.652f, 8689.843f, -94.64201f, "None"),
				new Vector3(-2622.133f, 8690.367f, -94.64201f, "None"),
				new Vector3(-2629.023f, 8690.813f, -94.64201f, "None"),
				new Vector3(-2632.255f, 8685.66f, -94.34518f, "None"),
				new Vector3(-2631.532f, 8682.224f, -94.57051f, "None"),
				new Vector3(-2630.736f, 8678.808f, -94.57051f, "None"),
				new Vector3(-2629.935f, 8675.372f, -94.57051f, "None"),
				new Vector3(-2627.922f, 8668.782f, -94.57051f, "None"),
				new Vector3(-2625.667f, 8666.084f, -94.81638f, "None"),
				new Vector3(-2623.365f, 8663.522f, -95.60426f, "None"),
				new Vector3(-2621.159f, 8660.685f, -95.54629f, "None"),
				new Vector3(-2618.504f, 8654.283f, -95.4168f, "None"),
				new Vector3(-2618.562f, 8650.7f, -95.40883f, "None"),
				new Vector3(-2619.442f, 8643.876f, -95.17373f, "None"),
				new Vector3(-2619.441f, 8640.329f, -94.57049f, "None"),
				new Vector3(-2617.898f, 8633.59f, -94.57049f, "None"),
				new Vector3(-2616.882f, 8630.19f, -94.57049f, "None"),
				new Vector3(-2616.267f, 8626.667f, -94.57049f, "None"),
				new Vector3(-2618.595f, 8623.212f, -94.64153f, "None"),
				new Vector3(-2624.84f, 8624.264f, -94.64153f, "None"),
				new Vector3(-2628.149f, 8625.831f, -94.64153f, "None"),
				new Vector3(-2634.317f, 8628.753f, -93.53957f, "None"),
				new Vector3(-2637.505f, 8630.28f, -92.43549f, "None"),
				new Vector3(-2641.534f, 8632.063f, -91.02247f, "None"),
				new Vector3(-2645.697f, 8634.362f, -89.55556f, "None"),
				new Vector3(-2649.011f, 8637.873f, -88.0525f, "None"),
				new Vector3(-2650.576f, 8642.582f, -86.50688f, "None"),
				new Vector3(-2651.444f, 8647.335f, -84.95063f, "None"),
				new Vector3(-2652.006f, 8652.258f, -83.29588f, "None"),
				new Vector3(-2651.739f, 8657.239f, -81.60348f, "None"),
				new Vector3(-2651.125f, 8661.92f, -79.97961f, "None"),
				new Vector3(-2650.49f, 8666.831f, -79.12866f, "None"),
				new Vector3(-2651.935f, 8671.472f, -79.3241f, "None"),
				new Vector3(-2655.812f, 8671.948f, -79.50228f, "None"),
				new Vector3(-2658.252f, 8669.352f, -79.31216f, "None"),
				new Vector3(-2661.493f, 8663.099f, -78.06527f, "None"),
				new Vector3(-2662.703f, 8659.979f, -76.97829f, "None"),
				new Vector3(-2663.6f, 8656.596f, -76.4397f, "None"),
				new Vector3(-2663.851f, 8652.986f, -75.45214f, "None"),
				new Vector3(-2663.634f, 8649.556f, -74.43121f, "None"),
				new Vector3(-2663.382f, 8646.027f, -73.96042f, "None"),
				new Vector3(-2662.607f, 8642.557f, -73.18192f, "None"),
				new Vector3(-2659.946f, 8636.225f, -71.85538f, "None"),
				new Vector3(-2658.412f, 8633.051f, -71.07401f, "None"),
				new Vector3(-2656.463f, 8630.146f, -70.3092f, "None"),
				new Vector3(-2654.242f, 8627.398f, -69.53065f, "None"),
				new Vector3(-2651.74f, 8624.983f, -68.77285f, "None"),
				new Vector3(-2648.708f, 8623.252f, -68.00529f, "None"),
				new Vector3(-2645.654f, 8621.644f, -67.25648f, "None"),
				new Vector3(-2642.453f, 8619.986f, -66.42426f, "None"),
				new Vector3(-2639.364f, 8618.371f, -65.60063f, "None"),
				new Vector3(-2636.213f, 8616.708f, -64.85413f, "None"),
				new Vector3(-2633.161f, 8615.098f, -64.23537f, "None"),
				new Vector3(-2627.453f, 8611.091f, -63.90548f, "None"),
				new Vector3(-2626.744f, 8604.469f, -63.93601f, "None"),
			};
			public static List<Vector3> VendicarKrokuun = new List<Vector3>()
			{
				new Vector3(380.6077f, 1411.1f, 769.5513f, "None"),
				new Vector3(384.6037f, 1413.542f, 769.5699f, "None"),
				new Vector3(390.5711f, 1416.953f, 769.8036f, "None"),
				new Vector3(393.7018f, 1418.727f, 770.7841f, "None"),
				new Vector3(396.6957f, 1420.311f, 771.988f, "None"),
				new Vector3(399.9756f, 1421.769f, 773.0316f, "None"),
				new Vector3(403.4054f, 1422.701f, 773.1301f, "None"),
				new Vector3(410.3314f, 1422.837f, 772.8928f, "None"),
				new Vector3(416.7403f, 1420.09f, 772.8928f, "None"),
				new Vector3(422.2933f, 1416.679f, 772.6581f, "None"),
				new Vector3(425.8799f, 1417.107f, 772.7009f, "None"),
				new Vector3(429.4912f, 1417.239f, 772.6975f, "None"),
				new Vector3(436.3504f, 1416.25f, 772.1172f, "None"),
				new Vector3(439.7328f, 1415.159f, 771.4123f, "None"),
				new Vector3(442.9966f, 1414.017f, 770.6302f, "None"),
				new Vector3(446.2604f, 1412.874f, 769.8582f, "None"),
				new Vector3(449.6487f, 1411.992f, 769.0402f, "None"),
				new Vector3(453.1695f, 1411.405f, 768.2419f, "None"),
				new Vector3(456.6183f, 1410.898f, 767.4414f, "None"),
				new Vector3(460.1303f, 1410.736f, 766.633f, "None"),
				new Vector3(467.0241f, 1411.795f, 765.0319f, "None"),
				new Vector3(470.359f, 1412.735f, 764.442f, "None"),
				new Vector3(476.5515f, 1415.776f, 762.7235f, "None"),
				new Vector3(479.5449f, 1417.615f, 761.9563f, "None"),
				new Vector3(485.1977f, 1421.686f, 760.2083f, "None"),
				new Vector3(487.5748f, 1424.416f, 759.6351f, "None"),
				new Vector3(489.5512f, 1427.227f, 758.5774f, "None"),
				new Vector3(491.4135f, 1430.207f, 757.8238f, "None"),
				new Vector3(492.6515f, 1433.517f, 757.3085f, "None"),
				new Vector3(490.3814f, 1439.908f, 757.2772f, "None"),
				new Vector3(488.0989f, 1442.613f, 757.4602f, "None"),
				new Vector3(483.9664f, 1448.248f, 757.3466f, "None"),
				new Vector3(484.1214f, 1454.78f, 757.6591f, "None"),
				new Vector3(489.3701f, 1459.255f, 759.5164f, "None"),
				new Vector3(492.0392f, 1461.444f, 761.4918f, "None"),
				new Vector3(494.7982f, 1463.72f, 763.953f, "None"),
				new Vector3(497.4787f, 1466.003f, 766.0671f, "None"),
				new Vector3(502.8314f, 1470.405f, 765.7975f, "None"),
				new Vector3(497.8248f, 1468.906f, 765.9579f, "None"),
				new Vector3(494.4213f, 1467.698f, 764.8423f, "None"),
				new Vector3(491.1201f, 1466.768f, 762.3483f, "None"),
				new Vector3(487.5714f, 1466.323f, 760.0363f, "None"),
				new Vector3(484.1101f, 1466.41f, 758.1446f, "None"),
				new Vector3(480.6193f, 1466.786f, 757.6613f, "None"),
				new Vector3(477.8523f, 1464.121f, 757.6599f, "None"),
				new Vector3(478.5919f, 1460.409f, 757.6599f, "None"),
				new Vector3(480.8808f, 1453.757f, 757.6599f, "None"),
				new Vector3(481.8991f, 1450.395f, 757.6599f, "None"),
				new Vector3(482.6734f, 1446.954f, 757.2805f, "None"),
				new Vector3(482.168f, 1440.074f, 757.4487f, "None"),
				new Vector3(481.1583f, 1436.679f, 757.1586f, "None"),
				new Vector3(479.8855f, 1433.483f, 755.8617f, "None"),
				new Vector3(477.7867f, 1430.665f, 754.6228f, "None"),
				new Vector3(475.4201f, 1428.108f, 753.426f, "None"),
				new Vector3(472.5546f, 1426.032f, 752.2216f, "None"),
				new Vector3(469.373f, 1424.692f, 751.0359f, "None"),
				new Vector3(466.036f, 1423.668f, 749.8351f, "None"),
				new Vector3(462.5989f, 1422.941f, 748.6362f, "None"),
				new Vector3(459.1531f, 1422.52f, 747.4651f, "None"),
				new Vector3(455.6945f, 1422.329f, 746.3049f, "None"),
				new Vector3(452.1349f, 1422.172f, 745.1487f, "None"),
				new Vector3(448.6509f, 1422.058f, 744.1195f, "None"),
				new Vector3(445.1964f, 1421.68f, 743.4313f, "None"),
				new Vector3(442.9923f, 1415.871f, 741.9744f, "None"),
				new Vector3(444.886f, 1409.227f, 741.9744f, "None"),
				new Vector3(444.3686f, 1402.302f, 741.9744f, "None"),
				new Vector3(443.7277f, 1398.843f, 741.9744f, "None"),
				new Vector3(443.7239f, 1395.328f, 741.9744f, "None"),
				new Vector3(448.013f, 1398.446f, 741.9744f, "None"),
				new Vector3(450.5824f, 1400.874f, 741.9744f, "None"),
				new Vector3(456.566f, 1404.371f, 741.9744f, "None"),
				new Vector3(462.8984f, 1407.237f, 741.9744f, "None"),
				new Vector3(469.2564f, 1410.115f, 741.9744f, "None"),
				new Vector3(472.47f, 1411.571f, 741.9744f, "None"),
				new Vector3(478.5426f, 1415.155f, 741.9744f, "None"),
				new Vector3(483.6702f, 1419.751f, 741.9744f, "None"),
				new Vector3(485.8626f, 1422.496f, 741.9744f, "None"),
				new Vector3(489.7062f, 1428.344f, 741.9744f, "None"),
				new Vector3(490.9909f, 1431.613f, 741.9744f, "None"),
				new Vector3(492.7155f, 1438.42f, 741.9744f, "None"),
				new Vector3(494.1823f, 1445.264f, 742.0061f, "None"),
				new Vector3(494.9216f, 1448.714f, 742.0209f, "None"),
				new Vector3(496.3786f, 1455.553f, 742.0115f, "None"),
				new Vector3(497.1696f, 1462.477f, 742.2704f, "None"),
				new Vector3(496.8562f, 1469.444f, 742.3998f, "None"),
				new Vector3(491.9464f, 1473.117f, 742.2695f, "None"),
				new Vector3(486.7861f, 1469.183f, 741.9743f, "None"),
				new Vector3(485.4192f, 1465.91f, 741.9743f, "None"),
				new Vector3(481.2001f, 1460.434f, 742.0455f, "None"),
				new Vector3(474.8958f, 1457.497f, 742.0455f, "None"),
				new Vector3(471.5056f, 1456.451f, 742.0455f, "None"),
				new Vector3(468.0819f, 1455.983f, 741.369f, "None"),
				new Vector3(464.6079f, 1455.808f, 740.9735f, "None"),
				new Vector3(461.1214f, 1455.336f, 741.2162f, "None"),
				new Vector3(454.7112f, 1453.298f, 741.1985f, "None"),
				new Vector3(449.5303f, 1448.65f, 741.0731f, "None"),
				new Vector3(446.9606f, 1446.192f, 741.9631f, "None"),
				new Vector3(441.4048f, 1442.106f, 742.0453f, "None"),
				new Vector3(438.2257f, 1440.482f, 742.0453f, "None"),
				new Vector3(431.7025f, 1439.583f, 742.0453f, "None"),
				new Vector3(428.5927f, 1441.705f, 741.9742f, "None"),
				new Vector3(428.5104f, 1448.21f, 741.9742f, "None"),
				new Vector3(430.9644f, 1454.761f, 742.806f, "None"),
				new Vector3(432.2701f, 1457.958f, 743.7925f, "None"),
				new Vector3(433.6135f, 1461.205f, 744.9489f, "None"),
				new Vector3(435.2536f, 1464.237f, 746.1141f, "None"),
				new Vector3(437.2883f, 1467.058f, 747.274f, "None"),
				new Vector3(440.0688f, 1469.392f, 748.4808f, "None"),
				new Vector3(442.753f, 1471.386f, 749.6243f, "None"),
				new Vector3(445.9684f, 1473.076f, 750.8835f, "None"),
				new Vector3(449.1497f, 1474.271f, 752.0891f, "None"),
				new Vector3(452.4887f, 1475.389f, 753.3344f, "None"),
				new Vector3(455.8538f, 1476.443f, 754.6071f, "None"),
				new Vector3(459.1489f, 1477.259f, 755.8445f, "None"),
				new Vector3(462.5331f, 1478.232f, 757.1066f, "None"),
				new Vector3(465.8984f, 1479.442f, 757.5099f, "None"),
				new Vector3(468.2351f, 1484.934f, 757.4162f, "None"),
				new Vector3(462.5571f, 1488.56f, 758.1231f, "None"),
				new Vector3(459.0462f, 1488.879f, 758.9802f, "None"),
				new Vector3(455.5907f, 1489.012f, 759.8131f, "None"),
				new Vector3(452.128f, 1489.093f, 760.6642f, "None"),
				new Vector3(448.5562f, 1488.31f, 761.6186f, "None"),
				new Vector3(445.2748f, 1487.154f, 762.2878f, "None"),
				new Vector3(441.9681f, 1485.925f, 763.1035f, "None"),
				new Vector3(438.7202f, 1484.572f, 763.9707f, "None"),
				new Vector3(435.7233f, 1482.825f, 764.4606f, "None"),
				new Vector3(432.9084f, 1480.685f, 765.2348f, "None"),
				new Vector3(430.5395f, 1478.156f, 766.0153f, "None"),
				new Vector3(428.2153f, 1475.477f, 766.8104f, "None"),
				new Vector3(426.2347f, 1472.69f, 767.567f, "None"),
				new Vector3(424.6549f, 1469.514f, 768.3783f, "None"),
				new Vector3(423.4268f, 1466.251f, 769.1356f, "None"),
				new Vector3(422.1682f, 1462.954f, 769.9647f, "None"),
				new Vector3(421.0466f, 1459.692f, 770.7456f, "None"),
				new Vector3(420.1392f, 1456.335f, 771.5321f, "None"),
				new Vector3(419.0479f, 1453.018f, 772.2363f, "None"),
				new Vector3(417.5495f, 1449.832f, 772.6526f, "None"),
				new Vector3(414.7644f, 1447.612f, 772.698f, "None"),
				//add fix
				//new Vector3(420.5348f, 1458.081f, 771.1585f, "None"),
				//new Vector3(419.4931f, 1454.251f, 771.9688f, "None"),
				//new Vector3(418.4002f, 1450.788f, 772.5312f, "None"),
				//new Vector3(416.7452f, 1447.708f, 772.6981f, "None"),
				new Vector3(410.3899f, 1445.858f, 772.7097f, "None"),
				new Vector3(406.553f, 1451.261f, 772.6549f, "None"),
				new Vector3(406.502f, 1458.116f, 772.6549f, "None"),
				new Vector3(406.5947f, 1461.678f, 772.6549f, "None"),
				new Vector3(406.6883f, 1465.275f, 773.1895f, "None"),
			};
			public static List<Vector3> VendicarMacAree = new List<Vector3>()
			{
				new Vector3(4601.1f, 9831.9f, 68.06099f, "None"),
				new Vector3(4607.443f, 9833.164f, 68.23334f, "None"),
				new Vector3(4611.074f, 9833.775f, 68.77486f, "None"),
				new Vector3(4614.429f, 9834.34f, 69.98125f, "None"),
				new Vector3(4617.957f, 9834.93f, 71.30105f, "None"),
				new Vector3(4624.851f, 9836.059f, 71.38628f, "None"),
				new Vector3(4631.797f, 9836.297f, 71.38632f, "None"),
				new Vector3(4635.108f, 9835.078f, 71.38632f, "None"),
				new Vector3(4641.114f, 9831.559f, 71.13745f, "None"),
				new Vector3(4646.696f, 9827.514f, 71.20641f, "None"),
				new Vector3(4649.511f, 9825.205f, 71.15475f, "None"),
				new Vector3(4654.932f, 9820.9f, 70.09737f, "None"),
				new Vector3(4657.827f, 9818.847f, 69.35673f, "None"),
				new Vector3(4660.812f, 9817.122f, 68.58424f, "None"),
				new Vector3(4663.944f, 9815.629f, 67.81998f, "None"),
				new Vector3(4667.179f, 9814.428f, 67.07005f, "None"),
				new Vector3(4670.584f, 9813.268f, 66.26212f, "None"),
				new Vector3(4673.908f, 9812.264f, 65.50397f, "None"),
				new Vector3(4677.341f, 9811.511f, 64.71761f, "None"),
				new Vector3(4680.873f, 9811.465f, 63.94407f, "None"),
				new Vector3(4684.33f, 9811.518f, 63.16489f, "None"),
				new Vector3(4687.873f, 9811.876f, 62.71216f, "None"),
				new Vector3(4694.474f, 9813.815f, 60.99109f, "None"),
				new Vector3(4697.774f, 9815.209f, 60.47472f, "None"),
				new Vector3(4700.92f, 9816.741f, 59.44198f, "None"),
				new Vector3(4703.908f, 9818.532f, 58.43387f, "None"),
				new Vector3(4706.665f, 9820.7f, 57.86514f, "None"),
				new Vector3(4708.974f, 9823.339f, 56.80674f, "None"),
				new Vector3(4712.162f, 9829.395f, 55.70933f, "None"),
				new Vector3(4710.141f, 9835.95f, 55.91319f, "None"),
				new Vector3(4708.584f, 9839.187f, 55.77505f, "None"),
				new Vector3(4707.343f, 9842.46f, 55.77505f, "None"),
				new Vector3(4709.083f, 9848.912f, 56.14101f, "None"),
				new Vector3(4715.203f, 9852.192f, 58.69751f, "None"),
				new Vector3(4718.427f, 9853.479f, 61.17265f, "None"),
				new Vector3(4721.666f, 9854.771f, 63.35135f, "None"),
				new Vector3(4724.949f, 9856.237f, 64.4607f, "None"),
				new Vector3(4729.957f, 9861.003f, 64.29031f, "None"),
				new Vector3(4727.948f, 9866.546f, 64.28621f, "None"),
				//
				new Vector3(4732.345f, 9870.513f, 64.28891f, "None"),
				new Vector3(4737.655f, 9870.644f, 64.28891f, "None"),
				new Vector3(4734.451f, 9872.519f, 64.28891f, "None"),
				//
				new Vector3(4721.382f, 9864.66f, 64.48296f, "None"),
				new Vector3(4718.301f, 9863.749f, 61.92882f, "None"),
				new Vector3(4714.95f, 9862.801f, 59.30589f, "None"),
				new Vector3(4711.512f, 9863.46f, 57.58124f, "None"),
				new Vector3(4708.233f, 9864.582f, 56.15429f, "None"),
				new Vector3(4704.896f, 9865.912f, 56.14085f, "None"),
				new Vector3(4699.33f, 9870.004f, 55.77478f, "None"),
				new Vector3(4696.46f, 9872.151f, 55.77483f, "None"),
				new Vector3(4690.819f, 9876.313f, 55.3103f, "None"),
				new Vector3(4687.697f, 9877.795f, 54.04862f, "None"),
				new Vector3(4684.318f, 9878.61f, 52.83521f, "None"),
				new Vector3(4680.774f, 9878.662f, 51.58945f, "None"),
				new Vector3(4677.41f, 9878.119f, 50.39554f, "None"),
				new Vector3(4674.066f, 9876.879f, 49.16621f, "None"),
				new Vector3(4671.05f, 9875.121f, 47.93072f, "None"),
				new Vector3(4668.188f, 9873.07f, 46.66597f, "None"),
				new Vector3(4665.338f, 9871.074f, 45.43304f, "None"),
				new Vector3(4662.498f, 9869.152f, 44.28737f, "None"),
				new Vector3(4659.542f, 9867.307f, 43.22178f, "None"),
				new Vector3(4656.325f, 9865.764f, 42.25885f, "None"),
				new Vector3(4653.007f, 9864.746f, 41.74153f, "None"),
				new Vector3(4649.562f, 9864.291f, 40.46704f, "None"),
				new Vector3(4646.041f, 9864.55f, 40.46747f, "None"),
				new Vector3(4640.234f, 9862.358f, 40.47208f, "None"),
				new Vector3(4637.805f, 9855.824f, 40.50006f, "None"),
				new Vector3(4636.682f, 9852.45f, 40.81742f, "None"),
				new Vector3(4635.4f, 9845.565f, 40.4589f, "None"),
				new Vector3(4635.49f, 9838.617f, 40.2531f, "None"),
				new Vector3(4638.286f, 9832.359f, 40.54211f, "None"),
				new Vector3(4640.285f, 9829.487f, 40.93464f, "None"),
				new Vector3(4643.054f, 9827.365f, 40.54893f, "None"),
				new Vector3(4648.888f, 9830.673f, 40.50171f, "None"),
				new Vector3(4651.134f, 9833.436f, 40.46887f, "None"),
				new Vector3(4652.553f, 9836.654f, 40.46887f, "None"),
				new Vector3(4655.304f, 9842.961f, 40.53946f, "None"),
				new Vector3(4658.023f, 9845.168f, 40.53792f, "None"),
				new Vector3(4664.434f, 9847.76f, 40.53792f, "None"),
				new Vector3(4667.751f, 9849.144f, 40.53792f, "None"),
				new Vector3(4670.583f, 9851.151f, 39.94507f, "None"),
				new Vector3(4673.234f, 9853.393f, 39.4949f, "None"),
				new Vector3(4679.192f, 9857.043f, 39.66972f, "None"),
				new Vector3(4686.09f, 9857.595f, 39.52822f, "None"),
				new Vector3(4692.889f, 9856.339f, 39.93702f, "None"),
				new Vector3(4696.368f, 9855.712f, 40.53844f, "None"),
				new Vector3(4703.269f, 9856.657f, 40.53844f, "None"),
				new Vector3(4710.042f, 9858.184f, 40.53844f, "None"),
				new Vector3(4713.539f, 9858.972f, 40.76598f, "None"),
				new Vector3(4720.378f, 9860.288f, 40.76392f, "None"),
				new Vector3(4724.379f, 9860.979f, 40.91229f, "None"),
			};
		}
		//krokuun middle base
		internal class KrokulHovel
		{
			internal static void OnMovementPulse(List<Vector3> points, System.ComponentModel.CancelEventArgs cancelable)
			{
				if (!InKrokuunOpen)
					return;

				if (points == null)
					return;
				var myPos = ObjectManager.Me.Position;
				var pointsCount = points.Count;
				if (pointsCount < 1)
					return;

				var end = points[points.Count - 1];
			}
		}
		//S from krorul hovel
		internal class DarkfallRidge
		{
			internal static void OnMovementPulse(List<Vector3> points, System.ComponentModel.CancelEventArgs cancelable)
			{
				if (!InKrokuunOpen)
					return;

				if (points == null)
					return;
				var myPos = ObjectManager.Me.Position;
				var pointsCount = points.Count;
				if (pointsCount < 1)
					return;

				var end = points[points.Count - 1];
			}
		}
		//N of Krokuun
		internal class NathraxasHold
		{
			internal static void OnMovementPulse(List<Vector3> points, System.ComponentModel.CancelEventArgs cancelable)
			{
				if (!InKrokuunOpen)
					return;

				if (points == null)
					return;
				var myPos = ObjectManager.Me.Position;
				var pointsCount = points.Count;
				if (pointsCount < 1)
					return;

				var end = points[points.Count - 1];
			}
		}
		//S of Krokuun
		internal class ShatteredFieldsCliff
		{
			internal static bool IsInside(Vector3 p)
			{
				return !InKrokuunShatteredFieldsCliff;
			}
			internal static void OnMovementPulse(List<Vector3> points, System.ComponentModel.CancelEventArgs cancelable)
			{
				if (!InKrokuunOpen)
					return;

				if (points == null)
					return;
				var myPos = ObjectManager.Me.Position;
				var pointsCount = points.Count;
				if (pointsCount < 1)
					return;

				var end = points[points.Count - 1];
			}
		}
		//S mac'aree 
		internal class TriumviratesEnd
		{
			internal static Vector3 Pylon3 = new Vector3(5027.635, 9870.633, -76.49744);
			static List<Vector3> PathToPylon3 = new List<Vector3>()
			{
				new Vector3(4989.852f, 9834.66f, -79.22293f, "None"),
				new Vector3(4995.672f, 9839.013f, -78.81702f, "None"),
				new Vector3(5000.911f, 9843.524f, -78.50297f, "None"),
				new Vector3(5005.154f, 9849.115f, -78.09761f, "None"),
				new Vector3(5009.672f, 9854.396f, -78.09761f, "None"),
				new Vector3(5014.718f, 9859.407f, -78.29232f, "None"),
				new Vector3(5019.76f, 9864.109f, -77.85727f, "None"),
				new Vector3(5024.31f, 9868.215f, -76.76617f, "None"),
			};
			internal static bool IsNearPylon3(Vector3 p)
			{
				return Pylon3.DistanceTo2D(p) < 30;
			}
			internal static void OnMovementPulse(List<Vector3> points, System.ComponentModel.CancelEventArgs cancelable)
			{
				if (!InMacAreeOpen)
					return;
				var myPos = ObjectManager.Me.Position;
				var isNear = IsNearPylon3(myPos);
				if (isNear || points.Count < 1)
					return;

				var end = points[points.Count - 1];
				foreach (var p in points)
				{
					if (IsNearPylon3(p))
					{
						var success = false;
						var path = PathFinder.FindPath(PathToPylon3[0], out success);
						if (success)
						{
							var old = points.Count;
							points.Clear();
							points.AddRange(path);
							var path2 = Questing.PathClamp(PathToPylon3, PathToPylon3[0], end);
							points.AddRange(path2);
							Log("Mac'Aree: Triumvirate's End fix path to pylon3 from outside path=" + old + " to=" + points.Count);
							return;
						}
					}
				}
			}
		}
	}
	#endregion

	static void Log(string text)
	{
		Logging.Write("[Argus Helper] " + text);
	}

	public static class Warframe
	{
		static Vector3 _destination = Vector3.Zero;
		//1 248292/judgment-blast dist=60 cd=1.5
		//2 251509/purifying-flame dist=14 cd=6
		//3 251485/crusader-leap dist=10-60 cd=10
		//5 251569/vindication cast=6s cd=15 nomove
		static void Trace(string text)
		{
			Log("Warframe: " + text);
		}
		public static bool Can
		{
			get
			{
				return ObjectManager.Me.PlayerUsingVehicle && ObjectManager.Me.HaveBuff(250924); //  Fel Sludge Immunity: ID=250924
			}
		}
		public static bool Pulse(params int[] mobsID)
		{
			if (!Can)
				return false;

			var mob = Questing.FindUnit(mobsID);
			if (mob != null && mob.IsValid && mob.IsAlive && mob.IsAttackable)
			{
				Trace("target=" + mob.Name);
				Interact.InteractGameObject(mob.GetBaseAddress, false, false, true);
				if (!MovementManager.IsFacing(ObjectManager.Me.Position, ObjectManager.Me.Rotation, mob.Position))
				{
					MovementManager.Face(mob);
					Thread.Sleep(Usefuls.Latency * 2);
				}
				return Pulse(mob.Position);
			}
			return Pulse(Vector3.Zero);
		}
		public static bool Pulse(Vector3 dest, float dist = 5f)
		{
			if (!Can)
				return false;

			_destination = dest;
			if (_destination != null && _destination != Vector3.Zero && ObjectManager.Me.Position.DistanceTo(dest) > dist)
			{
				Trace("goto " + _destination.ToStringNewVector());
				GoToTask.ToPosition(_destination, 9, false, (c) => { return Pulse(); });
			}
			return Pulse();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns>continue other actions or not</returns>
		public static bool Pulse()
		{
			if (!Can)
				return false;

			if (TryVindication())
				return false;

			if (TryCrusaderLeap())
				return false;

			TryPurifyingFlame();
			TryJudgmentBlast();
			return true;
		}
		public static bool TryCrusaderLeap()
		{
			if (_destination == null || _destination == Vector3.Zero)
				return false;

			var dist = ObjectManager.Me.Position.DistanceTo(_destination);
			if (dist > 10 && dist < 60 && Questing.NoCooldownSpell(251485))
			{
				Trace("#3 jump at targ=" + _destination.ToStringNewVector());
				Questing.Vehicle.UseButton(3);
				Thread.Sleep(Usefuls.Latency * 2);
				ClickOnTerrain.Pulse(_destination);
				Questing.Wait(2);
				return true;
			}
			return false;
		}
		public static bool TryVindication()
		{
			if (Questing.NoCooldownSpell(251569) && ObjectManager.Pet.HealthPercent < 50)
			{
				Trace("#5 heal");
				Questing.Vehicle.UseButton(5);
				Thread.Sleep(Usefuls.Latency * 2);
				Usefuls.WaitIsCasting();
				return true;
			}
			return false;
		}
		public static bool TryPurifyingFlame()
		{
			if (Questing.NoCooldownSpell(251509))
			{
				Trace("#2 attack");
				Questing.Vehicle.UseButton(2);
				return true;
			}
			return false;
		}
		public static bool TryJudgmentBlast()
		{
			if (Questing.NoCooldownSpell(248292))
			{
				//Trace("#1 attack");
				Questing.Vehicle.UseButton(1);
				return true;
			}
			return false;
		}
	}

	public class Invasion
	{
		public Invasion()
		{
		}
		internal static void Log(string text)
		{
			Logging.WriteDebug("[Argus Invasion] " + text);
		}
		public static class MobID
		{
			//greater boss
			public static int MatronFolnuna = 124514;
			public static int PitLordVilemus = 124719;
			public static int InquisitorMeto = 124592;
			public static int Occularus = 124492;
			public static int Sotanathor = 124555;
			public static int MistressAlluradel = 124625;
			public static List<int> BossesGreater = new List<int>() { MatronFolnuna, PitLordVilemus, InquisitorMeto, Occularus, Sotanathor, MistressAlluradel };
			//normal boss
			public static int Mazgoroth = 125137;
			public static int Gorgoloth = 125148;
			public static int DreadKnightZakgal = 125252;
			public static int FelLordKazral = 125272;
			public static int FlamecallerVezrah = 125280;
			public static int FlameweaverVerathix = 125314;
			public static int HarbingerDrelnathar = 125483;
			public static int DreadbringerValus = 125527;
			public static int Malphazel = 125578;
			public static int VogretharTheDefiled = 125587;
			public static int VelthrakThePunisher = 125634;
			public static int FlamebringerAzrothel = 125655;
			public static int Baldrazar = 125666;
			public static List<int> Bosses = new List<int>() { Mazgoroth, Gorgoloth, DreadKnightZakgal, FelLordKazral, FlamecallerVezrah, FlameweaverVerathix, HarbingerDrelnathar, DreadbringerValus, Malphazel, VogretharTheDefiled, VelthrakThePunisher, FlamebringerAzrothel, Baldrazar, };
			//mobs / common
			public static int BlacksoulInquisitor = 125779;
			public static int BladeswornDisciple = 125788;
			public static int ConqueringRiftstalker = 125199;
			public static int FelflameSubjugator = 125197;
			public static int FelrageMarauder = 125781;
			public static int WickedCorruptor = 125777;
			public static int WickedCorruptor2 = 125776;
			public static int BurningHound = 125936;
			public static int ChitteringFiend = 125921;
			public static int FelflameInvader = 125757;
			public static int FelflameInvader2 = 125755;
			public static int FelfrenzyBerserker = 125785;
			public static int FrothingRazorwing = 125933;
			public static int ShadowbladeAcolyte = 125790;
			public static int ShadowswornBetrayer = 125758;
			public static int ShadowswornBetrayer2 = 125759;
			public static int HatefulScamp = 125931;
			public static int DeepTerror = 126275;
			public static int WyrmtongueBloodhauler = 125963;
			public static int RedwoodGuardian = 127569;
			public static int RedwoodGuardian2 = 127611;
			public static int VengefulConqueror = 125745;
			public static int CrazedCorruptor = 126231;
			//mobs / specific
			public static int FragmentOfArgus = 125254;
			public static int ExplosiveOrb = 125656;
			public static int GrippingShadows = 125667;
			public static int ShadowyIllusion = 125579;
			public static List<int> Regular = new List<int>()
			{
				BlacksoulInquisitor,
				BladeswornDisciple,
				ConqueringRiftstalker,
				FelflameSubjugator,
				FelrageMarauder,
				WickedCorruptor,
				WickedCorruptor2,
				BurningHound,
				FelflameInvader,
				FelflameInvader2,
				FelfrenzyBerserker,
				FrothingRazorwing,
				ShadowbladeAcolyte,
				ShadowswornBetrayer,
				ShadowswornBetrayer2,
				HatefulScamp,
				DeepTerror,
				WyrmtongueBloodhauler,
				RedwoodGuardian,
				RedwoodGuardian2,
				VengefulConqueror,
				//
				FragmentOfArgus,
				ExplosiveOrb,
				GrippingShadows,
				ShadowyIllusion,
			};
		}
		public static bool InRift { get { return Usefuls.ContinentId == (int)ContinentId.Argus_Rifts; } }

		public static int MinBossHitpointsPercent = 95;
		public static int MinBossHitpoints = 2 * 1000 * 1000;

		#region QUESTS
		public class RiftBase
		{
			internal int _area = -1;
			internal int _portal = -1;
			internal List<int> _mobs = new List<int>();
			internal List<Vector3> _hotpots = new List<Vector3>();
			internal List<Vector3> _exit = new List<Vector3>();
			public virtual bool Pulse()
			{
				return true;
			}
			public virtual bool Exit()
			{
				var port = Questing.FindUnit(Portals.Out);
				if (port != null && port.IsValid)
				{
					Log("Using portal exit.");
					GoToTask.ToPositionAndIntecractWithNpc(port.Position, port.Entry);
				}
				else if (_exit.Count > 0)
				{
					var p = _exit[0];
					Log("Going to poral exit");
					GoToTask.ToPosition(p);
				}
				return true;
			}
		}
		public class RiftNormal : RiftBase
		{
			public override bool Pulse()
			{
				var stage = Questing.Scenario.Stage;
				if (stage == 3)
				{
					var boss = Questing.FindUnit(MobID.Bosses);
					if (boss != null && boss.IsValid && boss.IsAlive && boss.IsAttackable && (boss.HealthPercent < MinBossHitpointsPercent || boss.Health < MinBossHitpoints))
					{
						Log("Attacking boss " + boss.Name);
						Questing.Attack(boss);
					}
					else if (_hotpots.Count > 0)
					{
						var p = _hotpots[0];
						if (ObjectManager.Me.Position.DistanceTo2D(p) > 15)
						{
							Log("Camping boss at " + p.ToStringNewVector() );
							GoToTask.ToPosition(p, 10, false, (c)=>
							{
								if (ObjectManager.GetUnitAttackPlayer().Count > 0)
								{
									MountTask.DismountMount();
									return false;
								}
								return true;
							});
						}
					}
					return true;
				}
				else if (stage == 0)
				{
					Log("Out of rift.");
					Exit();
				}
				else
				{
					Questing.Grind(_mobs, _hotpots);
				}
				return true;
			}
		}
		public class RiftGreater : RiftBase
		{
			internal float _range = 50;
			internal int _questID = -1;
			public override bool Pulse()
			{
				if (_questID < 0)
					Log("Greater Rift quest for area = " + _area + " doesnt have questID=" + _questID + ".");

				if (_questID < 0 || Quest.GetQuestCompleted(_questID))
				{
					Exit();
					return true;
				}
				var boss = Questing.FindUnit(_mobs);
				if (boss != null && boss.IsValid && boss.IsAlive && boss.IsAttackable && (boss.HealthPercent < MinBossHitpointsPercent || boss.Health < MinBossHitpoints))
				{
					Log("Attacking Greater Boss " + boss.Name + "!");
					Questing.Attack(boss);
				}
				else if (_hotpots.Count > 0)
				{
					var p = _hotpots[0];
					if (ObjectManager.Me.Position.DistanceTo2D(p) > _range)
					{
						Log("Moving to a safe position " + _range +"y away from Greater Boss " + boss.Name + " to camp until the condiditions are met.");
						if (GoToTask.ToPosition(p))
						{
							MountTask.DismountMount();
						}
						return true;
					}
				}
				else
				{
					Log("Greater Rift quest for area = " + _area + " doesnt have hotspots=" + _hotpots.Count);
				}
				return true;
			}
		}
		#endregion QUESTS

		#region RIFTS NORMAL
		//tested 1
		public class Val : RiftNormal
		{
			public const int Area = 9127;
			public const int Portal = 126499;
			public static List<Vector3> Exits = new List<Vector3>() { new Vector3(-4170.895, 663.2453, 20.1905, "None"), };
			public static List<Vector3> Hotspots = new List<Vector3>()
			{
				new Vector3(-4102.611, 602.3239, 9.893845, "None"),
				new Vector3(-4004.833, 584.2082, 0.05630473, "None"),
				new Vector3(-4066.593, 605.5196, 4.601417, "None"),
				new Vector3(-4015.308, 600.5054, 2.307814, "None"),
			};
			public static List<int> Mobs = MobID.Regular;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public Val() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; }
			public override bool Pulse()
			{
				var stage = Questing.Scenario.Stage;
				if (stage == 2)
				{
					var demonhunter = Questing.FindUnit(126446);
					if (demonhunter != null && demonhunter.IsValid && demonhunter.IsAlive)
					{
						Log("interact=" + demonhunter.Name);
						GoToTask.ToPositionAndIntecractWithNpc(demonhunter.Position, demonhunter.Entry);
					}
					else
					{
						var i = Others.Random(0, Hotspots.Count - 1);
						var p = Hotspots[i];
						Log("to hotspot #" + i + " at=" + p.ToStringNewVector());
						GoToTask.ToPosition(p);
					}
					return true;
				}
				return base.Pulse();
			}
		}
		//tested: 2
		public class Cengar : RiftNormal
		{
			public const int Area = 9126;
			public const int Portal = 126120;
			public static List<Vector3> Exits = new List<Vector3>() { new Vector3(654.0816, 596.4531, 40.225), };
			public static List<Vector3> Hotspots = new List<Vector3>() { new Vector3(664.9532, 643.9272, 40.22417, "None"), };
			public static List<int> Mobs = MobID.Regular;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public static float MinZ = 39.25407f;//bellow this Z -> in lava, force run to near hotspot
			public Cengar()
			{
				_area = Area;
				_hotpots = Hotspots;
				_exit = Exits;
				_mobs = Mobs;
				_portal = Portal;
				StartCengarFix();
			}
			void StartCengarFix()
			{
				StopCengarFix();
				wManager.Events.FightEvents.OnFightLoop += OnFightLoop;
				Log("start cengar fight fix");
			}
			void StopCengarFix()
			{
				wManager.Events.FightEvents.OnFightLoop -= OnFightLoop;
				Log("stop cengar fight fix");
			}
			void OnFightLoop(WoWUnit unit, System.ComponentModel.CancelEventArgs cancelable)
			{
				if (!IsInside)
				{
					StopCengarFix();
					return;
				}
				if (ObjectManager.Me.Position.Z < MinZ)
				{
					cancelable.Cancel = true;
					var p = _hotpots.OrderBy(v => ObjectManager.Me.Position.DistanceTo2D(v)).FirstOrDefault();
					if (p != null && p != Vector3.Zero)
					{
						//GoToTask.ToPosition(p);
						Log("get out from lava");
						Questing.ClickMove(p);
						Questing.Wait(5);
					}
				}
			}
			public override bool Pulse()
			{
				var stage = Questing.Scenario.Stage;
				if (stage == 1)
				{
					Questing.Grind(new List<int>() { 126230 }, _hotpots);
					return true;
				}
				else if (stage == 2)
				{
					//kill fire giants without buffs
					//var mob = ObjectManager.GetObjectWoWUnit().Where(u => u.IsValid && u.Entry == 124835 && u.IsAlive && u.IsAttackable && !u.HaveBuff(251888)).OrderBy(o => o.GetDistance).FirstOrDefault();
					//Questing.Attack(mob);
					Questing.Grind(new List<int>() { 124835 }, _hotpots);
					return true;
				}
				return base.Pulse();
			}
		}
		//tested 2
		public class Aurinor : RiftNormal
		{
			public const int Area = 9100;
			public const int Portal = 125849;
			public static List<Vector3> Exits = new List<Vector3>() { new Vector3(-4066.524, -4659.221, 80.46143), };
			public static List<Vector3> Hotspots = new List<Vector3>() { new Vector3(-4033.093, -4945.625, 121.7319, "None"), new Vector3(-4076.119, -4868.174, 112.4614, "None"), new Vector3(-4073.653, -4759.229, 85.88012, "None"), };
			public static List<int> Mobs = MobID.Regular;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public Aurinor()
			{
				_area = Area;
				_hotpots = Hotspots;
				_exit = Exits;
				_mobs = Mobs;
				_portal = Portal;
				StartPathfindingFix();
			}
			public override bool Pulse()
			{
				var stage = Questing.Scenario.Stage;
				if (stage == 2)
				{
					Questing.Grind(new List<int>() { 125856 }, _hotpots);
					return true;
				}
				else if (stage == 3)
				{
					_hotpots = new List<Vector3>() { new Vector3(-4061.876, -4688.732, 80.46176, "None"), };
				}
				return base.Pulse();
			}
			static void StartPathfindingFix()
			{
				StopPathfindingFix();
				wManager.Events.MovementEvents.OnMovementPulse += OnMovementPulse;
				Log("Aurinor path fix start");
			}
			static void StopPathfindingFix()
			{
				wManager.Events.MovementEvents.OnMovementPulse -= OnMovementPulse;
				Log("Aurinor path fix stop");
			}
			static void OnMovementPulse(List<Vector3> points, System.ComponentModel.CancelEventArgs cancelable)
			{
				if (!IsInside)
				{
					StopPathfindingFix();
					return;
				}
				if (points == null)
					return;
				var count = points.Count;
				if (count < 2)
					return;

				var start = points[0];
				var end = points[count - 1];
				if (!TraceLine.TraceLineGo(end))
					return;

				points.Clear();
				var pathMain = Questing.PathClampDirected(Path, start, end);
				if (pathMain.Count < 1)
					return;

				var startMain = pathMain[0];
				var countStart = 0;
				if (TraceLine.TraceLineGo(startMain))
				{
					var pathStart = PathFinder.FindPath(start, startMain);
					countStart = pathStart.Count;
					points.AddRange(pathStart);
				}
				else
				{
					points.Add(start);
				}
				points.AddRange(pathMain);
				var mainEnd = pathMain[pathMain.Count - 1];
				var countEnd = 0;
				if (TraceLine.TraceLineGo(mainEnd))
				{
					var pathEnd = PathFinder.FindPath(mainEnd, end);
					countEnd = pathEnd.Count;
					points.AddRange(pathEnd);
				}
				Log("Aurinor path fixed. from=" + count + " to=" + points.Count + " start=" + countStart + " main=" + pathMain.Count + " end=" + countEnd);
			}
			static List<Vector3> Path = new List<Vector3>()
			{
				new Vector3(-4025.564f, -4992.333f, 128.8848f, "None"),
				new Vector3(-4016.318f, -4983.517f, 128.8458f, "None"),
				new Vector3(-4011.164f, -4978.282f, 127.8125f, "None"),
				new Vector3(-4007.015f, -4972.218f, 125.9949f, "None"),
				new Vector3(-4004.714f, -4965.212f, 124.1661f, "None"),
				new Vector3(-4004.234f, -4957.897f, 122.6991f, "None"),
				new Vector3(-4006.113f, -4950.851f, 121.2496f, "None"),
				new Vector3(-4009.024f, -4944.099f, 119.8396f, "None"),
				new Vector3(-4013.073f, -4938.024f, 118.5348f, "None"),
				new Vector3(-4017.793f, -4932.315f, 117.0585f, "None"),
				new Vector3(-4022.745f, -4926.983f, 115.9411f, "None"),
				new Vector3(-4027.783f, -4921.612f, 115.2574f, "None"),
				new Vector3(-4032.689f, -4916.067f, 114.6451f, "None"),
				new Vector3(-4036.646f, -4909.876f, 113.8957f, "None"),
				new Vector3(-4040.125f, -4903.438f, 113.1196f, "None"),
				new Vector3(-4043.048f, -4896.679f, 112.6042f, "None"),
				new Vector3(-4045.94f, -4889.906f, 112.1279f, "None"),
				new Vector3(-4049.167f, -4883.274f, 111.582f, "None"),
				new Vector3(-4053.404f, -4877.33f, 111.3725f, "None"),
				new Vector3(-4057.894f, -4871.585f, 111.2205f, "None"),
				new Vector3(-4062.85f, -4866.124f, 110.8011f, "None"),
				new Vector3(-4074.533f, -4857.14f, 110.0281f, "None"),
				new Vector3(-4081.032f, -4853.886f, 109.3773f, "None"),
				new Vector3(-4087.624f, -4850.769f, 108.5416f, "None"),
				new Vector3(-4094.077f, -4847.196f, 107.9549f, "None"),
				new Vector3(-4099.733f, -4842.314f, 107.8856f, "None"),
				new Vector3(-4103.973f, -4836.387f, 107.882f, "None"),
				new Vector3(-4106.978f, -4829.721f, 107.8183f, "None"),
				new Vector3(-4109.365f, -4822.818f, 107.0545f, "None"),
				new Vector3(-4112.172f, -4815.994f, 105.9499f, "None"),
				new Vector3(-4111.906f, -4808.731f, 104.8956f, "None"),
				new Vector3(-4117.56f, -4799.142f, 104.3143f, "None"),
				new Vector3(-4122.608f, -4793.928f, 103.5937f, "None"),
				new Vector3(-4126.129f, -4787.473f, 102.1221f, "None"),
				new Vector3(-4127.136f, -4780.277f, 100.7896f, "None"),
				new Vector3(-4124.586f, -4773.502f, 99.35542f, "None"),
				new Vector3(-4118.481f, -4769.553f, 97.54041f, "None"),
				new Vector3(-4111.322f, -4768.311f, 95.11562f, "None"),
				new Vector3(-4103.973f, -4767.881f, 92.26426f, "None"),
				new Vector3(-4089.338f, -4768.954f, 88.09329f, "None"),
				new Vector3(-4082.054f, -4769.608f, 87.20068f, "None"),
				new Vector3(-4075.191f, -4767.11f, 86.60323f, "None"),
				new Vector3(-4071.413f, -4760.931f, 85.86458f, "None"),
				new Vector3(-4069.637f, -4753.786f, 85.11414f, "None"),
				new Vector3(-4069.093f, -4746.534f, 84.6414f, "None"),
				new Vector3(-4068.798f, -4739.189f, 83.94742f, "None"),
				new Vector3(-4068.51f, -4732.036f, 82.89617f, "None"),
				new Vector3(-4068.209f, -4724.575f, 81.82955f, "None"),
				new Vector3(-4067.519f, -4717.11f, 80.70802f, "None"),
				new Vector3(-4067.115f, -4709.935f, 80.4714f, "None"),
				new Vector3(-4066.892f, -4702.559f, 80.46216f, "None"),
				new Vector3(-4066.982f, -4695.209f, 80.46216f, "None"),
				new Vector3(-4067.169f, -4687.832f, 80.46216f, "None"),
			};
		}
		//tested 2
		public class Naigtal : RiftNormal
		{
			public const int Area = 9102;
			public const int Portal = 126593;
			public static List<Vector3> Exits = new List<Vector3>() { new Vector3(-1753.264, -1410.38, 27.798), };
			public static List<Vector3> Hotspots = new List<Vector3>()
			{
				new Vector3(-1781.135, -1449.18, 16.94704, "None"),
				new Vector3(-1787.167, -1532.51, 4.557395, "None"),
				new Vector3(-1898.394, -1515.631, 7.051389, "None"),
			};
			public static List<int> Mobs = MobID.Regular;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public Naigtal() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; }
			public override bool Pulse()
			{
				var stage = Questing.Scenario.Stage;
				if (stage == 1)
				{
					Questing.Grind(new List<int>() { 125958 }, _hotpots);
					return true;
				}
				else if (stage == 3)
				{
					_hotpots = new List<Vector3>() { new Vector3(-1858.271, -1565.391, -0.2610433, "None"), };
				}
				return base.Pulse();
			}
		}
		//tested 2
		public class Sangua : RiftNormal
		{
			public const int Area = 9128;
			public const int Portal = 125863;
			public static List<Vector3> Exits = new List<Vector3>() { new Vector3(-1390.74, 761.0278, 62.40871), };
			public static List<Vector3> Hotspots = new List<Vector3>()
			{
				new Vector3(-1481.606, 759.5313, 61.09289, "None"),
				new Vector3(-1413.726, 775.7604, 61.09098, "None"),
				new Vector3(-1390.267, 734.3663, 61.08427, "None"),
				new Vector3(-1340.552, 815.0903, 61.09292, "None"),
				new Vector3(-1481.606, 759.5313, 61.09289, "None"),
				new Vector3(-1413.726, 775.7604, 61.09098, "None"),
				new Vector3(-1390.267, 734.3663, 61.08427, "None"),
				new Vector3(-1340.552, 815.0903, 61.09292, "None"),
			};
			public static List<int> Mobs = MobID.Regular;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public Sangua() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; }
			public override bool Pulse()
			{
				var stage = Questing.Scenario.Stage;
				if (stage == 1)
				{
					Questing.Grind(new List<int>() { 125874 }, _hotpots);
					return true;
				}
				else if (stage == 3)
				{
					_hotpots = new List<Vector3>() { new Vector3(-1440.662, 793.8834, 64.87183, "None"), }; // old { new Vector3(-3750.61, -8077.96, 1.104051, "None"), };
				}
				return base.Pulse();
			}
		}
		//tested 2
		public class Bonich : RiftNormal
		{
			public const int Area = 9180;
			public const int Portal = 126547;
			public static List<Vector3> Exits = new List<Vector3>() { };
			public static List<Vector3> Hotspots = new List<Vector3>()
			{
				new Vector3(-3795.264, -8127.464, 7.570378, "None"),
				new Vector3(-3714.877, -8076.997, 0.8833292, "None"),
				new Vector3(-3813.733, -8045.342, 0.9020075, "None"),
				new Vector3(-3686.788, -8045.567, 3.918347, "None"),
				new Vector3(-3742.601, -7942.385, 1.102061, "None"),
				new Vector3(-3747.285, -7953.367, 0.9858366, "None"),
			};
			public static List<int> Mobs = MobID.Regular;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public Bonich() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; }
			public override bool Pulse()
			{
				var stage = Questing.Scenario.Stage;
				if (stage == 2)
				{
					Questing.Grind(new List<int>() { 127982 }, _hotpots);
					return true;
				}
				else if (stage == 3)
				{
					_hotpots = new List<Vector3>() { new Vector3(-3743.218, -8061.853, 0.889137, "None"), };
				}
				return base.Pulse();
			}
		}
		#endregion RIFTS NORMAL

		#region RIFTS GREATER
		public class MatronFolnuna : RiftGreater
		{
			public const int Area = 9295;
			public const int Portal = 127528;
			public const int QuestID = 49169;
			public const int QuestBonusLootID = 49173;
			public static List<Vector3> Exits = new List<Vector3>() { new Vector3(4498.033, 6591.74, 41.91304), };
			public static List<Vector3> Hotspots = new List<Vector3>() { new Vector3(4425.705, 6565.272, 41.96574, "None"), };
			public static List<int> Mobs = MobID.BossesGreater;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public MatronFolnuna() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; _questID = QuestID; }
		}
		public class PitLordVilemus : RiftGreater
		{
			public const int Area = -2;
			public const int Portal = -1;
			public const int QuestID = 49168;
			public const int QuestBonusLootID = 49174;
			public static List<Vector3> Exits = new List<Vector3>() { };
			public static List<Vector3> Hotspots = new List<Vector3>() { };
			public static List<int> Mobs = MobID.BossesGreater;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public PitLordVilemus() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; _questID = QuestID; }
		}
		public class InquisitorMeto : RiftGreater
		{
			public const int Area = -3;
			public const int Portal = -1;
			public const int QuestID = 49166;
			public const int QuestBonusLootID = -1;
			public static List<Vector3> Exits = new List<Vector3>() { };
			public static List<Vector3> Hotspots = new List<Vector3>() { };
			public static List<int> Mobs = MobID.BossesGreater;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public InquisitorMeto() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; _questID = QuestID; }
		}
		public class Occularus : RiftGreater
		{
			public const int Area = -4;
			public const int Portal = -1;
			public const int QuestID = 49170;
			public const int QuestBonusLootID = -1;
			public static List<Vector3> Exits = new List<Vector3>() { };
			public static List<Vector3> Hotspots = new List<Vector3>() { };
			public static List<int> Mobs = MobID.BossesGreater;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public Occularus() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; _questID = QuestID; }
		}
		//tested 1
		public class Sotanathor : RiftGreater
		{
			public const int Area = 9299;
			public const int Portal = 127532;
			public const int QuestID = 49171;	//good
			public const int QuestBonusLootID = -1;
			public static List<Vector3> Exits = new List<Vector3>() { new Vector3(-1385.455, 8355.195, 94.52447, "None"), };
			public static List<Vector3> Hotspots = new List<Vector3>() { new Vector3(-1431.579, 8236.357, 62.15427, "None"), };
			public static List<int> Mobs = MobID.BossesGreater;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public Sotanathor() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; _questID = QuestID; }
		}
		public class MistressAlluradel : RiftGreater
		{
			public const int Area = 9300;
			public const int Portal = 127536;
			public const int QuestID = 49167;
			public const int QuestBonusLootID = -1;
			public static List<Vector3> Exits = new List<Vector3>() { new Vector3(5259.431, -9772.651, 10.80953), };
			public static List<Vector3> Hotspots = new List<Vector3>() { new Vector3(5303.167, -9685.952, 0.9761651, "None"), };
			public static List<int> Mobs = MobID.BossesGreater;
			public static bool IsInside { get { return InRift && Usefuls.AreaId == Area; } }
			public MistressAlluradel() { _area = Area; _hotpots = Hotspots; _exit = Exits; _mobs = Mobs; _portal = Portal; _questID = QuestID; }
		}
		#endregion RIFTS GREATER

		#region LOGIC
		internal static RiftBase _current;
		public static bool Work()
		{
			if (!InRift)
			{
				_current = null;
				return false;
			}

			if (_current == null)
			{
				switch (Usefuls.AreaId)
				{
					//normal
					case Cengar.Area:
						Log("started Cengar");
						_current = new Cengar();
						break;
					case Naigtal.Area:
						Log("started Naigtal");
						_current = new Naigtal();
						break;
					case Val.Area:
						Log("started Val");
						_current = new Val();
						break;
					case Sangua.Area:
						Log("started Sangua");
						_current = new Sangua();
						break;
					case Bonich.Area:
						Log("started Bonich");
						_current = new Bonich();
						break;
					case Aurinor.Area:
						Log("started Aurinor");
						_current = new Aurinor();
						break;
					//greater
					case MatronFolnuna.Area:
						Log("started MatronFolnuna");
						_current = new MatronFolnuna();
						break;
					case PitLordVilemus.Area:
						Log("started PitLordVilemus");
						_current = new PitLordVilemus();
						break;
					case InquisitorMeto.Area:
						Log("started InquisitorMeto");
						_current = new InquisitorMeto();
						break;
					case Occularus.Area:
						Log("started Occularus");
						_current = new Occularus();
						break;
					case Sotanathor.Area:
						Log("started Sotanathor");
						_current = new Sotanathor();
						break;
					case MistressAlluradel.Area:
						Log("started MistressAlluradel");
						_current = new MistressAlluradel();
						break;
					default:
						Log("unknown rift area " + Usefuls.AreaId);
						return true;
				}
			}
			_current.Pulse();
			return true;
		}
		#endregion LOGIC

		public static class Portals
		{
			public static float NearRange = 100f;
			public static string IconRift = "poi-rift1";
			public static string IconRiftGreater = "poi-rift2";

			static List<Vector3> CurrentKrokuun = new List<Vector3>();
			static List<Vector3> CurrentKrokuunGreater = new List<Vector3>();
			static List<Vector3> CurrentAntoranWastes = new List<Vector3>();
			static List<Vector3> CurrentAntoranWastesGreater = new List<Vector3>();
			static List<Vector3> CurrentMacAree = new List<Vector3>();
			static List<Vector3> CurrentMacAreeGreater = new List<Vector3>();

			static robotManager.Helpful.Timer TimerKrokuun = new robotManager.Helpful.Timer(30 * 1000);
			static robotManager.Helpful.Timer TimerKrokuunGreater = new robotManager.Helpful.Timer(30 * 1000);
			static robotManager.Helpful.Timer TimerAntoranWastes = new robotManager.Helpful.Timer(30 * 1000);
			static robotManager.Helpful.Timer TimerAntoranWastesGreater = new robotManager.Helpful.Timer(30 * 1000);
			static robotManager.Helpful.Timer TimerMacAree = new robotManager.Helpful.Timer(30 * 1000);
			static robotManager.Helpful.Timer TimerMacAreeGreater = new robotManager.Helpful.Timer(30 * 1000);
			static Portals()
			{
				TimerKrokuun.ForceReady();
				TimerKrokuunGreater.ForceReady();
				TimerAntoranWastes.ForceReady();
				TimerAntoranWastesGreater.ForceReady();
				TimerMacAree.ForceReady();
				TimerMacAreeGreater.ForceReady();
			}
			public static bool UseNear()
			{
				var port = FindNear();
				if (port != null && port.IsValid)
				{
					Log("found invasion portal " + port.Name + " at=" + port.Position.ToStringNewVector());
					GoToTask.ToPositionAndIntecractWithNpc(port.Position, port.Entry);
					return true;
				}
				return false;
			}
			public static WoWUnit FindNear(Vector3 p = null)
			{
				if (p == null || p == Vector3.Zero)
					p = ObjectManager.Me.Position;

				var all = new List<int>(Minor);
				all.AddRange(Greater);
				var port = Questing.FindUnit(all);
				if (IsActive(port))
					return port;

				return null;
			}
			public static bool IsActive(WoWUnit port)
			{
				if (port == null || !port.IsValid)
					return false;

				return IsActive(port.Position);
			}
			public static bool IsActive(Vector3 p)
			{
				if (p == null || p == Vector3.Zero)
					return false;

				var hotspot = GetNearActive(p);
				if (hotspot != null && hotspot != Vector3.Zero)
					return true;

				return false;
			}
			public static Vector3 GetNearActive(Vector3 p)
			{
				if (p == null || p == Vector3.Zero)
					p = ObjectManager.Me.Position;

				Update();

				Vector3 hotspot = null;

				hotspot = GetNearFromList(p, CurrentKrokuun);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				hotspot = GetNearFromList(p, CurrentKrokuunGreater);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				hotspot = GetNearFromList(p, CurrentAntoranWastes);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				hotspot = GetNearFromList(p, CurrentAntoranWastesGreater);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				hotspot = GetNearFromList(p, CurrentMacAree);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				hotspot = GetNearFromList(p, CurrentMacAreeGreater);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				return null;
			}
			public static Vector3 GetNearFromList(Vector3 p, List<Vector3> list)
			{
				return list.OrderBy(v => v.DistanceTo2D(p)).FirstOrDefault(v => v.DistanceTo2D(p) < NearRange);
			}
			public static Vector3 GetNearHotspot(Vector3 p)
			{
				if (p == null || p == Vector3.Zero)
					p = ObjectManager.Me.Position;

				Vector3 hotspot = null;
				
				hotspot = GetNearFromList(p, KrokuunHotspots);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				hotspot = GetNearFromList(p, AntoranWastesHotspots);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				hotspot = GetNearFromList(p, MacAreeHotspots);
				if (hotspot != null && hotspot != Vector3.Zero)
					return hotspot;

				return null;
			}
			public static int CountRegular
			{
				get
				{
					GetKrokuun();
					GetAntoranWastes();
					GetMacAree();
					return CurrentKrokuun.Count + CurrentAntoranWastes.Count + CurrentMacAree.Count;
				}
			}
			public static int CountGreater
			{
				get
				{
					GetKrokuunGreater();
					GetAntoranWastesGreater();
					GetMacAreeGreater();
					return CurrentKrokuunGreater.Count + CurrentAntoranWastesGreater.Count + CurrentMacAreeGreater.Count;
				}
			}
			public static int CountAll
			{
				get
				{
					return CountRegular + CountGreater;
				}
			}
			public static void UpdateKrokuun()
			{
				GetKrokuun();
				GetKrokuunGreater();
			}
			public static void UpdateAntotanWastes()
			{
				GetAntoranWastes();
				GetAntoranWastesGreater();
			}
			public static void UpdateMacAree()
			{
				GetMacAree();
				GetMacAreeGreater();
			}
			public static void UpdateRegular()
			{
				GetKrokuun();
				GetAntoranWastes();
				GetMacAree();
			}
			public static void UpdateGreater()
			{
				GetKrokuunGreater();
				GetAntoranWastesGreater();
				GetMacAreeGreater();
			}
			public static void Update()
			{
				UpdateRegular();
				UpdateGreater();
			}
			static List<Vector3> CorrectMapPositions(List<Vector3> mapPositions, List<Vector3> precisePositions)
			{
				var result = new List<Vector3>();
				foreach (var mapP in mapPositions)
				{
					var near = GetNearFromList(mapP, precisePositions);
					if (near != null && near != Vector3.Zero)
						result.Add(near);
					else
						result.Add(mapP);
				}
				return result;
			}
			public static List<Vector3> GetKrokuun()
			{
				if (TimerKrokuun.IsReady)
				{
					TimerKrokuun.Reset();
					var mapPositions = Questing.Map.GetLandMarks(MAP_KROKUUN, IconRift);
					CurrentKrokuun = CorrectMapPositions(mapPositions, KrokuunHotspots);
				}
				return CurrentKrokuun;
			}
			public static List<Vector3> GetKrokuunGreater()
			{
				if (TimerKrokuunGreater.IsReady)
				{
					TimerKrokuunGreater.Reset();
					var mapPositions = Questing.Map.GetLandMarks(MAP_KROKUUN, IconRiftGreater);
					CurrentKrokuunGreater = CorrectMapPositions(mapPositions, KrokuunHotspots);
				}
				return CurrentKrokuunGreater;
			}
			public static List<Vector3> GetAntoranWastes()
			{
				if (TimerAntoranWastes.IsReady)
				{
					TimerAntoranWastes.Reset();
					var mapPositions = Questing.Map.GetLandMarks(MAP_ANTORAN_WASTES, IconRift);
					CurrentAntoranWastes = CorrectMapPositions(mapPositions, AntoranWastesHotspots);
				}
				return CurrentAntoranWastes;
			}
			public static List<Vector3> GetAntoranWastesGreater()
			{
				if (TimerAntoranWastesGreater.IsReady)
				{
					TimerAntoranWastesGreater.Reset();
					var mapPositions = Questing.Map.GetLandMarks(MAP_ANTORAN_WASTES, IconRiftGreater);
					CurrentAntoranWastesGreater = CorrectMapPositions(mapPositions, AntoranWastesHotspots);
				}
				return CurrentAntoranWastesGreater;
			}
			public static List<Vector3> GetMacAree()
			{
				if (TimerMacAree.IsReady)
				{
					TimerMacAree.Reset();
					var mapPositions = Questing.Map.GetLandMarks(MAP_MACAREE, IconRift);
					CurrentMacAree = CorrectMapPositions(mapPositions, MacAreeHotspots);
				}
				return CurrentMacAree;
			}
			public static List<Vector3> GetMacAreeGreater()
			{
				if (TimerMacAreeGreater.IsReady)
				{
					TimerMacAreeGreater.Reset();
					var mapPositions = Questing.Map.GetLandMarks(MAP_MACAREE, IconRiftGreater);
					CurrentMacAreeGreater = CorrectMapPositions(mapPositions, MacAreeHotspots);
				}
				return CurrentMacAreeGreater;
			}
			public static List<int> Minor = new List<int>() { Cengar.Portal, Naigtal.Portal, Val.Portal, Sangua.Portal, Bonich.Portal, Aurinor.Portal, };
			public static List<int> Greater = new List<int>() { MistressAlluradel.Portal, MatronFolnuna.Portal, PitLordVilemus.Portal, InquisitorMeto.Portal, Occularus.Portal, Sotanathor.Portal };
			public static List<int> Out = new List<int>()
			{
				124884, //Cengar, Aurinor
			};
			public static List<Vector3> KrokuunHotspots = new List<Vector3>()
			{
				new Vector3(628.2518, 1222.403, 441.9168), //70,81
				new Vector3(1815.953, 1045.214, 501.4757), //75,34
				new Vector3(2016.802, 1520.774, 408.6734), //62,25 // greater
				new Vector3(736.7952, 2027.215, 390.4212), //48,77
			};
			public static List<Vector3> KrokuunMapCoords = new List<Vector3>()
			{
				new Vector3(0.7001, 0.8170, 0), //70,81
				new Vector3(0.9299, 0.6068, 0), //75,34
				new Vector3(0.8597, 0.5569, 0), //62,25 // greater
				new Vector3(0.7848, 0.8743, 0), //48,77
			};
			public static List<Vector3> AntoranWastesHotspots = new List<Vector3>()
			{
				new Vector3(-3357.363, 9082.359, -168.0572), //66, 70 //good
				new Vector3(-2692.599, 8985.086, -137.701), //greater
				new Vector3(-2544.95, 8999.505, -138.3018), //69, 33 //good
				new Vector3(2458.948, 9397.223, -128.9223), //57, 30
				new Vector3(-2763.939, 9230.536, -169.6993), //60, 43
			};
			public static List<Vector3> AntoranWastesMapCoords = new List<Vector3>()
			{
				new Vector3(0.6608, 0.6916, 0), //66, 70 ///good
				new Vector3(0.6894, 0.3983, 0), //bad?
				new Vector3(0.6851, 0.3332, 0), //69, 33 //good
				new Vector3(0.5682, 0.2953, 0), //57, 30 //good
				new Vector3(0.6025, 0.4298, 0), //60, 43 //good
			};
			public static List<Vector3> MacAreeHotspots = new List<Vector3>()
			{
				new Vector3(5786.783, 9248.674, -28.09992), //72,39
				new Vector3(6352.261, 10281.95, 38.57191), //40,13
				new Vector3(6215.544, 9557.909, -79.70746), //63,19
				new Vector3(5519.492, 10072.35, -85.66666), //47,51 //greater //good
			};
			public static List<Vector3> MacAreeMapCoords = new List<Vector3>()
			{
				new Vector3(0.7203, 0.3848, 0), //72,39 //good
				new Vector3(0.4032, 0.1246, 0), //40,13
				new Vector3(0.6252, 0.1877, 0), //63,19 //good
				new Vector3(0.4680, 0.5078, 0), //47,51 //greater //good
			};

		}
	}
	public static void ResetSettings()
	{
		Log("reset settings");
	}
}