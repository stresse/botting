namespace Stresse
{
	public static class Legion
	{
		public static class Zones
		{
			public const int Azsuna = 1015;
			public const int Valsharah = 1018;
			public const int Highmountain = 1024;
			public const int Stormheim = 1017;
			public const int Suramar = 1033;
			public const int Dalaran = 1014;
			public const int BrokenShore = 1021;
			public const int EyeOfAzsharah = 1096;			
		}
		public static class Azsuna
		{
			public static string Name = "Azsuna";
			public static int ZoneId = Stresse.Legion.Zones.Azsuna;
			public static class Fishing
			{
				public static class Locations
				{
					public const Vector3 Default = new Vector3(597.1454, 5660.678, 81.93417, "None");
					public const Vector3 GhostlyQueenfish = new Vector3(200.7957, 6218.867, -0.8501156, "None");
					// My Rotation = 6.003941
				}
				public static class AllFish //from Fish
				{
					/*		Add Other Fish Here!!!		*/
					public const Fish LeyshimmerBlenny = Stresse.Fish.LeyshimmerBlenny;
					public const Fish NarthalasHermit = Stresse.Fish.NarthalasHermit;
					public const Fish GhostlyQueenfish = Stresse.Fish.GhostlyQueenfish;
				}
				public static List<Fish> SpecialFish = Stresse.Fish.GetType().GetFields().Where( f => f.IsSpecial && (f.ZoneName == "Azsuna"));
/* 				public static class SpecialFish
				{
					public const Fish LeyshimmerBlenny = Stresse.Fish.LeyshimmerBlenny;
					public const Fish NarthalasHermit = Stresse.Fish.NarthalasHermit;
					public const Fish GhostlyQueenfish = Stresse.Fish.GhostlyQueenfish;
				} */
				public static class Pools
				{
					//public const GhostlyQueenfish
				}
				public static class PoolIDs
				{

				}
			}
			//	new Vector3(3384.844, 5817.442, 309.0113, "None")
		}
		public static class Dalaran
		{
			public static class Locations
			{

			}
		}

		public static class Valsharah
		{
			public static class Fishing
			{
				public static class Locations
				{
					public const Vector3 Default = new Vector3(3384.844, 5817.442, 309.0113, "None");
					// My Rotation = 6.003941
				}
				public static class AllFish
				{
					/*		Add Other Fish Here!!!		*/
					public const Fish Terrorfin = Stresse.Fish.Terrorfin;
					public const Fish ThornedFlounder = Stresse.Fish.ThornedFlounder;
					public const Fish GhostlyQueenfish = Stresse.Fish.GhostlyQueenfish;
				}
				public static class SpecialFish
				{
					public const Fish Terrorfin = Stresse.Fish.Terrorfin;
					public const Fish ThornedFlounder = Stresse.Fish.ThornedFlounder;
					public const Fish AncientMossgill = Stresse.Fish.AncientMossgill;
				}
				public static class Pools
				{
					//public const GhostlyQueenfish
					public const List<int> PoolIDs = new List<int>() {};

				}
				public static class PoolIDs
				{

				}
			}
			//new Vector3(3384.844, 5817.442, 309.0113, "None")
		}
		public static class Highmountain
		{
			public static class Fishing
			{
				public static class Locations
				{
					public const Vector3 Default = new Vector3(4431.042, 4743.378, 651.1548, "None");
					//	My rotation: 4.406743
				}
				public static class AllFish
				{
					public const Fish MountainPuffer = Stresse.Fish.MountainPuffer;
					public const Fish ColdriverCarp = Stresse.Fish.ColdriverCarp;
					public const Fish AncientHighmountainSalmon = Stresse.Fish.AncientHighmountainSalmon;
				}
				public static class SpecialFish
				{
					public const Fish MountainPuffer = Stresse.Fish.MountainPuffer;
					public const Fish ColdriverCarp = Stresse.Fish.ColdriverCarp;
					public const Fish AncientHighmountainSalmon = Stresse.Fish.AncientHighmountainSalmon;
				}
				public static class Pools
				{

				}
				public static class PoolIDs
				{
					
				}
			}
		}
		public static class Stormheim
		{
			public static class Fishing
			{
				public static class Locations
				{
					//	Working (this position will let bot fish from Oodelfijiskenpool as soon as you use the ring)
					public const Vector3 Default = new Vector3(2471.167, 1790.379, 0.08700086, "None");
					//	My Rotation:  5.31
				}
				public static class AllFish
				{
					public const Fish Oodelfjisk = Stresse.Fish.Oodelfjisk;
					public const Fish GraybellyLobster = Stresse.Fish.GraybellyLobster;
					public const Fish ThunderingStormray = Stresse.Fish.ThunderingStormray;

				}
				public static class SpecialFish
				{
					public const Fish Oodelfjisk = Stresse.Fish.Oodelfjisk;
					public const Fish GraybellyLobster = Stresse.Fish.GraybellyLobster;
					public const Fish ThunderingStormray = Stresse.Fish.ThunderingStormray;
				}
				public static class Pools
				{

				}
				public static class PoolIDs
				{

				}
			}
		}
		public static class Suramar
		{
			public static class Fishing
			{
				public static class Locations
				{
					public const Vector3 Default = new Vector3(2306.726, 5287.468, 97.50949, "None");
					//	My rotation: 4.406743
				}
				public static class AllFish
				{

				}
				public static class SpecialFish
				{

				}
				public static class Pools
				{

				}
				public static class PoolIDs
				{
					
				}
			}
		}
		public static class OpenOcean
		{
			public static class Fishing
			{
				public static class Locations
				{
					//	Suramar
					public const Vector3 Default = new Vector3(1598.849, 2538.656, -0.2989297, "None");
					//	My Rotation:  2.873638
				}
				public static class AllFish
				{

				}
				public static class SpecialFish
				{

				}
				public static class Pools
				{

				}
				public static class PoolIDs
				{

				}
			}
		}
	public class Questing
	{
		public class UnderlightAngler
		{
			public class Locations
			{
				public const Vector3 ArchmageKhadgar = new Vector3(-848.4618, 4638.848, 749.5473, "None");
				public const Vector3 MarciaChase = new Vector3(-943.2778, 4438.424, 734.1776, "None");		//	95844
			}
			public class Paths
			{
				public const List<Vector3> AroundDalaran = new List<Vector3> { new Vector3(-930.0134, 4459.739, 733.9698), new Vector3(-927.0876, 4463.095, 733.9388), new Vector3(-922.4046, 4468.466, 734.0826), new Vector3(-917.7997, 4473.748, 734.0249), new Vector3(-913.356, 4478.844, 733.6286), new Vector3(-910.8994, 4481.662, 733.1843), new Vector3(-906.4786, 4486.732, 732.149), new Vector3(-904.1233, 4489.434, 731.7917), new Vector3(-901.8186, 4492.077, 731.3967), new Vector3(-899.4495, 4494.794, 731.074), new Vector3(-894.9688, 4499.933, 730.3817), new Vector3(-890.2307, 4505.368, 730.2027), new Vector3(-885.658, 4510.612, 730.0147), new Vector3(-881.3568, 4515.545, 730.0969), new Vector3(-878.8819, 4518.384, 730.0809), new Vector3(-876.5082, 4521.106, 729.909), new Vector3(-871.8895, 4526.403, 729.6229), new Vector3(-867.4365, 4531.511, 729.4086), new Vector3(-863.5723, 4535.943, 729.0049), new Vector3(-861.1618, 4538.708, 728.7231), new Vector3(-856.5356, 4543.737, 728.3599), new Vector3(-850.1238, 4545.809, 728.1875), new Vector3(-842.7483, 4545.971, 728.3684), new Vector3(-836.4686, 4543.872, 728.6958), new Vector3(-833.4271, 4541.651, 728.8474), new Vector3(-827.8132, 4537.553, 728.8458), new Vector3(-824.9639, 4535.472, 728.8692), new Vector3(-819.6044, 4531.559, 729.2608), new Vector3(-816.5743, 4529.183, 729.2846), new Vector3(-813.1789, 4525.876, 729.5295), new Vector3(-808.5497, 4521.37, 729.9347), new Vector3(-805.9768, 4518.864, 730.1287), new Vector3(-803.449, 4516.403, 730.6533), new Vector3(-798.3935, 4511.481, 730.7646), new Vector3(-793.8846, 4507.091, 730.9837), new Vector3(-789.0146, 4502.349, 731.4576), new Vector3(-783.8523, 4497.954, 731.5946), new Vector3(-780.8565, 4495.777, 731.6287), new Vector3(-775.7305, 4491.024, 731.8276), new Vector3(-773.5432, 4484.387, 732.6451), new Vector3(-773.9371, 4477.569, 733.0591), new Vector3(-775.3341, 4472.494, 733.5662), new Vector3(-777.0655, 4466.204, 734.459), new Vector3(-780.0792, 4460.711, 735.1342), new Vector3(-782.5369, 4457.648, 735.2499), new Vector3(-784.8851, 4454.722, 735.7551), new Vector3(-787.058, 4452.014, 736.2128), new Vector3(-790.8914, 4447.237, 737.0602), new Vector3(-793.4849, 4444.005, 737.5466), new Vector3(-795.7761, 4441.149, 737.8769), new Vector3(-800.0476, 4435.826, 738.3323), new Vector3(-802.2512, 4433.08, 738.3992), new Vector3(-804.468, 4430.317, 738.4216), new Vector3(-808.6737, 4425.076, 738.3269), new Vector3(-810.766, 4422.271, 738.1954), new Vector3(-814.2061, 4415.967, 737.9835), new Vector3(-817.5021, 4409.926, 737.9725), new Vector3(-821.6838, 4404.167, 737.808), new Vector3(-826.0494, 4399.78, 737.55), new Vector3(-829.3239, 4396.974, 737.5806), new Vector3(-834.5067, 4392.534, 737.6711), new Vector3(-837.1699, 4390.252, 737.7199), new Vector3(-840.2452, 4388.553, 737.7888), new Vector3(-845.4757, 4390.732, 737.7047), new Vector3(-848.5967, 4392.535, 737.6567), new Vector3(-853.5965, 4397.159, 737.5269), new Vector3(-856.1848, 4399.566, 737.5464), new Vector3(-858.8961, 4402.088, 737.5596), new Vector3(-864.4147, 4406.049, 737.4094), new Vector3(-867.4315, 4408.217, 737.4413), new Vector3(-872.8275, 4412.222, 737.5274), new Vector3(-875.6941, 4414.35, 737.5164), new Vector3(-878.6147, 4416.544, 737.5149), new Vector3(-881.1903, 4418.986, 737.526), new Vector3(-886.1028, 4423.643, 737.267), new Vector3(-888.7344, 4426.138, 737.1045), new Vector3(-894.0581, 4430.852, 736.6105), new Vector3(-899.5716, 4435.108, 736.259), new Vector3(-904.9632, 4439.27, 736.0974), new Vector3(-907.8557, 4441.503, 735.8643), new Vector3(-910.6737, 4443.746, 735.5911), new Vector3(-914.4871, 4449.227, 735.2423) };
			}
			public class NPCIDs
			{
				//	Nat Pagle (	Entry: 102639	)
				//	Location is anywhere he spawns in the AroundDalaran loop (maybe just run loop one ance check
				//	"I am ready. Take me to this fish frezy of yours"
				int UnderlightSupplicant = 102507;
				int UnderLightSentry = 102508;
				int UnderlightCrusader = 102509;
				int UnderlightSorceror = 102510;
            }
		}
	}	
	public class SchoolIDs
	{
		public const int GhostlyQueenfish = 246553;
		public const int CursedQueenfish = 246488;
		public const int MossgillPerch = 246489;
		public const int HighmountainSalmon = 246490;
		//	Preflag Pre-flag
		public const int FeverofStormrays = 246491;
		public const int FeverOfStormrays = FeverofStormrays;
		public const int Oodelfjisk = 246554;
		public const int BlackBarracuda = 246493;
		public const int RunescaleKoi = 246492;
		public const List<int> FrenziedFish = new List<int>() {246676, 246677, 246678, 250679, 250680, 250681};
/*		public const int FISH = ID;
		public const int FISH = ID;
		public const int FISH = ID;
		public const int FISH = ID;
*/	}
	}
	public class NPCLists
	{
		public const List<int> UnderlightMurlocs = new List<int>() { NPCIDs.UnderlightSupplicant, NPCIDs.UnderLightSentry, NPCIDs.UnderlightSorceror, NPCIDs.UnderlightCrusader};
	}
	public class NPCIDs
	{
		//Nat Pagle (Entry: 102639 )
		//Location is anywhere he spawns in the AroundDalaran loop (maybe just run loop one ance check
		//"I am ready. Take me to this fish frezy of yours"
		public const int UnderlightSupplicant = 102507;
		public const int UnderLightSentry = 102508;
		public const int UnderlightCrusader = 102509;
		public const int UnderlightSorceror = 102510;
	}

	//		Fish FISH = new Fish(name: "NAME", id: ITEMID, zonename: "ZONENAME", continentid: CONTINENTID, zoneid: ZONEID, hasspecialpools: HASSPEACIALPOOLS, israre: ISRARE, isap: ISAP, isskillup: ISSKILLUP, givesbuff: GIVESBUFF, givesbuffname: "GIVESBUFFNAME", requiresbuff: REQUIRESBUFF, requiresbuffname: "REQUIRESBUFFNAME", buffitemid: BUFFITEMID, buffitemname: "BUFFITEMNAME", skillupid: SKILLUPID, apid: APID, biggerfishtofrycriteria: BIGGERFISHTOFRYCRITERIA, poolcoords: new List<Vector3>() { VECTORS });
	public class Fish
	{
		public static Fish LeyshimmerBlenny = new Fish(name: "Leyshimmer Blenny", id: 139652, zonename: "Azsuna", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Azsuna, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201805, requiresbuffname: "Aromatic Murloc Slime", buffitemid: 133702, buffitemname: "Aromatic Murloc Slime", skillupid: 133725, apid: 139652, biggerfishtofrycriteria: 8, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish NarthalasHermit = new Fish(name: "Nar'Thalas Hermit", id: 139653, zonename: "Azsuna", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Azsuna, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201806, requiresbuffname: "Pearlescent Conch", buffitemid: 133703, buffitemname: "Pearlescent Conch", skillupid: 133726, apid: 139653, biggerfishtofrycriteria: 11, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish GhostlyQueenfish = new Fish(name: "Ghostly Queenfish", id: 139654, zonename: "Azsuna", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Azsuna, hasspecialpools: true, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201807, requiresbuffname: "Rusty Queenfish Brooch", buffitemid: 133704, buffitemname: "Rusty Queenfish Brooch", skillupid: 133727, apid: 139654, biggerfishtofrycriteria: 6, poolcoords: new List<Vector3>() { VECTORS });
			
		//	Val'sharah
		public static Fish Terrorfin = new Fish(name: "Terrorfin", id: 139655, zonename: "Val'sharah", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Valsharah, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201810, requiresbuffname: "Nightmare Nightcrawler", buffitemid: 133707, buffitemname: "Nightmare Nightcrawler", skillupid: 133728, apid: 139655, biggerfishtofrycriteria: 17, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish ThornedFlounder = new Fish(name: "Thorned Flounder", id: 139656, zonename: "Val'sharah", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Valsharah, hasspecialpools: false, israre: true, isap: false, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 202067, requiresbuffname: "Blessing of the Thistleleaf", buffitemid: 133708, buffitemname: "Drowned Thistleleaf", skillupid: 133729, apid: 139656, biggerfishtofrycriteria: 18, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish AncientMossgill = new Fish(name: "Ancient Mossgill", id: 139657, zonename: "Val'sharah", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Valsharah, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201809, requiresbuffname: "The Cat's Meow", buffitemid: 133705, buffitemname: "Rotten Fishbone", skillupid: 133730, apid: 139657, biggerfishtofrycriteria: 3, poolcoords: new List<Vector3>() { VECTORS });
	
		//	Highmountain
		public static Fish MountainPuffer = new Fish(name: "Mountain Puffer", id: 139658, zonename: "Highmountain", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Highmountain, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 202056, requiresbuffname: "Blessing of the Murlocs", buffitemid: 133711, buffitemname: "Swollen Murloc Egg", skillupid: 133731, apid: 139658, biggerfishtofrycriteria: 10, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish ColdriverCarp = new Fish(name: "Coldriver Carp", id: 139659, zonename: "Highmountain", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Highmountain, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201815, requiresbuffname: "Frost Worm", buffitemid: 133712, buffitemname: "Frost Worm", skillupid: 133732, apid: 139659, biggerfishtofrycriteria: 5, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish AncientHighmountainSalmon = new Fish(name: "Ancient Highmountain Salmon", id: 139660, zonename: "Highmountain", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Highmountain, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201813, requiresbuffname: "Salmon Lure", buffitemid: 133710, buffitemname: "Salmon Lure", skillupid: 133733, apid: 139660, biggerfishtofrycriteria: 2, poolcoords: new List<Vector3>() { VECTORS });
	
		// Stormheim
		public static Fish Oodelfjisk = new Fish(name: "Oodelfjisk", id: 139661, zonename: "Stormheim", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Stormheim, hasspecialpools: true, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201818, requiresbuffname: "Ancient Vrykul Ring", buffitemid: 133715, buffitemname: "Ancient Vrykul Ring", skillupid: 133734, apid: 139661, biggerfishtofrycriteria: 12, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish GraybellyLobster = new Fish(name: "Graybelly Lobster", id: 139662, zonename: "Stormheim", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Stormheim, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201819, requiresbuffname: "Soggy Drakescale", buffitemid: 133716, buffitemname: "Soggy Drakescale", skillupid: 133735, apid: 139662, biggerfishtofrycriteria: 7, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish ThunderingStormray = new Fish(name: "Thundering Stormray", id: 139663, zonename: "Stormheim", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Stormheim, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201817, requiresbuffname: "Silverscale Minnow", buffitemid: 133714, buffitemname: "Silverscale Minnow", skillupid: 133736, apid: 139663, biggerfishtofrycriteria: 15, poolcoords: new List<Vector3>() { VECTORS });
	
		//	Suramar
		public static Fish MagicEaterFrog = new Fish(name: "Magic-Eater Frog", id: 139664, zonename: "Suramar", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Suramar, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201820, requiresbuffname: "Enchanted Lure", buffitemid: 133717, buffitemname: "Enchanted Lure", skillupid: 133737, apid: 139664, biggerfishtofrycriteria: 9, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish SeerspinePuffer = new Fish(name: "Seerspine Puffer", id: 139665, zonename: "Suramar", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Suramar, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 0, requiresbuffname: "IsSpecial", buffitemid: 133719, buffitemname: "Sleeping Murloc", skillupid: 133738, apid: 139665, biggerfishtofrycriteria: 16, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish TaintedRunescaleKoi = new Fish(name: "Tainted Runescale Koi", id: 139666, zonename: "Suramar", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Suramar, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201822, requiresbuffname: "Demonic Detritus", buffitemid: 133720, buffitemname: "Demonic Detritus", skillupid: 133739, apid: 139666, biggerfishtofrycriteria: 14, poolcoords: new List<Vector3>() { VECTORS });
	
		//	Ocean
		public static Fish Axefish = new Fish(name: "Axefish", id: 139667, zonename: "Suramar Open Ocean", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Suramar, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 201823, requiresbuffname: "Axefish Lure", buffitemid: 133722, buffitemname: "Axefish Lure", skillupid: 133740, apid: 139667, biggerfishtofrycriteria: 4, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish SeabottomSquid = new Fish(name: "Seabottom Squid", id: 139668, zonename: "Suramar Open Ocean", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Suramar, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 0, requiresbuffname: "IsSpecial", buffitemid: 133723, buffitemname: "Stunned, Angry Shark", skillupid: 133741, apid: 139668, biggerfishtofrycriteria: 13, poolcoords: new List<Vector3>() { VECTORS });
		public static Fish AncientBlackBarracuda = new Fish(name: "Ancient Black Barracuda", id: 139669, zonename: "Suramar Open Ocean", continentid: (int)ContinentId.Troll_Raid, zoneid: Locations.Legion.Suramar, hasspecialpools: false, israre: true, isap: true, isskillup: false, givesbuff: 0, givesbuffname: "", requiresbuff: 202131, requiresbuffname: "Ravenous Flyfishing", buffitemid: 133795, buffitemname: "Ravenous Fly", skillupid: 133742, apid: 139669, biggerfishtofrycriteria: 1, poolcoords: new List<Vector3>() { VECTORS });
		public static List<Fish> Special = typeof(Stresse.Fish).GetFields().Where(x => x.IsSpecial);
	}
}


public struct Pool
{
	//	public 
}

public struct Coords
{
	public int x,y,z;
	public string t;
	public Coords(float p1, float p2, float p3, string typ = "None")
	{
		x = p1;
		y = p2;
		z = p3;
		t = typ;
	}
}
//namespace
public static class StringExtension
{

}
public struct Fish
{
	public string Name;
	public int Id;
	public string ZoneName;
	public int ContinentId;
	public bool ZoneId;
	public bool IsRare;
	public bool IsAP;
	public bool IsSkillUp;
	public int GivesBuff;
	public string GivesBuffName;
	public int RequiresBuff;
	public string RequiresBuffName;
	public int SkillUpId;
	public int APId;
	public int BiggerFishToFryCriteria;
	public List<Vector3> PoolCoords;
	public Fish(string name, int id, string zonename, int continentid, int zoneid, bool hasspecialpools = false, bool israre = false, bool isap = false, bool isskillup = false, int givesbuff = 0, string givesbuffname = "", int requiresbuff = 0, string requiresbuffname = "", int buffitemid = 0, string buffitemname = "", int skillupid = 0, int apid = 0, int biggerfishtofrycriteria, List<Vector3> poolcoords = new List<Vector3>() {})
	{
		Name = name;
		Id = id;
		ZoneName = zonename;
		ContinentId = continentid;
		ZoneId = zoneid;
		HasSpecialPools = hasspecialpools;
		IsRare = israre;
		IsAP = isap;
		IsSkillUp = isskillup;
		GivesBuff = givesbuff;
		GivesBuffName = givesbuffname;
		RequiresBuff = requiresbuff;
		RequiresBuffName = requiresbuffname;
		BuffItemId = buffitemid;
		BuffItemName = buffitemname;
		SkillUpId = skillupid;
		APId = apid;
		BiggerFishToFryCriteria = biggerfishtofrycriteria;
		PoolCoords = poolcoords;
	}
}
public struct NPC
{
    private string name;
    public int Entry, ID, Id, LocationsCount ;
	public List<int> QuestsGiven, QuestsAccepted ;
	public Dictionary<int, Vector3> Locations = new Dictionary<int, Vector3> () {} ;
	public bool IsGameObject, Roams ;

    public string Name { get => name; set => name = value; }

    public NPC (string name, int id, List<int> questsgiven, List<int> questsaccepted, Dictionary<int, Vector3> locations, bool isgameobject = false, bool roams = false)
	{
		Name = name ;
		Id = ID = Entry = id ;
		Locations = locations ;
		LocationsCount = locations.Count ;
		QuestsGiven = questsgiven ;
		QuestsAccepted = questsaccepted ;
		IsGameObject = isgameobject ;
		Roams = roams ;
	}
}

//	TEMPLATE
//	Fish(name: NAME, id: ID, zone: ZONE, israre: ISRARE, isap: ISAP, isskillup: ISSKILLUP, givesbuff = GIVESBUFF, requiresbuff = REQUIRESBUFF, poolcoords = POOLCOORDINATES)
