using robotManager.Helpful;
using System.Threading;
using robotManager.Products;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System;
using System.Collections.Generic;
using System.IO;

public class Main : wManager.Plugin.IPlugin
{
    private bool _isLaunched;
    bool goToTown;

    public void Initialize()
    {
        Logging.Write("[Template Project Name] Started.");
        _isLaunched = true;
        goToTown = false;

        initializeToTownWatcher();
        watchForEvents();

        pluginLoop();

    }

    public void Dispose()
    {

        _isLaunched = false;
        Logging.Write("[Template Project Name] Disposed.");
    }
    public void Settings()
    {
        _settings.ToForm();
        _settings.Save();
    }


    public void pluginLoop()
    {

        while (Products.IsStarted && _isLaunched)
        {

            if (!Products.InPause)
            {
                Logging.Write(_settings.testSettingValue);
                Thread.Sleep(3000);
            }
        }

    }

    private void watchForEvents()
    {

        EventsLuaWithArgs.OnEventsLuaWithArgs += (LuaEventsId id, List<string> args) =>
        {


            if (id == wManager.Wow.Enums.LuaEventsId.PARTY_MEMBERS_CHANGED)
            {
                Logging.Write("Joined Group or something about the current group was changed.");
            }
            if (id == wManager.Wow.Enums.LuaEventsId.CHAT_MSG_SYSTEM)
            {
                  Logging.Write("We Received a System Message.");
            }
            

        };

    }

	private void initializeToTownWatcher() {
        robotManager.Events.FiniteStateMachineEvents.OnRunState += (engine, state, cancelable) =>
        {
            if (state != null && state.DisplayName == "To Town")
            {
             
                    goToTown = true;
                    Logging.Write("Going to Town State has been initiated.");
            }
        };

        robotManager.Events.FiniteStateMachineEvents.OnAfterRunState += (engine, state) =>
        {
            if (state != null && state.DisplayName == "To Town")
            {
                goToTown = false;
                Logging.Write("We have completed going To Town State.");
                

            }
        };

    }

    private pluginSettings _settings
    {
        get
        {
            try
            {
                if (pluginSettings.CurrentSetting == null)
                    pluginSettings.Load();
                return pluginSettings.CurrentSetting;
            }
            catch
            {
            }
            return new pluginSettings();
        }
        set
        {
            try
            {
                pluginSettings.CurrentSetting = value;
            }
            catch
            {
            }
        }
    }

    [Serializable]
    public class pluginSettings : Settings
    {
        public pluginSettings()
        {
          
            testSettingValue = "Hello World!";
        }

        [Setting]
        [Category("General Settings")]
        [DisplayName("Test Setting Value")]
        [Description("This is a Description.")]
        public string testSettingValue { get; set; }

        [Setting]
        [Category("Other Settings")]
        [DisplayName("Another Setting Value")]
        [Description("This is another Description.")]
        public int anothertestSettingValue { get; set; }

        public static pluginSettings CurrentSetting { get; set; }

        public bool Save()
        {
            try
            {
                return Save(AdviserFilePathAndName("TemplateProjectName", ObjectManager.Me.Name + "." + Usefuls.RealmName));
            }
            catch (Exception e)
            {
                Logging.WriteError("TemplateProjectName > Save(): " + e);
                return false;
            }
        }

        public static bool Load()
        {
            try
            {
                if (File.Exists(AdviserFilePathAndName("TemplateProjectName", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
                {
                    CurrentSetting =
                        Load<pluginSettings>(AdviserFilePathAndName("TemplateProjectName",
                                                                      ObjectManager.Me.Name + "." + Usefuls.RealmName));
                    return true;
                }
                CurrentSetting = new pluginSettings();
            }
            catch (Exception e)
            {
                Logging.WriteError("TemplateProjectName > Load(): " + e);
            }
            return false;
        }
    }

}