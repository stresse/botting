using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using robotManager.FiniteStateMachine;
using robotManager.Helpful;
using wManager.Wow.Bot.Tasks;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using Timer = robotManager.Helpful.Timer;

namespace wManager.Wow.Bot.States
{
    public class ToTown : State
    {
        public ToTown()
        {
            ForceToTown = false;
            if (wManagerSetting.CurrentSetting.ToTownTimerActivated)
            {
                _forceToTownTimer = new Timer(wManagerSetting.CurrentSetting.ToTownTimer * 60 * 1000);
            }
        }

        public override string DisplayName
        {
            get { return "To Town"; }
        }

        public bool PetBattleMode;
        public static bool ForceToTown;
        public static bool GoToTownInProgress;

        private readonly Timer _forceToTownTimer;
        private Timer _toTownNeedToRunCheckTimer = new Timer(-1);

        private bool _needDrink;
        private bool _needFood;
        private bool _needVendor;
        private bool _needRepair;
        private bool _needSendMail;

        public override bool NeedToRun()
        {
            if (!_toTownNeedToRunCheckTimer.IsReady)
                return false;
            _toTownNeedToRunCheckTimer = new Timer(1000 * 5);
            if (!Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause ||
                Conditions.IsAttackedAndCannotIgnore)
                return false;
            if (wManagerSetting.CurrentSetting.UseMammoth && CharacterHaveMammoth() && !CanUseMammoth())
                return false;
            if (wManagerSetting.CurrentSetting.ToTownTimerActivated && _forceToTownTimer != null && _forceToTownTimer.IsReady)
            {
                Logging.WriteDebug("[ToTown] Timer to Vendor/Repair/Mailbox ready");
                ForceToTown = true;
                _forceToTownTimer.Reset();
            }
            var reason = string.Empty;
            if (ForceToTown)
                reason += ", force go to town enabled";
            if (SendMailNeedAndCanRun(wManagerSetting.CurrentSetting.MinFreeBagSlotsToGoToTown).IsValid())
                reason += ", send items per mail";
            if (RepairNeedAndCanRun(30).IsValid())
                reason += ", repair";
            if (ResurrectBattlePetsNeedAndCanRun().IsValid())
                reason += ", resurrect battle pets";
            if (SellNeedAndCanRun(wManagerSetting.CurrentSetting.MinFreeBagSlotsToGoToTown).IsValid())
                reason += ", sell items";
            if (NeedToBuy(true).Count > 0)
                reason += ", need to buy items";
            if (!string.IsNullOrWhiteSpace(reason))
            {
                Logging.Write("[ToTown] Go to town: " + reason.TrimStart(',', ' ')); 
                return true;
            }
            return false;
        }

        public override void Run()
        {
            GoToTownInProgress = true;
            try
            {
                Pulse();
            }
            catch (Exception e)
            {
                Logging.WriteError("ToTown : State > Run(): " + e);
            }
            GoToTownInProgress = false;
        }

        void Pulse()
        {
            var listVendor = new List<Npc>();
            // MailBox
            var mailBox = SendMailNeedAndCanRun(200);

            // If need repair
            if (RepairNeedAndCanRun(99).IsValid())
                listVendor.Add(RepairNeedAndCanRun(99));

            // If need buy food drink
            if (BuyFoodDrinkNeedAndCanRun().IsValid())
                listVendor.Add(BuyFoodDrinkNeedAndCanRun());

            // need buy from list:
            listVendor.AddRange(NeedToBuy());

            // If no repair/vendor and need sell
            if (listVendor.Count == 0 && SellNeedAndCanRun(200).IsValid())
                listVendor.Add(SellNeedAndCanRun(200));

            // Ressurect pet:
            if (ResurrectBattlePetsNeedAndCanRun().IsValid())
                listVendor.Add(ResurrectBattlePetsNeedAndCanRun());

            #region Vendor

            if (listVendor.Count > 0)
            {
                foreach (var vendor in listVendor)
                {
                    if (!vendor.IsValid() ||
                        Conditions.IsAttackedAndCannotIgnore ||
                        !Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause)
                        continue;

                    Logging.Write("[ToTown] Go to vendor " + vendor.Name + " (" + vendor.Type + ")");

                    if ((vendor.Type == Npc.NpcType.Repair || vendor.Type == Npc.NpcType.Vendor) && CanUseMammoth())
                    {
                        Logging.Write("[ToTown] Use Mammoth");
                        if (!_travelersTundraMammoth.HaveBuff)
                        {
                            MountTask.DismountMount();
                            MovementManager.StopMove();
                            Thread.Sleep(1000 + Usefuls.Latency);
                            if (Conditions.IsAttackedAndCannotIgnore || ObjectManager.ObjectManager.Me.IsMounted)
                            {
                                Logging.WriteDebug("[ToTown] Use Mammoth > In combat or IsMounted");
                                return;
                            }
                            Thread.Sleep(2000 + Usefuls.Latency);
                            _travelersTundraMammoth.Launch(true);
                            Thread.Sleep(3000 + Usefuls.Latency);
                        }
                    }

                    GoToTask.ToPosition(vendor.Position, 10, wManagerSetting.CurrentSetting.BlackListIfNotCompletePath);

                    if (ObjectManager.ObjectManager.Me.IsFlying)
                        MountTask.Land();

                    MillingAndProspecting();

                    if (GoToTask.ToPositionAndIntecractWith(vendor, wManagerSetting.CurrentSetting.BlackListIfNotCompletePath) &&
                        ObjectManager.ObjectManager.Target.IsValid &&
                        ObjectManager.ObjectManager.Target.Entry == vendor.Entry &&
                        ObjectManager.ObjectManager.Target.IsGoodInteractDistance &&
                        Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause &&
                        !Conditions.IsAttackedAndCannotIgnore)
                    {
                        if (ObjectManager.ObjectManager.Me.IsFlying)
                            MountTask.Land();

                        var vendorObj = new WoWUnit(ObjectManager.ObjectManager.Target.GetBaseAddress);

                        Logging.Write("[ToTown] Vendor found " + vendorObj.Name);

                        // Repair:
                        if (wManagerSetting.CurrentSetting.Repair && vendor.Type == Npc.NpcType.Repair)
                        {
                            Logging.Write("[ToTown] Repair items");
                            Vendor.RepairAllItems();
                            Thread.Sleep(1000);
                        }

                        if (wManagerSetting.CurrentSetting.Selling &&
                            (vendor.Type == Npc.NpcType.Repair || vendor.Type == Npc.NpcType.Vendor))
                        {
                            // Sell:
                            var vQuality = new List<WoWItemQuality>();
                            if (wManagerSetting.CurrentSetting.SellGray)
                                vQuality.Add(WoWItemQuality.Poor);
                            if (wManagerSetting.CurrentSetting.SellWhite)
                                vQuality.Add(WoWItemQuality.Common);
                            if (wManagerSetting.CurrentSetting.SellGreen)
                                vQuality.Add(WoWItemQuality.Uncommon);
                            if (wManagerSetting.CurrentSetting.SellBlue)
                                vQuality.Add(WoWItemQuality.Rare);
                            if (wManagerSetting.CurrentSetting.SellPurple)
                                vQuality.Add(WoWItemQuality.Epic);

                            int bagFreeSlots = -1;
                            for (int i = 0; i < 12 && bagFreeSlots != Bag.GetContainerNumFreeSlots; i++)
                            {
                                bagFreeSlots = Bag.GetContainerNumFreeSlots;
                                Logging.Write("[ToTown] Sell items (try " + (i + 1) + ") (Free slots: " + bagFreeSlots + ")");
                                Thread.Sleep(Usefuls.Latency + 300);
                                Vendor.SellItems(wManagerSetting.CurrentSetting.ForceSellList, wManagerSetting.CurrentSetting.DoNotSellList, vQuality);
                                Thread.Sleep(3000);
                            }
                        }

                        // Buy:
                        if (vendor.Type == Npc.NpcType.Vendor)
                        {
                            if (vendor.VendorItemClass == Npc.NpcVendorItemClass.Food && (NeededBuyFood() || NeededBuyDrink()))
                            {
                                Logging.Write("[ToTown] Buy drink and food");
                                for (int i = 0; i < wManagerSetting.CurrentSetting.FoodAmount + 1 && NeededBuyFood(); i++)
                                {
                                    Vendor.BuyItem(wManagerSetting.CurrentSetting.FoodName, 1);
                                    Thread.Sleep(Usefuls.Latency + 400);
                                }
                                for (int i = 0; i < wManagerSetting.CurrentSetting.DrinkAmount + 1 && NeededBuyDrink(); i++)
                                {
                                    Vendor.BuyItem(wManagerSetting.CurrentSetting.DrinkName, 1);
                                    Thread.Sleep(Usefuls.Latency + 400);
                                }
                            }

                            foreach (var b in wManagerSetting.CurrentSetting.BuyList)
                            {
                                if (!b.IsValid())
                                    continue;
                                if (b.VendorItemClass != vendor.VendorItemClass)
                                    continue;
                                if (!b.NeedToBuy())
                                    continue;

                                Logging.Write($"[ToTown] Buy {b.ItemName} X{b.ItemName}");

                                for (int i = 0; i < b.Quantity && b.NeedToBuy(); i++)
                                {
                                    Vendor.BuyItem(b.ItemName, 1);
                                    Thread.Sleep(Usefuls.Latency + 400);
                                }
                            }
                        }

                        //  Resurrect Battle Pets:
                        if (vendor.Type == Npc.NpcType.ResurrectBattlePets)
                        {
                            Lua.LuaDoString("GossipTitleButton3:Click()");
                            Lua.LuaDoString("StaticPopup1Button1:Click()");
                        }


                        Lua.LuaDoString("CloseMerchant()");
                        Lua.LuaDoString("GossipFrameCloseButton:Click()");
                    }
                    else
                    {
                        if (Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause &&
                        !Conditions.IsAttackedAndCannotIgnore)
                        {
                            vendor.BlackList(120 * 60 * 1000);
                            Logging.Write("[ToTown] Unable to reach the vendor, blacklist it 120 minutes (you can disable this NPC in NPC DB tab 'Tools').");
                        }
                    }
                }
            }

            #endregion Vendor

            #region Mail

            if (mailBox.IsValid() &&
                !Conditions.IsAttackedAndCannotIgnore &&
                Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause)
            {
                Logging.Write("[ToTown] Go to MailBox (" + mailBox + ")");
                if (CanUseMollE())
                {
                    MountTask.DismountMount();
                    if (Conditions.IsAttackedAndCannotIgnore)
                    {
                        Logging.WriteDebug("[ToTown] MollE > In combat");
                        return;
                    }
                    Thread.Sleep(2000 + Usefuls.Latency);
                    ItemsManager.UseItem(40768);
                    Usefuls.WaitIsCasting();
                    Thread.Sleep(1500 + Usefuls.Latency);
                    mailBox.Position = ObjectManager.ObjectManager.Me.Position;
                    mailBox.CanFlyTo = false;
                }



                GoToTask.ToPosition(mailBox.Position, 10, wManagerSetting.CurrentSetting.BlackListIfNotCompletePath);

                MillingAndProspecting();

                if (GoToTask.ToPositionAndIntecractWith(mailBox, wManagerSetting.CurrentSetting.BlackListIfNotCompletePath))
                {
                    Logging.Write("[ToTown] MailBox found");

                    if (!Conditions.IsAttackedAndCannotIgnore &&
                        Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause)
                    {
                        Thread.Sleep(200);

                        var mQuality = new List<WoWItemQuality>();
                        if (wManagerSetting.CurrentSetting.MailGray)
                            mQuality.Add(WoWItemQuality.Poor);
                        if (wManagerSetting.CurrentSetting.MailWhite)
                            mQuality.Add(WoWItemQuality.Common);
                        if (wManagerSetting.CurrentSetting.MailGreen)
                            mQuality.Add(WoWItemQuality.Uncommon);
                        if (wManagerSetting.CurrentSetting.MailBlue)
                            mQuality.Add(WoWItemQuality.Rare);
                        if (wManagerSetting.CurrentSetting.MailPurple)
                            mQuality.Add(WoWItemQuality.Epic);

                        var needRunAgain = true;
                        for (var i = 14; i > 0 && needRunAgain; i--)
                        {
                            GoToTask.ToPositionAndIntecractWith(mailBox);
                            Thread.Sleep(1500 + Usefuls.Latency);

                            Mail.SendMessage(wManagerSetting.CurrentSetting.MailRecipient,
                                wManagerSetting.CurrentSetting.MailSubject, "",
                                wManagerSetting.CurrentSetting.ForceMailList,
                                wManagerSetting.CurrentSetting.DoNotMailList, mQuality, out needRunAgain);
                            
                            Thread.Sleep(500);

                            Mail.CloseMailFrame();
                        }
                        Logging.Write("[ToTown] Mail sending at " + wManagerSetting.CurrentSetting.MailRecipient[0] + "...");
                    }
                    else
                    {
                        mailBox.BlackList(120 * 60 * 1000);
                        Logging.Write("[ToTown] Unable to reach the mail box, blacklist it 120 minutes (you can disable this NPC in NPC DB tab 'Tools').");
                    }
                }
            }

            #endregion Mail

            if (!Conditions.IsAttackedAndCannotIgnore)
                ForceToTown = false;
            else
                Logging.WriteDebug("[ToTown] In combat, ForceToTown not disabled");
        }

        private void MillingAndProspecting()
        {
            // Prospection
            if (wManagerSetting.CurrentSetting.ProspectingInTown &&
                wManagerSetting.CurrentSetting.Prospecting &&
                wManagerSetting.CurrentSetting.ProspectingList.Count > 0)
            {
                if (Prospecting.NeedRun(wManagerSetting.CurrentSetting.ProspectingList))
                {
                    var prospectingState = new ProspectingState();
                    prospectingState.Run();
                }
            }
            // End Prospection


            // Milling
            if (wManagerSetting.CurrentSetting.MillingInTown &&
                wManagerSetting.CurrentSetting.Milling &&
                wManagerSetting.CurrentSetting.MillingList.Count > 0)
            {
                if (Prospecting.NeedRun(wManagerSetting.CurrentSetting.MillingList))
                {
                    var millingState = new MillingState();
                    millingState.Run();
                }
            }
            // End Milling
        }

        private bool NeededBuyFood()
        {
            // food
            if (wManagerSetting.CurrentSetting.FoodAmount > 0 && !string.IsNullOrWhiteSpace(wManagerSetting.CurrentSetting.FoodName))
            {
                if (Bag.GetContainerNumFreeSlotsNormalType > 0 &&
                    ItemsManager.GetItemCountById(ItemsManager.GetIdByName(wManagerSetting.CurrentSetting.FoodName)) < wManagerSetting.CurrentSetting.FoodAmount)
                    return true;
            }
            return false;
        }

        private bool NeededBuyDrink()
        {
            // Drink
            if (wManagerSetting.CurrentSetting.DrinkAmount > 0 && !string.IsNullOrWhiteSpace(wManagerSetting.CurrentSetting.DrinkName))
            {
                if (Bag.GetContainerNumFreeSlotsNormalType > 0 && 
                    ItemsManager.GetItemCountById(ItemsManager.GetIdByName(wManagerSetting.CurrentSetting.DrinkName)) < wManagerSetting.CurrentSetting.DrinkAmount)
                    return true;
            }
            return false;
        }

        private Spell _travelersTundraMammoth;
        private bool CanUseMammoth()
        {
            if (!CharacterHaveMammoth﻿() || !ObjectManager.ObjectManager.Me.IsOutdoors)
                return false;

            return true;
        }

        private bool CharacterHaveMammoth()
        {
            if (!wManagerSetting.CurrentSetting.UseMammoth)
                return false;

            if (ObjectManager.ObjectManager.Me.Level < 20)
                return false;

            if (_travelersTundraMammoth == null)
                _travelersTundraMammoth = new Spell("Traveler's Tundra Mammoth");

            if (_travelersTundraMammoth == null || _travelersTundraMammoth.Id <= 0 || !_travelersTundraMammoth.KnownSpell)
                return false;

            return true;
        }


        private Npc GetNpcMammothRepair()
        {
            Npc npc;

            if (ObjectManager.ObjectManager.Me.IsAlliance)
            {
                npc = Gnimo;
                
            }
            else
            {
                npc = DrixBlackwrench;
            }

            return npc;
        }
        private Npc Gnimo = new Npc 
        {
            Entry = 32639,
            Type = Npc.NpcType.Repair,
            Name = "Gnimo",
            ContinentId = (ContinentId)Usefuls.ContinentId,
            Faction = Npc.FactionType.Neutral,
            GossipOption = -1,
            Position = ObjectManager.ObjectManager.Me.Position,
            CanFlyTo = false
        };
        private Npc DrixBlackwrench = new Npc
        {
            Entry = 32641,
            Type = Npc.NpcType.Repair,
            Name = "Drix Blackwrench",
            ContinentId = (ContinentId)Usefuls.ContinentId,
            Faction = Npc.FactionType.Neutral,
            GossipOption = -1,
            Position = ObjectManager.ObjectManager.Me.Position,
            CanFlyTo = false
        };
        private Npc HakmuddArgus = new Npc
        {
            Entry = 32638,
            Type = Npc.NpcType.Vendor,
            VendorItemClass = Npc.NpcVendorItemClass.Food,
            Name = "Hakmud d'Argus",
            ContinentId = (ContinentId)Usefuls.ContinentId,
            Faction = Npc.FactionType.Neutral,
            GossipOption = -1,
            Position = ObjectManager.ObjectManager.Me.Position,
            CanFlyTo = false
        };
        private Npc Mojodishu = new Npc
        {
            Entry = 32642,
            Type = Npc.NpcType.Vendor,
            VendorItemClass = Npc.NpcVendorItemClass.Food,
            Name = "Mojodishu",
            ContinentId = (ContinentId)Usefuls.ContinentId,
            Faction = Npc.FactionType.Neutral,
            GossipOption = -1,
            Position = ObjectManager.ObjectManager.Me.Position,
            CanFlyTo = false
        };
        private Npc MollE = new Npc
        {
            Entry = 191605,
            Type = Npc.NpcType.Mailbox,
            Name = "Mailbox",
            ContinentId = (ContinentId)Usefuls.ContinentId,
            Faction = Npc.FactionType.Neutral,
            GossipOption = -1,
            Position = ObjectManager.ObjectManager.Me.Position,
            CanFlyTo = false
        };
        private Npc ClosestMailbox => NpcDB.GetNpcNearby(Npc.NpcType.Mailbox);
        private Npc ClosestVendor => NpcDB.GetNpcNearby(Npc.NpcType.Vendor);
        private Npc ClosestRepair => NpcDB.GetNpcNearby(Npc.NpcType.Repair);
        private bool DrinkEnabled
        {
            get => (wManagerSetting.CurrentSetting.DrinkAmount > 0 && !string.IsNullOrWhiteSpace(wManagerSetting.CurrentSetting.DrinkName));
        }
        
        private bool NeedRepair
        {
            get => (wManagerSetting.CurrentSetting.Repair && ObjectManager.ObjectManager.Me.GetDurabilityPercent <= minDurability);
        }
        private bool NeedVendor
        {
            get => (wManagerSetting.CurrentSetting.Selling && (Bag.GetContainerNumFreeSlots <= minFreeSlots || Bag.GetContainerNumFreeSlotsNormalType <= minFreeSlots || (wManagerSetting.CurrentSetting.GoToTownHerbBags && Bag.GetContainerNumFreeSlotsHerbBags <= minFreeSlots) || (wManagerSetting.CurrentSetting.GoToTownMiningBags && Bag.GetCo﻿ntainerNumFreeSlotsMiningBags <= minFreeSlots)));
        }
        private bool NeedMail
        {
            get => (wManagerSetting.CurrentSetting.UseMail && !string.IsNullOrWhiteSpace(wManagerSetting.CurrentSetting.MailRecipient) && (Bag.GetContainerNumFreeSlots <= minFreeSlots || Bag.GetContainerNumFreeSlotsNormalType <= minFreeSlots || (wManagerSetting.CurrentSetting.GoToTownHerbBags && Bag.GetContainerNumFreeSlotsHerbBags <= minFreeSlots) || (wManagerSetting.CurrentSetting.GoToTownMiningBags && Bag.GetContainerNumFreeSlotsMiningBags <= minFreeSlots)));
        }
        private Npc GetNpcMammothBuy()
        {
            Npc npc;

            if (ObjectManager.ObjectManager.Me.IsAlliance)
            {
                npc = HakmuddArgus;
            }
            else
            {
                npc = Mojodishu;
            }

            return npc;
        }

        private bool CanUseMollE() => wManagerSetting.CurrentSetting.UseMollE;
        
        //  Going to set this to true for now; add option (and check) for Katy's Stampwhistle later.
        private bool CanUseKatyStampwhistle() => true;



        private Npc SendMailNeedAndCanRun(int minFreeSlots)
        {
            var mailBox = new Npc();

            if (NeedMail)
            {
                if (CanUseMollE())
                    mailBox = MollE;
                else
                    mailBox = ClosestMailbox;
            }

            return mailBox;
        }

        private Npc RepairNeedAndCanRun(int minDurability)
        {
            var npc = new Npc();

            if (NeedRepair)
            {
                if (CanUseMammoth())
                    npc = GetNpcMammothRepair();
                else
                    npc = ClosestRepair;
            }

            return npc;
        }

        private List<Npc> NeedToBuy(bool forceToVendorOnly = false)
        {
            var r = new List<Npc>();
            foreach (var b in wManagerSetting.CurrentSetting.BuyList)
            {
                if (!b.IsValid())
                    continue;
                if (r.Any(n => n.Type == Npc.NpcType.Vendor && n.VendorItemClass == b.VendorItemClass))
                    continue;
                if (forceToVendorOnly && b.ForceGoToVendorNow())
                {
                    var n = NpcDB.GetNpcNearby(Npc.NpcType.Vendor, b.VendorItemClass);
                    if (n.IsValid())
                        r.Add(n);
                }
                else if (!forceToVendorOnly && b.NeedToBuy())
                {
                    var n = NpcDB.GetNpcNearby(Npc.NpcType.Vendor, b.VendorItemClass);
                    if (n.IsValid())
                        r.Add(n);
                }
                
                    
            }
            return r;
        }

        private Npc BuyFoodDrinkNeedAndCanRun()
        {
            var npc = new Npc();

            if (NeededBuyFood() || NeededBuyDrink())
            {
                if (CanUseMammoth())
                    npc = GetNpcMammothBuy();
                else
                    npc = NpcDB.GetNpcNearby(Npc.NpcType.Vendor, Npc.NpcVendorItemClass.Food);
            }

            return npc;
        }

        private Npc ResurrectBattlePetsNeedAndCanRun()
        {
            var npc = new Npc();

            if (PetBattleMode &&
                PetBattles.ConditionResurrectBattlePets &&
                !PetBattles.ReviveBattlePetsIsUsable)
                npc = NpcDB.GetNpcNearby(Npc.NpcType.ResurrectBattlePets);

            return npc;
        }

        private Npc SellNeedAndCanRun(int minFreeSlots)
        {
            var npc = new Npc();

            if (NeedVendor)
            {
                if (CanUseMammoth())
                    npc = GetNpcMammothBuy();
                else
                    npc = NpcDB.GetNpcNearby(Npc.NpcType.Vendor);
                if (!npc.IsValid())
                    npc = NpcDB.GetNpcNearby(Npc.NpcType.Repair);
            }

            return npc;
        }
    }
}