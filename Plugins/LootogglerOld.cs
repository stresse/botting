using robotManager.Helpful;
using System.Threading;
using robotManager.Products;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System;
using System.Collections.Generic;
using System.IO;

public class Main : wManager.Plugin.IPlugin
{
    private bool _isLaunched;

    public void Initialize()
    {
        Logging.Write("[Template Project Name] Started.");
        _isLaunched = true;

        pluginLoop();

    }

    public void Dispose()
    {
        _isLaunched = false;
        Logging.Write("[Template Project Name] Disposed.");
    }

    public void Settings()
    {
        _settings.Load();
        _settings.ToForm();
        _settings.Save();
    }


    public void pluginLoop()
﻿﻿    {        


        bool lootStuff = false;
//        int longerTimeToWait = 300000;    #Added to "Settings"
//        int shortTimeToWait = 60000;      #Added to "Settings"
        int actualTimeToWait = longerTimeToWait;
﻿
        while (Products.IsStarted && _isLaunched)
        {


            if (!Products.InPause)
            {

                wManager.wManagerSetting.CurrentSetting.LootMobs = lootStuff;﻿
                lootStuff= !lootStuff;

                if(actualTimeToWait == longerTimeToWait){
﻿                    ac﻿tualTimeToWait = shortTimeToWait;
                }
                else{
                    actualTimeToWait = longerTimeToWait;
                }

                Thread.Sleep(actualTimeToWait); 

            }
        }

    }﻿﻿



    private pluginSettings _settings
    {
        get
        {
            try
            {
                if (pluginSettings.CurrentSetting == null)
                    pluginSettings.Load();
                return pluginSettings.CurrentSetting;
            }
            catch
            {
            }
            return new pluginSettings();
        }
        set
        {
            try
            {
                pluginSettings.CurrentSetting = value;
            }
            catch
            {
            }
        }
    }

    [Serializable]
    public class pluginSettings : Settings
    {
        public pluginSettings()
        {
          
            longerTimeToWait = 300000;
            shortTimeToWait = 60000;
        }

        [Setting]
        [Category("General Settings")]
        [DisplayName("Kill Time")]
        [Description("How long the bot should kill shit")]
        public int longerTimeToWait { get; set; }

        [Setting]
        [Category("General Settings")]
        [DisplayName("Loot Time")]
        [Description("How long the bot should loot shit")]
        public int shortTimeToWait { get; set; }

        public static pluginSettings CurrentSetting { get; set; }

        public bool Save()
        {
            try
            {
                return Save(AdviserFilePathAndName("TemplateProjectName", ObjectManager.Me.Name + "." + Usefuls.RealmName));
            }
            catch (Exception e)
            {
                Logging.WriteError("TemplateProjectName > Save(): " + e);
                return false;
            }
        }

        public static bool Load()
        {
            try
            {
                if (File.Exists(AdviserFilePathAndName("TemplateProjectName", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
                {
                    CurrentSetting =
                        Load<pluginSettings>(AdviserFilePathAndName("TemplateProjectName",
                                                                      ObjectManager.Me.Name + "." + Usefuls.RealmName));
                    return true;
                }
                CurrentSetting = new pluginSettings();
            }
            catch (Exception e)
            {
                Logging.WriteError("TemplateProjectName > Load(): " + e);
            }
            return false;
        }
    }

}
