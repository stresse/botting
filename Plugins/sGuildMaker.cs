using robotManager.Helpful;
using System.Threading;
using robotManager.Products;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System;
using System.Collections.Generic;
using System.IO;

public class Main : wManager.Plugin.IPlugin
{
    private string ProjectName = "GuildMaker";
    private bool _isLaunched;

    public static void Log(string text)
    {
        Logging.Write(string.Format("[{0}] {1}",ProjectName,text));
//        Lua.LuaDoString("DEFAULT_CHAT_FRAME:AddMessage(\"" + "Stressed".Green + ": " + text+ "\")");
//        Lua.LuaDoString("DEFAULT_CHAT_FRAME:AddMessage(\"|cff008000Stressed|r :" + text + "\")");
        // Is it necessary to use function "FormatLua"? I still don't know.
        //Lua.LuaDoString(string.Format("DEFAULT_CHAT_FRAME:AddMessage(\"|cff008000{0}|r :{1}\")",ProjectName,text));
        // But, if so, use this (otherwise, use code above)
        Lua.LuaDoString(FormatLua(string.Format("DEFAULT_CHAT_FRAME:AddMessage(\"|cff008000{0}|r :{1}\")",ProjectName,text)));
    }

    private string FormatLua(string str, params object[] names)
    {
        return string.Format(str, names.Select(s => s.ToString().Replace("'", "\\'").Replace("\"", "\\\"")).ToArray());
    }


    public void Initialize()
    {
        GuildMakerSettings.Load();
        _isLaunched = true;
        Log("Started");
        watchForEvents();
        pluginLoop();

    }

    public void pluginLoop()
    {
        //do stuff
    }

    private void watchForEvents()
    {

        EventsLuaWithArgs.OnEventsLuaStringWithArgs += (string id, List<string> args) =>
        {
            if (id == "NAME_PLATE_UNIT_ADDED")
            
            {  
                Log("Something happened.");
                Log(args[0]);
                //Log(args);
            }
        };

    }

    public static bool CheckIsElligible(str unitID)
    {
        string luastring = string.Format(@"
            local name, realm = UnitName({0})
            local guildName, guildRankName, guildRankIndex = GetGuildInfo({0})
            local isPlayer = not not UnitIsPlayer({0})
            local guildName = select(1,GetGuildInfo({0}))
            local hasNoGuild = (guildName == nil)
            local isSameRealm = (realm ==nil)
            local isElligible = isPlayer and isSameRealm and hasNoGuild
            return isElligible",unitID);
        Lua.LuaDoString<bool>(luastring);
    }
    public void Dispose()
    {
        _isLaunched = false;
        Log("Disposed");
    }

    public void Settings()
    {
        // Code to run when someone clicks the plugin settings button
        
        // First we need to load any settings if there are any
        GuildMakerSettings.Load();
        // Then create the settings form
        GuildMakerSettings.CurrentSetting.ToForm();
        // Then save the settings when the settings form is closed
        GuildMakerSettings.CurrentSetting.Save();            
    }
    
    [Serializable]
    public class GuildMakerSettings : Settings
    {
        // This creates a setting
        // This is a default value 'guide'. If it matches the current
        // user setting, it will not be in bold.
        [DefaultValue(1000)]
        // This is a category, if you create multiple settings and give
        // them the same category they will be under the same header
        [Category("Time")]
        // The name of your setting that the user sees.
        [DisplayName("Time to walk foward")]
        // The description shown when a user clicks on the settings
        // It is shown in the box at the bottom of the window
        [Description("The amount of time in ms that the bot will walk forward")]
        // The name of setting in the code
        public int ForwardTime { get; set; }

        private GuildMakerSettings()
        {
            // Add each of your settings here to set its default value
            ForwardTime = 1000;
            
            // This is the settings window that opens up, in this example
            // its 300 pixels wide and 400 pixel high. You set the title 
            // of the window here too.
            ConfigWinForm(
                new System.Drawing.Point(300, 400), "GuildMaker " 
                + Translate.Get("Settings")
            );
        }

        public static GuildMakerSettings CurrentSetting { get; set; }
        
        // This is the save method. It saves the settings into the settings
        // folder with your plugin name, the char name and the char realm
        // I recommend just leaving this as it is other than changing 
        // the GuildMaker bits to your own
        public bool Save()
        {
            try
            {
                return Save(AdviserFilePathAndName("GuildMaker", 
                    ObjectManager.Me.Name + "." + Usefuls.RealmName));
            }
            catch (Exception e)
            {
                Logging.WriteError("GuildMakerSettings > Save(): " + e);
                return false;
            }
        }
        
        // Similar to save, leave this as it is except for replacing GuildMaker
        public static bool Load()
        {
            try
            {
                if (File.Exists(AdviserFilePathAndName("GuildMaker", 
                    ObjectManager.Me.Name + "." + Usefuls.RealmName)))
                {
                    CurrentSetting = Load<GuildMakerSettings>(
                        AdviserFilePathAndName("GuildMaker", 
                        ObjectManager.Me.Name+"."+Usefuls.RealmName));
                    return true;
                }
                CurrentSetting = new GuildMakerSettings();
            }
            catch (Exception e)
            {
                Logging.WriteError("GuildMakerSettings > Load(): " + e);
            }
            return false;
        }
    }

}