using robotManager;
using robotManager.Helpful;
// using Timer = robotManager.Helpful.Timer;
using robotManager.Products;

using wManager.Plugin;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Timers;



public class Main : IPlugin
{
	static string PROJECT_NAME = "StayClassyRecruiter";
	bool _isLaunched;
	bool _queryRun;
	public static int QueryIndex;
	public static bool DoingQueries;
	public static List<string> Queries;
	// public static List<string> Prospects = new List<string>();
	public static HashSet<string> Prospects = new HashSet<string>();
	public static System.Timers.Timer qTimer;

	public static void Log(int result)
	{
		Log(result.ToString());
	}

	public static void Log(bool result)
	{
		Log(result.ToString());
	}

	public static void Log(string text)
	{
		Logging.Write(string.Format("[{0}] {1}",PROJECT_NAME,text));
		Lua.LuaDoString(string.Format("DEFAULT_CHAT_FRAME:AddMessage(\"|cff008000{0}|r :{1}\")",PROJECT_NAME,text));
	}


	public void Initialize()
	{
		string Faction = Lua.LuaDoString<string>(@"local faction = UnitFactionGroup(""player""); return faction");
		Log(string.Format("Faction: {0}",Faction));
		if (Faction == "Horde")
		{
			List<string> Races = new List<string>(){"Blood Elf","Goblin","Orc","Pandaren","Tauren","Troll","Undead"};
			int StayClassyID = 5158;
			List<int> ClassyRaceAchievements = new List<int>(){5163,5165,5160,6625,5161,5162,5164};
			Dictionary<int,string> AchievementIDRaces = new Dictionary<int,string>() {{5163,"Blood Elf"},{5165,"Goblin"}, {5160,"Orc"}, {6625,"Pandaren"}, {5161,"Tauren"}, {5162,"Troll"}, {5164,"Undead"}};
		}
		else if (Faction == "Alliance")
		{
			List<string> Races = new List<string>(){"Draenei","Dwarf","Gnome","Human","Night Elf","Pandaren","Worgen"};
			uint StayClassyID = 5152;
			List<int> ClassyRaceAchievements = new List<int>(){5151,5153,5154,5155,5156,5157,6624};
			Dictionary<int,string> AchievementIDRaces = new Dictionary<int,string>() {{5156,"Draenei"},{5155,"Dwarf"}, {5154,"Gnome"}, {5151,"Human"}, {5153,"Night Elf"}, {6624,"Pandaren"}, {5157,"Worgen"}};
		}
		else
		{
			Log("Error! Cannot determine character faction!");
		}
		Lua.LuaDoString("C_FriendList.SetWhoToUi(whoToUi)");
		StayClassyRecruiterSettings.Load();

		Queries=GenerateQueries();
		Log("Started");
		_isLaunched = true;
		Log(string.Format("Query Interval: {0}",StayClassyRecruiterSettings.CurrentSetting.QueryInterval));
		EventWatcher();
		pluginLoop();
		Log("Initialization completed!");
	}

	private void EventWatcher()
	{

		EventsLuaWithArgs.OnEventsLuaStringWithArgs += (string id, List<string> args) =>
		{
			if (id == "WHO_LIST_UPDATE")
			{  
				GetNewWhoProspects();

			}
		};

	}

	public static void DoQueries()
	{
		qTimer = new System.Timers.Timer();
		qTimer.Interval = StayClassyRecruiterSettings.CurrentSetting.QueryInterval *1000;
		qTimer.Elapsed += DoWhoQuery;
		qTimer.AutoReset = true;
		qTimer.Enabled = true;
	}
	public static void DoWhoQuery(Object source, System.Timers.ElapsedEventArgs e)
	{

		Log(string.Format("There are {0} queries to be run", Queries.Count));
		string cQuery = GetQuery();
		if (cQuery!="")
		{
			string luastring=string.Format(@"C_FriendList.SendWho('{0}')",cQuery);
			Log(luastring);
			Lua.LuaDoString(luastring);
		}
		Log(cQuery);

	}
	public static void GetNewWhoProspects()
	{
		string luastring = @"
local prospects = {}
for i=1,C_FriendList.GetNumWhoResults() do
	local who_info = C_FriendList.GetWhoInfo(i)
	local full_name = who_info.fullName
	local has_no_guild = (who_info.fullGuildName == "")
	if has_no_guild and full_name then tinsert(prospects,full_name)
end
return unpack(prospects)
";
		var newProspects = Lua.LuaDoString<List<string>>(luastring);
		for (int i = 0; i < newProspects.Count; i++)
		{
			Prospects.Add(newProspects[i]);
			Log(string.Format("Adding {0} to Prospects. There are now {1} total prospects.",newProspects[i],Prospects.Count));
		}
		foreach(string prospect in Prospects)
		{
			Log(prospect);
		}
		Log("Done processing new prospects!");
	}

	public static void SendWhisper(string prospectName)
	{
		string luastring =string.Format(@"
			local GuildVoucherSlotLink = select(2,GetItemInfo(68136))
			local StayClassyLink = GetAchievementLink({0})
			SendChatMessage(""Hi mate! I'm putting together a guild to get the ""..StayClassyLink.."", unlock the ""..GuildVoucherSlotLink.."" for everyone in the guild, and then never use it. Everyone in the guild can use the voucher on all other guilds they are in."",""WHISPER"",nil,{1})",5158,prospectName);
	}

	public static void InviteToGuild(string prospectName)
	{
		string luastring=string.Format("GuildInvite({0})", prospectName);
	}

	public static string GetQuery()
	{
		if (Queries.Count>0)
		{
			string q = Queries[0];
			Queries.RemoveAt(0);
			return q;
		}
		else
		{
			qTimer.Stop();
			DoingQueries = false;
			Log("We have run out of queries");
			return "";
		}
	}
	public void pluginLoop()
	{
		{
			Log("Have queries");
			if (Queries.Count>0)
			{
				DoQueries();
				DoingQueries = true;
			}
			Thread.Sleep(1000);
		}
	}

	public static List<string> GenerateQueries()
	{
		string luastring = @"
local ClassyRaceAchievementIDs= {5163,5165,5160,6625,5161,5162,5164}
local RaceAchievementIDs= {[5163]=""Blood Elf"", [5165]=""Goblin"", [5160]=""Orc"", [6625]=""Pandaren"", [5161]=""Tauren"", [5162]=""Troll"", [5164]=""Undead""}
local Classes = {""Death Knight"",""Demon Hunter"",""Druid"",""Hunter"",""Mage"",""Monk"",""Paladin"",""Priest"",""Rogue"",""Shaman"",""Warlock"",""Warrior""}

local queries={}
for i=1,#ClassyRaceAchievementIDs do
   local id = ClassyRaceAchievementIDs[i]
   local numCriteria = GetAchievementNumCriteria(id)
   local qRace = RaceAchievementIDs[id]
   DEFAULT_CHAT_FRAME:AddMessage(qRace)
   for c=1,numCriteria do
	  local criteriaString, criteriaType, completed, quantity, reqQuantity, charName, flags, assetID, quantityString, criteriaID, eligible = GetAchievementCriteriaInfo(id,c)
	  local qClass
	  for i= 1,#Classes do
		 if Classes[i] == criteriaString then
			qClass=Classes[i]
		 end
	  end
	  local query=""r-\""""..qRace..""\"" c-\""""..qClass..""\"" 120""
	  if completed==false then
		 tinsert(queries, query)
	  end
   end
end
for i=1,#queries do
   local q = queries[i]
   DEFAULT_CHAT_FRAME:AddMessage(q)
end


return unpack(queries);
";
		var queries = Lua.LuaDoString<List<string>>(luastring);
		Log(string.Format("There are {0} queries ",queries.Count));
		Log("Done");
		return queries;
	}

	public static string GetPlayerFaction()
	{
		string faction = Lua.LuaDoString<string>(@"local faction = UnitFactionGroup(""player""); return faction");
		return faction;
	}

	public void Dispose()
	{
		// Code to run when the bot is stopped
	}
	public void Settings()
    {
        StayClassyRecruiterSettings.Load();
        StayClassyRecruiterSettings.CurrentSetting.ToForm();
        StayClassyRecruiterSettings.CurrentSetting.Save();
        Log("Settings saved.");
    }

}
public class StayClassyRecruiterSettings : Settings
{
    public StayClassyRecruiterSettings()
    {
		QueryInterval = 10;
        AlreadyDeclined = new List<string>();
    }

    public static StayClassyRecruiterSettings CurrentSetting { get; set; }

    public bool Save()
    {
        try
        {
            return Save(AdviserFilePathAndName("StayClassyRecruiter", ObjectManager.Me.Name + "." + Usefuls.RealmName));
        }
        catch (Exception e)
        {
            Logging.WriteError("StayClassyRecruiterSettings > Save(): " + e);
            return false;
        }
    }

    public static bool Load()
    {
        try
        {
            if (File.Exists(AdviserFilePathAndName("StayClassyRecruiter", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
            {
                CurrentSetting =
                    Load<StayClassyRecruiterSettings>(AdviserFilePathAndName("StayClassyRecruiter",
                        ObjectManager.Me.Name + "." + Usefuls.RealmName));
                return true;
            }
            CurrentSetting = new StayClassyRecruiterSettings();
        }
        catch (Exception e)
        {
            Logging.WriteError("StayClassyRecruiterSettings > Load(): " + e);
        }
        return false;
    }

    [Setting]
    [Category("Settings")]
    [DisplayName("Query Interval")]
    [Description("The time (in seconds) between queries")]
    [DefaultValue(10)]
    public int QueryInterval { get; set; }

    [Setting]
    [Category("Settings")]
    [DisplayName("Non-Participants")]
    [Description("List of players who have rejected the guild invitation already")]
    public List<string> AlreadyDeclined { get; set; }

}

