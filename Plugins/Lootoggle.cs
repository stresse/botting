using robotManager.Helpful;
using System.Threading;
using robotManager.Products;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System;
using System.Collections.Generic;
using System.IO;

public class Main : wManager.Plugin.IPlugin
{
    private bool _isLaunched;

    public void Initialize()
    {
        _isLaunched = true;
        LootogglerSettings.Load();
        Logging.Write("[Lootoggler] Started.");
        pluginLoop();
    }

    public void Dispose()
    {
        _isLaunched = false;
        Logging.Write("[Lootoggler] Disposed.");
    }

    public void Settings()
    {
        Lootoggler.Load();
        Lootoggler.ToForm();
        Lootoggler.Save();
        Logging.Write("[Lootoggler] Settings saved.");
    }


    public void pluginLoop()
﻿﻿    {
        bool lootStuff = false;
//        int longerTimeToWait = 300000;    #Added to "Settings"
//        int shortTimeToWait = 60000;      #Added to "Settings"
//        int actualTimeToWait = _settings﻿.longerTimeToWait;
﻿
        while (Products.IsStarted && _isLaunched)
        {


            if (!Products.InPause)
            {

                wManager.wManagerSetting.CurrentSetting.LootMobs = lootStuff;﻿
                lootStuff = !lootStuff;

                if (actualTimeToWait == killTime)
                {
﻿                    ac﻿tualTimeToWait = lootTime;
                }
                else
                {
                    actualTimeToWait = killTime;
                }

                Thread.Sleep(actualTimeToWait); 

            }
        }

    }﻿﻿

public class LootogglerSettings : Settings
{
    public LootogglerSettings()
    {
        killTime = 300000;
        lootTime = 60000;
    }

    public static LootogglerSettings CurrentSetting { get; set; }

    public bool Save()
    {
        try
        {
            return Save(AdviserFilePathAndName("Lootoggler", ObjectManager.Me.Name + "." + Usefuls.RealmName));
        }
        catch (Exception e)
        {
            Logging.WriteError("Lootoggler > Save(): " + e);
            return false;
        }
    }

    public static bool Load()
    {
        try
        {
            if (File.Exists(AdviserFilePathAndName("Lootoggler", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
            {
                CurrentSetting =
                    Load<LootogglerSettings>(AdviserFilePathAndName("Lootoggler",
                                                                  ObjectManager.Me.Name + "." + Usefuls.RealmName));
                return true;
            }
            CurrentSetting = new LootogglerSettings();
        }
        catch (Exception e)
        {
            Logging.WriteError("Lootoggler > Load(): " + e);
        }
        return false;
    }

    [Setting]
    [Category("General Settings")]
    [DisplayName("Kill Time")]
    [Description("How long the bot should kill shit")]
    public int killTime { get; set; }

    [Setting]
    [Category("General Settings")]
    [DisplayName("Loot Time")]
    [Description("How long the bot should loot shit")]
    public int lootTime { get; set; }
}