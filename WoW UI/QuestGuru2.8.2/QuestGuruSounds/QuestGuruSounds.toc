## Interface: 80200
## Title: QuestGuru Sounds |cffffffff2.8.2 by Lazare|r
## Version: 2.8.2
## Notes: Plays sounds for quest progress and completion "/qgs" for help
## Author: Lazare
## Dependencies: QuestGuru
## SavedVariables: QGSVars
## SavedVariablesPerCharacter: none

QuestGuruSounds.lua
#QGS_Options.lua
