## Interface: 80200
## Title: QuestGuru |cffffffff2.8.2 by Lazare|r
## Version: 2.8.2
## Notes: Quest Log Enhancement, "/qgs" for help
## Author: Lazare, current author (MrOBrian/Gregity, past authors)
## SavedVariables: QuestGuruSettings
## SavedVariablesPerCharacter: QuestGuruCollapsedHeaders

#DebugAids.lua

QuestGuru.xml
QuestGuru.lua
