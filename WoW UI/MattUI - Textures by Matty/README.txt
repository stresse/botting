-----------------------
-- Textures by Matty --
-----------------------

These are all the textures I've collected/created in my years of playing WoW. 

	20.000+ new clean icons
	250+ texture replacements

REMEMBER!

	This is NOT a addon. 

	These folders needs to be placed in your "Interface" folder. (fx "C:\World of Warcraft\Interface")

		If you the full effect of these textures, I'd recommend you using my AddOns:
		
		MattUI - DarkFrames
		MattUI - UnitFrames
		MattUI - Actionbars
		MattUI - Numberplate
		MattUI - Arena
		MattUI - Misc